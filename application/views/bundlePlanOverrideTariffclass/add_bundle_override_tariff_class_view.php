<style type="text/css">	
#dt_basic_length{float:right;}
#dt_basic_wrapper .DTTT.btn-group{float:right; margin-left:10px;  margin-right:10px;}
.smart-style-5 div.DTTT .btn{margin:0px;}
@media only screen and (min-width : 480px)
{
#dt_basic_filter{float: left;width: 50%;}
}

.loader {
  width: 3em;
  height: 3em;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  border-radius: 50%;
  box-shadow: 0 0 5px #444;
 
}
.loader:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: linear-gradient(#009EE3 , black 90%);
  animation: spin .5s infinite linear;
}
.loader:after {
  content: "";
  position: absolute;
  width: 90%;
  height: 90%;
  top: 5%;
  left:5%;
  background-color:#fff;
  border-radius: 50%;
  box-shadow: inset 0 0 5px #444;
}

@keyframes spin {
  to {
    transform: rotate(360deg);
  }
}
.load-bg{
    position:absolute;
    top:0;
    left:0;   
    width:100%;
    height:100%;
    
    z-index:99999;
    display:block;
}
.state-error{ color:#ff0000;}
.datepicker + .invalid {
    left: 0;
    position: absolute;
    top: 100%;
}
.modal-body{padding:0 20px;}
.loader1{position:relative; float:left; min-height:200px; width:100%;} 

label input.checkbox[type="checkbox"] + span{float: right; position:absolute;top:0;left:0;}
table.complete-compare thead tr th,table.complete-compare tbody tr td{white-space: nowrap; !important;}
#changeUpdate{height:200px; resize: none;}

[data-tip] {
	position:relative;

}
[data-tip]:before {
	content:'';
	/* hides the tooltip when not hovered */
	display:none;
	content:'';
	border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	border-bottom: 5px solid #1a1a1a;	
	position:absolute;
	top:30px;
	left:35px;
	z-index:8;
	font-size:0;
	line-height:0;
	width:0;
	height:0;
}
[data-tip]:after {
	display:none;
	content:attr(data-tip);
	position:absolute;
	top:35px;
	left:0px;
	padding:5px 8px;
	background:#1a1a1a;
	color:#fff;
	z-index:9;
	font-size: 0.75em;
	height:18px;
	line-height:8px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	white-space:nowrap;
	word-wrap:normal;
}
[data-tip]:hover:before,
[data-tip]:hover:after {
	display:block;
}
</style>
<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Bundle Plan Override TariffClass
        <small>Assign Package Id, TariffClass to Bundle Id</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Device list</a></li-->
        <li class="active">Bundle Plan Override TariffClass</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">	
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Add Bundle Plan Override TariffClass</h3>
						<div class="box-tools pull-right">
							<!-- <span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="3 peoples are late">3</span> -->
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form class="form-horizontal" action="<?php echo base_url();?>bundleoverridetariffclass/addBundleOverrideTariffClass" id="addBundlePlanOverrideForm" method="post">
							<div id="addBundlePlanOverrideFormDiv" style="display:none;"></div>
							<input type="hidden" name="selAccTariffClass" id="selAccTariffClass" value="" />
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Bundle ID<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<select class="form-control" name="bundleId" id="bundleId" onchange="getAccTariffClassInfo(this.value)">
											<option value="">Select</option>
											 <?php foreach($arrGetBundleInfoRes as $arrResult){ ?>
											 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']. ' (Bundle-id: '.$arrResult['Refid'].')'; ?></option>
												<?php } ?>	
										</select>										
									</div>									
								</div>															
							</div>	
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Package ID<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<select class="form-control" name="packageId" id="packageId">
											<option value="">Select</option>
											 <?php foreach($arrGetBundlePackagesInfo as $arrResult){ ?>
											 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']. ' (Package-id: '.$arrResult['Refid'].')'; ?></option>
												<?php } ?>	
										</select>										
									</div>									
								</div>															
							</div>	
						
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">ACC TARIFF CLASS<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<select class="form-control" name="acctariffclass" id="acctariffclass" multiple="multiple">												
											
										</select>										
									</div>									
								</div>															
							</div>							
							
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">NEW TARIFF CLASS<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5" data-tip="Type Capital letter only">
										<input type="text" name="newtariffclass" id="newtariffclass" value="" class="form-control" placeholder="New Tariff Class" maxlength="4" autocomplete="off" onkeydown="restrictSpace(this.id);" />
									</div>
									<!--div class="col-sm-4 col-md-4">
										<button type="submit" class="btn btn-primary">Search</button>
									</div-->
								</div>															
							</div>
							
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">COMMENT<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<input type="text" name="comment" id="comment" value="" class="form-control" placeholder="Enter Comment" maxlength="40" autocomplete="off" />
									</div>									
								</div>															
							</div>
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Activation Flag<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<input type="text" name="activationFlag" id="activationFlag" value="" class="form-control" placeholder="Enter Comment" maxlength="4" autocomplete="off" />
									</div>									
								</div>															
							</div>
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3 hidden-xs">&nbsp;</label>
									<div class="col-sm-5 col-md-5">
										<button type="submit" class="btn btn-primary">Add</button>
										<button type="button" id="delBundleOverrideButton" class="btn btn-primary">Delete</button>
									</div>
								</div>
							</div>
						</form>							
					</div>
				</div>
			</div>    
        
       
      </div>
	  <?php if($this->session->flashdata('successmsg')!=''){ ?>
				<div class="alert alert-success text-center" id="successmsg" >
				  <?php echo $this->session->flashdata('successmsg'); ?>
				  </div>
		<?php } ?>
		<?php if($this->session->flashdata('errormsg')!=''){ ?>
				<div class="alert alert-error text-center" id="errormsg" >
				  <?php echo $this->session->flashdata('errormsg'); ?>
				  </div>
		<?php } ?>
      <!-- /.row -->
    </section>
    <!-- /.content -->	
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
		<!-- widget content -->
		<div class="widget-body no-padding" id="bundleOverRideTableDiv">			
			<table id="datatable_tabletools" class="table table-bordered table-striped nowrap dt-responsive" width="100%">
				<thead>
					<tr>
						<th data-class="expand">BundleId</th>
						<th data-hide="phone">PackageId</th>
						<th data-hide="phone">Acc TariffClass</th>
						<th data-hide="phone">New TariffClass</th>
						<th data-hide="phone,tablet">Comment</th>
						<th data-hide="phone,tablet">Activation Flag</th>											
					</tr>
				</thead>
				<tbody>				
					<tr>
						<td><?php echo $arrResult['bundleid']; ?></td>
						<td><?php echo $arrResult['packid']; ?></td>
						<td><?php echo $arrResult['acc_tariffclass']; ?></td>
						<td><?php echo $arrResult['new_tariffclass']; ?></td>							
						<td><?php echo $arrResult['comment']; ?></td>
						<td><?php echo $arrResult['activation_flag']; ?></td>												
					</tr>					
				</tbody>
			</table>			
		</div>
		<!-- end widget content -->

	</div>
	<!-- end widget div -->
  </div>
  </div>
  </div>
   </section>
  <!-- /.content-wrapper -->
  
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-1 col-sm-offset-1 col-md-8 col-md-offset-2 col-xs-12">
							<form class="form-horizontal" action="">
								<div class="form-group">
									<label class="col-sm-12 col-xs-12 text-center">Assign ICCID Number</label>
									<div class="col-sm-12 col-xs-12">
										<input type="text" class="form-control" value="" placeholder="ICCID Number" />
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-12 col-xs-12 text-center">
										<input type="submit" class="btn btn-primary" value="Reset" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>				
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">OK</button>				
				</div>
			</div>
		</div>
	</div>
 <?php //include('footer.php'); ?>
 
 
 
 <script> 
 function getAllBundleOverRideOnLoad(){		
		$('#bundleOverRideTableDiv').html('<div class="load-bg"><div class="loader"></div></div>');		
		$.ajax({
			   type: "POST",
			   url: "<?php echo base_url(); ?>bundleoverridetariffclass/getAllBundleOverRideList",
			   data: '',
			   //dataType: 'json',
			   success: function(data){ 
					//alert(data);
					if(data!=""){
						$("#bundleOverRideTableDiv").html(data);							
					}else{
						 $("#bundleOverRideTableDiv").html('<b><center> No records found</center></b><br><br>');							 
					 }							
			   }
		});	
	}	
	getAllBundleOverRideOnLoad();
  
  $(document).ready(function() {
		 //alert('hai');	 	 	
		   $('#datatable_tabletools').DataTable();	
		  $('#delBundleOverrideButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundleoverridetariffclass/getBundleAccTariffClassByBundleId';
		 });
		 
		 $('#acctariffclass').click(function() {
			$("#acctariffclass option[value='']").attr('selected', false);
		});
			
		$('#addBundlePlanOverrideForm').validate({
			submitHandler : function(form){		
				//alert('hai');
				var bundleId = $("#bundleId").val();
				var packageId = $("#packageId").val();
				var acctariffclass = $("#acctariffclass").val() || [];
				$("#selAccTariffClass").val(acctariffclass);
				var comment = $('#comment').val();
				var selAccTariffClass = $("#selAccTariffClass").val();
				var activationFlag = $("#activationFlag").val();
				//alert(comment);
				
				if(selAccTariffClass!=='')
				{	
					$('#addBundlePlanOverrideFormDiv').html('<div class="load-bg"><div class="loader"></div></div>');
					$('#addBundlePlanOverrideFormDiv').show();				
					//$("#addBundlePlanOverrideForm").submit();	
					form.submit();		
				}
				else
				{						
					getAccTariffClassInfo(bundleId);					
				}						
			},
			rules:{
				bundleId:{
					required : true
				},
				packageId:{
					required : true
				},
				acctariffclass:{
					required : true
				},
				newtariffclass:{
					required : true,
					minlength: 4
				},				
				comment:{
					required : true
				},
				activationFlag:{
					required : true
				},
			},
			messages:{
				bundleId:"Please select the bundle Id",		
				packageId:"Please select the package Id",		
				acctariffclass:"Please select the acc tariff class",		
				//newtariffclass:"Please enter the new tariff class",		
				newtariffclass: {
				  required: "Please enter a tariff Class",					  
				},
				comment:"Please enter the comment",	
				activationFlag:	"Please enter the activation Flag",	
			}
		  });				
	  
	  $("#bundleId").blur(function(){
		  $("#bundleId").valid();
	  });
	  
	  $("#packageId").blur(function(){
		  $("#packageId").valid();
	  });
	  
	  $("#acctariffclass").blur(function(){
		  $("#acctariffclass").valid();
	  });
	  
	  $("#newtariffclass").blur(function(){
		  $("#newtariffclass").valid();
	  });
	  
	  $("#comment").blur(function(){
		  $("#comment").valid();
	  });
	  
	  $("#activationFlag").blur(function(){
		  $("#activationFlag").valid();
	  });
	
	/*$('#bundleId').on('click', function() {	
		$("#errormsg").hide();
		$("#successmsg").hide();
	 });	
	 $('.close').on('click', function() {	
		$('.alert').hide();
		
	 });*/
    
  });
  
  $.validator.setDefaults({
		errorElement: "span",
		errorClass: "help-block",
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		errorPlacement: function (error, element) {
			if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
			error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});	
	
	$('#activationFlag').keyup(function() {  		   
			if (this.value.match(/[^0-9 ]/g)) {
				this.value = this.value.replace(/[^0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^0-9 ]/g, '')); 
					}, 5);  
            });	

	$('#newtariffclass').keyup(function() {  		   
		if (this.value.match(/[^A-Z0-9 ]/g)) {
			this.value = this.value.replace(/[^A-Z0-9 ]/g, '');
	  }
	}).on('paste', function (e) {  
				var $this = $(this);  
				setTimeout(function () {  
					$this.val($this.val().replace(/[^A-Z0-9 ]/g, '')); 
				}, 5);  
		});				
	
	function getAccTariffClassInfo(bundleId){		
		var dataString = 'bundleId='+bundleId;	
		 $.ajax({
			   type: "POST",
			   url: "<?php echo base_url(); ?>bundleoverridetariffclass/getBundleAccTariffClass",
			   data: dataString,
			   //dataType: 'json',
			   success: function(data){ 
					//alert(data);
					if(data!==''){
						$("#acctariffclass").html(data);						
						
					}
			   }
		 });	
	}
	
	function restrictSpace(getId) {
		$('#'+getId).keydown(function (e) {	
			if (e.ctrlKey || e.altKey) {
			e.preventDefault();
			} else {
			var key = e.keyCode;
			//alert(key);
			if (key == 32) {
			e.preventDefault();
			}
			}
			});
		} 
	</script>