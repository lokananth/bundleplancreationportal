<style type="text/css">	
#dt_basic_length{float:right;}
#dt_basic_wrapper .DTTT.btn-group{float:right; margin-left:10px;  margin-right:10px;}
.smart-style-5 div.DTTT .btn{margin:0px;}
@media only screen and (min-width : 480px)
{
#dt_basic_filter{float: left;width: 50%;}
}

.loader {
  width: 3em;
  height: 3em;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  border-radius: 50%;
  box-shadow: 0 0 5px #444;
 
}
.loader:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: linear-gradient(#009EE3 , black 90%);
  animation: spin .5s infinite linear;
}
.loader:after {
  content: "";
  position: absolute;
  width: 90%;
  height: 90%;
  top: 5%;
  left:5%;
  background-color:#fff;
  border-radius: 50%;
  box-shadow: inset 0 0 5px #444;
}

@keyframes spin {
  to {
    transform: rotate(360deg);
  }
}
.load-bg{
    position:absolute;
    top:0;
    left:0;   
    width:100%;
    height:100%;
    
    z-index:99999;
    display:block;
}
.state-error{ color:#ff0000;}
.datepicker + .invalid {
    left: 0;
    position: absolute;
    top: 100%;
}
.modal-body{padding:0 20px;}
.loader1{position:relative; float:left; min-height:200px; width:100%;} 

label input.checkbox[type="checkbox"] + span{float: right; position:absolute;top:0;left:0;}
table.complete-compare thead tr th,table.complete-compare tbody tr td{white-space: nowrap; !important;}
#changeUpdate{height:200px; resize: none;}
</style>
<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>USSD Routing
        <small>Edit USSD Routing</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Device list</a></li-->
        <li class="active">Edit USSD Routing</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Edit USSD Routing</h3>
						<div class="box-tools pull-right">
							<!-- <span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="3 peoples are late">3</span> -->
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form class="form-horizontal" action="<?php echo base_url();?>bundlerouting/updateBundleUSSDRouting" id="editUSSDRoutingForm" method="post">
							<div id="editUSSDRoutingFormDiv" style="display:none;"></div>	
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">USSD Prefix<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<input type="text" class="form-control" value="" placeholder="USSD Prefix" id="ussdPrefix" name="ussdPrefix" autocomplete="off" maxlength="48" />
									</div>	
									<div class="col-sm-3 col-xs-9">
										<button type="button" id="ussdPrefixSearchButton" class="btn btn-info">Search</button>
									</div>	
								</div>															
							</div>							
							<div class="col-sm-12 col-md-10" id="ajaxErrorMsg" style="color:red;display:none;">
							<div id="loaderResultDiv" style="display:none;"></div>
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3 hidden-xs">&nbsp;</label>
									<div class="col-sm-5 col-md-5">
										<b>No records found</b>
									</div>	
								</div>
							</div>
							<div id="SearchBundleIdDiv" style="display:none;">	
								<div class="col-sm-12 col-md-10">
									<div class="form-group">
										<label class="control-label col-sm-4 col-md-3">Service Type<span style="color:red;">*</span></label>
										<div class="col-sm-5 col-md-5">
											<select class="form-control" name="serviceType" id="serviceType">	<option value="">Select</option>
											 <?php foreach($arrGetServiceType as $arrResult){ ?>
											 <option value="<?php echo base64_encode($arrResult['servicetype']); ?>"><?php echo $arrResult['servicetype']; ?></option>
												<?php  } ?>																							
											</select>										
										</div>									
									</div>															
								</div>						
								<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Bundle Id/Group Id<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<input type="text" name="bundleId" id="bundleId" value="" class="form-control" placeholder="Bundle Id" maxlength="20" autocomplete="off" />
									</div>									
								</div>															
							</div>		
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Sitecode<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<input type="text" name="Sitecode" id="Sitecode" value="<?php echo $_SESSION['SiteCode'];?>" class="form-control" placeholder="Sitecode" maxlength="20" readonly autocomplete="off" />
									</div>									
								</div>															
							</div>		
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3 hidden-xs">&nbsp;</label>
									<div class="col-sm-5 col-md-5">
										<button type="submit" class="btn btn-primary">Save</button>
										<button type="button" id="cancelUSSDRoutingButton" class="btn btn-primary">Cancel</button>
									</div>
								</div>
							</div>
						</form>	
						</div>	
					</div>
				</div>
			</div>    
        
       
      </div>
	  <?php if($this->session->flashdata('successmsg')!=''){ ?>
				<div class="alert alert-success text-center" id="successmsg" >
				  <?php echo $this->session->flashdata('successmsg'); ?>
				  </div>
		<?php } ?>
		<?php if($this->session->flashdata('errormsg')!=''){ ?>
				<div class="alert alert-error text-center" id="errormsg" >
				  <?php echo $this->session->flashdata('errormsg'); ?>
				  </div>
		<?php } ?>
      <!-- /.row -->
    </section>
    <!-- /.content -->	
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
		<!-- widget content -->
		<div class="widget-body no-padding" id="bundleRoutingTableDiv">
			
			<table id="datatable_tabletools" class="table table-bordered table-striped nowrap dt-responsive" width="100%">
				<thead>
					<tr>
						<th data-class="expand">USSD Prefix</th>
						<th data-hide="phone">Bundle Id/Group Id</th>
						<th data-hide="phone">Service Type</th>
						<th data-hide="phone">Sitecode</th>
						<!--th data-hide="phone,tablet">Renewal Mode</th-->
						<!--th data-hide="phone,tablet">Description</th-->											
					</tr>
				</thead>
				<tbody>
			
					<tr>
						<td><?php //echo $arrResult['USSD_PREFIX']; ?></td>
						<td><?php //echo $arrResult['DESTINATION_ADDRESS']; ?></td>
						<td><?php //echo $arrResult['servicetype']; ?></td>
						<td><?php //echo $arrResult['sitecode']; ?></td>						
						<!--td><?php //echo $arrResult['renewal_mode']; ?></td-->
						<!--td><?php //echo $arrResult['deskription']; ?></td-->												
					</tr>
					
				</tbody>
			</table>			
		</div>
		<!-- end widget content -->

	</div>
	<!-- end widget div -->
  </div>
  </div>
  </div>
   </section>
  <!-- /.content-wrapper -->
  
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-1 col-sm-offset-1 col-md-8 col-md-offset-2 col-xs-12">
							<form class="form-horizontal" action="">
								<div class="form-group">
									<label class="col-sm-12 col-xs-12 text-center">Assign ICCID Number</label>
									<div class="col-sm-12 col-xs-12">
										<input type="text" class="form-control" value="" placeholder="ICCID Number" />
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-12 col-xs-12 text-center">
										<input type="submit" class="btn btn-primary" value="Reset" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>				
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">OK</button>				
				</div>
			</div>
		</div>
	</div>
 <?php //include('footer.php'); ?>
 
 
 
 <script> 
 
 function getAllBundleRoutingOnLoad(){		
		$('#bundleRoutingTableDiv').html('<div class="load-bg"><div class="loader"></div></div>');		
		$.ajax({
			   type: "POST",
			   url: "<?php echo base_url(); ?>bundlerouting/getAllBundleRoutingList",
			   data: '',
			   //dataType: 'json',
			   success: function(data){ 
					//alert(data);
					if(data!=""){
						$("#bundleRoutingTableDiv").html(data);							
					}else{
						 $("#bundleRoutingTableDiv").html('<b><center> No records found</center></b><br><br>');							 
					 }							
			   }
		});	
	}	
	getAllBundleRoutingOnLoad();
   
  $(document).ready(function() {
		 //alert('hai');	 	 
		 $('#datatable_tabletools').DataTable();
		$('#cancelUSSDRoutingButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundlerouting';
		 });
		 
		 $('#editUSSDRoutingForm').validate({	
			submitHandler : function(form){					
				var ussdPrefix = $("#ussdPrefix").val();
				var serviceType = $("#serviceType").val();
				var bundleId = $('#bundleId').val();				
				//var renewalMode = $('#renewalMode').val();
				if(ussdPrefix!=='' && serviceType!=='' && bundleId!=='')
				{	
					$('#editUSSDRoutingFormDiv').html('<div class="load-bg"><div class="loader"></div></div>');
					$('#editUSSDRoutingFormDiv').show();
					//$("#editPackagePlanForm").submit();										
					form.submit();
				}
				else
				{						
					getServiceTypeByPrefix(ussdPrefix);					
				}						
			},
			rules: {
				ussdPrefix: "required",
				serviceType: "required",
				bundleId: "required",							
				//packBalance: "required",
				//renewalMode: "required",
			},
			messages: {
				ussdPrefix: "Please enter the ussd prefix",
				serviceType: "Please select the service type",
				bundleId: "Please enter the bundleId",								
				//packBalance: "Please enter the package balance",
				//renewalMode: "Please select the renewal mode",
			}	
		  });	
					
	  
	   $("#ussdPrefix").blur(function(){
		  $("#ussdPrefix").valid();
	  });
	  
	  $("#serviceType").blur(function(){
		  $("#serviceType").valid();
	  });
	  
	  $("#bundleId").blur(function(){
		  $("#bundleId").valid();
	  });  	  
	  
	  $('#serviceType').click(function() {
		 $("#serviceType option[value='']").attr('selected', false);
	  }); 	  
	  
	  $("#ussdPrefixSearchButton").click(function(){
		var ussdPrefix = $("#ussdPrefix").val();
		if(ussdPrefix==''){
			$("#ussdPrefix").valid();
		}else{
			getServiceTypeByPrefix(ussdPrefix);			
		}
	});
    
  });
  
  $.validator.setDefaults({
		errorElement: "span",
		errorClass: "help-block",
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		errorPlacement: function (error, element) {
			if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
			error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});		
	
	$('#bundleId').on('click', function() {	
		$("#errormsg").hide();
		$("#successmsg").hide();
	 });

	$('#bundleId').keyup(function() {  		   
		if (this.value.match(/[^0-9 ]/g)) {
			this.value = this.value.replace(/[^0-9 ]/g, '');
	  }
	}).on('paste', function (e) {  
				var $this = $(this);  
				setTimeout(function () {  
					$this.val($this.val().replace(/[^0-9 ]/g, '')); 
				}, 5);  
	});	 	
	
	$('.amount').keypress(function(event) {
            if(event.which == 8 || event.which == 0){
                return true;
            }
            if(event.which < 46 || event.which > 59) {
                return false;
                //event.preventDefault();
            } // prevent if not number/dot

            if(event.which == 46 && $(this).val().indexOf('.') != -1) {
                return false;
                //event.preventDefault();
            } // prevent if already dot
			
			if ($(this).val().indexOf('.') > 0) {
				var len = $(this).val().length;
				var charAfterdot = (len + 1) - $(this).val().indexOf('.');
				if (charAfterdot > 3) {
					return false;
				}
			}
			
        });
		
		
	function getServiceTypeByPrefix(ussdPrefix){
		$('#loaderResultDiv').html('<div class="load-bg"><div class="loader"></div></div>');
		$("#loaderResultDiv").show();
		var dataString = 'ussdPrefix='+ussdPrefix;	
		 $.ajax({
			   type: "POST",
			   url: "<?php echo base_url(); ?>bundlerouting/getServiceTypeByPrefix",
			   data: dataString,
			   dataType: 'json',
			   success: function(data){ 
					//alert(data.servicetype);
					//alert(data.bundleId);
					if(data.errcode!==''){
						$("#serviceType").val(data.servicetype);						
						$("#bundleId").val(data.bundleId);
						$("#ajaxErrorMsg").hide();	
						$("#loaderResultDiv").hide();
						$("#SearchBundleIdDiv").show();
					}else{	
						$("#SearchBundleIdDiv").hide();
						$("#loaderResultDiv").hide();
						$("#ajaxErrorMsg").show();
					}
			   }
		 });	
	}  
	</script>