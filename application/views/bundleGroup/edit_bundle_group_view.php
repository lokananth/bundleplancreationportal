<style type="text/css">	
#dt_basic_length{float:right;}
#dt_basic_wrapper .DTTT.btn-group{float:right; margin-left:10px;  margin-right:10px;}
.smart-style-5 div.DTTT .btn{margin:0px;}
@media only screen and (min-width : 480px)
{
#dt_basic_filter{float: left;width: 50%;}
}

.loader {
  width: 3em;
  height: 3em;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  border-radius: 50%;
  box-shadow: 0 0 5px #444;
 
}
.loader:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: linear-gradient(#009EE3 , black 90%);
  animation: spin .5s infinite linear;
}
.loader:after {
  content: "";
  position: absolute;
  width: 90%;
  height: 90%;
  top: 5%;
  left:5%;
  background-color:#fff;
  border-radius: 50%;
  box-shadow: inset 0 0 5px #444;
}

@keyframes spin {
  to {
    transform: rotate(360deg);
  }
}
.load-bg{
    position:absolute;
    top:0;
    left:0;   
    width:100%;
    height:100%;
    
    z-index:99999;
    display:block;
}
.state-error{ color:#ff0000;}
.datepicker + .invalid {
    left: 0;
    position: absolute;
    top: 100%;
}
.modal-body{padding:0 20px;}
.loader1{position:relative; float:left; min-height:200px; width:100%;} 

label input.checkbox[type="checkbox"] + span{float: right; position:absolute;top:0;left:0;}
table.complete-compare thead tr th,table.complete-compare tbody tr td{white-space: nowrap; !important;}
#changeUpdate{height:200px; resize: none;}
</style>
<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Bundle Group
        <small>Edit Group Id with Bundle Id</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Device list</a></li-->
        <li class="active">Bundle Group</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Bundle Group</h3>
						<div class="box-tools pull-right">
							<!-- <span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="3 peoples are late">3</span> -->
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form class="form-horizontal" action="<?php echo base_url();?>bundlegroup/updateBundleGroupId" id="editBundleGroupForm" method="post">
							<div id="editBundleGroupFormDiv" style="display:none;"></div>
							<input type="hidden" name="selBundleId" id="selBundleId" value="" />
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Group Id<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<input type="text" class="form-control" value="" placeholder="Enter the GroupId" id="getBundleGroupId" name="getBundleGroupId" autocomplete="off" maxlength="15" />
									</div>	
									<div class="col-sm-3 col-xs-9">
										<button type="button" id="bundleGroupIdButton" class="btn btn-info">Search</button>
									</div>	
								</div>															
							</div>
							<div class="col-sm-12 col-md-10" id="ajaxErrorMsg" style="color:red;display:none;">
							<div id="loaderResultDiv" style="display:none;"></div>
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3 hidden-xs">&nbsp;</label>
									<div class="col-sm-5 col-md-5">
										<b>No records found</b>
									</div>	
								</div>
							</div>
							<div id="SearchBundleIdDiv" style="display:none;">							
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Bundle ID<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<select class="form-control" name="bundleId" id="bundleId" multiple="multiple">											
											 <?php foreach($arrGetBundleInfoRes as $arrResult){ ?>
											 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']. ' (Bundle-id: '.$arrResult['Refid'].')'; ?></option>
												<?php } ?>	
										</select>										
									</div>
									<!--div class="col-sm-4 col-md-4">
										<button type="submit" class="btn btn-primary">Search</button>
									</div-->
								</div>															
							</div>		
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Comment<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<input type="text" name="comment" id="comment" value="" class="form-control" placeholder="Enter Comment" maxlength="50" autocomplete="off" />
									</div>									
								</div>															
							</div>		
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3 hidden-xs">&nbsp;</label>
									<div class="col-sm-5 col-md-5">
										<button type="submit" id="saveBundleGroupIdButton" class="btn btn-primary">Save</button>
										<button type="button" id="cancelBundleGroupIdButton" class="btn btn-primary">Cancel</button>
									</div>
								</div>
							</div>							
						</form>							
					</div>
				</div>
			</div>    
        
       
      </div>
	  <?php if($this->session->flashdata('successmsg')!=''){ ?>
				<div class="alert alert-success text-center" id="successmsg" >
				  <?php echo $this->session->flashdata('successmsg'); ?>
				  </div>
		<?php } ?>
		<?php if($this->session->flashdata('errormsg')!=''){ ?>
				<div class="alert alert-error text-center" id="errormsg" >
				  <?php echo $this->session->flashdata('errormsg'); ?>
				  </div>
		<?php } ?>
      <!-- /.row -->
    </section>
    <!-- /.content -->	
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
		<!-- widget content -->
		<div class="widget-body no-padding" id="bundleGroupTableDiv">			
			<table id="datatable_tabletools" class="table table-bordered table-striped nowrap dt-responsive" width="100%">
				<thead>
					<tr>
						<th data-class="expand">GroupId</th>
						<th data-hide="phone">BundleId</th>
						<th data-hide="phone">Comment</th>
						<!--th data-hide="phone">Currency</th>
						<th data-hide="phone,tablet">Site code</th>
						<th data-hide="phone,tablet">Description</th-->											
					</tr>
				</thead>
				<tbody>				
					<tr>
						<td><?php //echo $arrResult['groupid']; ?></td>
						<td><?php //echo $arrResult['Bundle_id']; ?></td>
						<td><?php //echo $arrResult['comment']; ?></td>
						<!--td><?php //echo number_format((float) $arrResult['price'], 2, '.', ''); ?></td>
						<td><?php //echo $arrResult['currency']; ?></td>
						<td><?php //echo $arrResult['sitecode']; ?></td-->																		
					</tr>					
				</tbody>
			</table>			
		</div>
		<!-- end widget content -->

	</div>
	<!-- end widget div -->
  </div>
  </div>
  </div>
   </section>
  <!-- /.content-wrapper -->
  
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-1 col-sm-offset-1 col-md-8 col-md-offset-2 col-xs-12">
							<form class="form-horizontal" action="">
								<div class="form-group">
									<label class="col-sm-12 col-xs-12 text-center">Assign ICCID Number</label>
									<div class="col-sm-12 col-xs-12">
										<input type="text" class="form-control" value="" placeholder="ICCID Number" />
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-12 col-xs-12 text-center">
										<input type="submit" class="btn btn-primary" value="Reset" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>				
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">OK</button>				
				</div>
			</div>
		</div>
	</div>
 <?php //include('footer.php'); ?>
 
 <script> 
 function getAllBundleGroupListInfoOnLoad(){		
		$('#bundleGroupTableDiv').html('<div class="load-bg"><div class="loader"></div></div>');		
		$.ajax({
			   type: "POST",
			   url: "<?php echo base_url(); ?>bundlegroup/getAllBundleGroupListInfo",
			   data: '',
			   //dataType: 'json',
			   success: function(data){ 
					//alert(data);
					if(data!=""){
						$("#bundleGroupTableDiv").html(data);							
					}else{
						 $("#bundleGroupTableDiv").html('<b><center> No records found</center></b><br><br>');							 
					 }							
			   }
		});	
	}	
	getAllBundleGroupListInfoOnLoad();
   $(document).ready(function() {		 
		$('#datatable_tabletools').DataTable();	
		$('#cancelBundleGroupIdButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundlegroup';
		 });
		 
		 $('#bundleId').multiselect({			
            maxHeight: 200
        });
		 
		$('#editBundleGroupForm').validate({
			submitHandler : function(form){		
				//alert('hai');
				$('#editBundleGroupFormDiv').html('<div class="load-bg"><div class="loader"></div></div>');
				$('#editBundleGroupFormDiv').show();
				var getBundleGroupId = $("#getBundleGroupId").val();
				var bundleId = $("#bundleId").val() || [];
				var comment = $('#comment').val();
				//alert(comment);
				$("#selBundleId").val(bundleId);
				if(comment!=='')
				{					
					//$("#editBundleGroupForm").submit();										
					form.submit();
				}
				else
				{						
					getBundleIdByGroup(getBundleGroupId);					
				}						
			},
			rules: {
				getBundleGroupId: "required",
				bundleId: "required",
				comment: "required",							
			},
			messages: {
				getBundleGroupId: "Please enter the Group Id",
				bundleId: "Please select the bundle Id",
				comment: "Please enter the comments",								
			}	
		  });				
	  
	  $("#getBundleGroupId").blur(function(){
		  $("#getBundleGroupId").valid();
	  });	
	  
	  $("#bundleId").blur(function(){
		  $("#bundleId").valid();
	  });	
	  
	  $("#comment").blur(function(){
		  $("#comment").valid();
	  });	
	
	/*$('#bundleId').on('click', function() {	
		$("#errormsg").hide();
		$("#successmsg").hide();
	 });	*/
    
  });
  
  $.validator.setDefaults({
		errorElement: "span",
		errorClass: "help-block",
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		errorPlacement: function (error, element) {
			if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
			error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});		
	 
			
	$('#getBundleGroupId').keyup(function() {  		   
		if (this.value.match(/[^0-9 ]/g)) {
			this.value = this.value.replace(/[^0-9 ]/g, '');
	  }
	}).on('paste', function (e) {  
				var $this = $(this);  
				setTimeout(function () {  
					$this.val($this.val().replace(/[^0-9 ]/g, '')); 
				}, 5);  
	});	 
			
	$('#getBundleGroupId').click(function() {  										
		$("#errormsg").hide();
		$("#successmsg").hide();
	});
	
	
	
	$('#bundleId').click(function() {
		$("#bundleId option[value='']").attr('selected', false);
	});
	
	$("#bundleGroupIdButton").click(function(){
		var getBundleGroupId = $("#getBundleGroupId").val();
		if(getBundleGroupId==''){
			$("#getBundleGroupId").valid();
		}else{
			getBundleIdByGroup(getBundleGroupId);			
		}
	});
	
	function getBundleIdByGroup(getBundleGroupId){
		$('#loaderResultDiv').html('<div class="load-bg"><div class="loader"></div></div>');
		$("#loaderResultDiv").show();
		var dataString = 'getBundleGroupId='+getBundleGroupId;	
		 $.ajax({
			   type: "POST",
			   url: "<?php echo base_url(); ?>bundlegroup/getBundleIdByGroupId",
			   data: dataString,
			   dataType: 'json',
			   success: function(data){ 
					//alert(data);
					if(data!==''){
						$('#bundleId').multiselect();
						var dataarray=data.bundleId.split(",");
						var stringArray = (new Function("return [" + dataarray+ "];")());												
						$('#bundleId').multiselect('select', stringArray);	
						//$("#bundleId").val(dataarray);
						$("#comment").val(data.Comment);
						$("#ajaxErrorMsg").hide();	
						$("#loaderResultDiv").hide();
						$('#editBundleGroupFormDiv').hide();
						$("#SearchBundleIdDiv").show();
					}else{	
						$("#SearchBundleIdDiv").hide();
						$("#loaderResultDiv").hide();
						$('#editBundleGroupFormDiv').hide();
						$("#ajaxErrorMsg").show();
					}
			   }
		 });	
	}
  
	</script>