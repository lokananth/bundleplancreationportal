<?php //include('top_header.php');?>
<?php //include('left_menu.php');?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Select Product
      </h1>	  
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Select Product</li>
      </ol>
    </section>

    <!-- Main content -->
    
    <!-- /.content -->
	
  </div>
  <!-- /.content-wrapper -->

  
  <!-- Modal -->
  <div class="modal fade" id="myModal-10" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header">
			  <h4 class="modal-title">Select Product</h4>
			</div>
			<div class="modal-body">
			<form name="site_code_form" id="site_code_form" class="form-horizontal" method="post">
				<div class="form-group">
					<label for="siteCode" class="col-sm-4"> Select Product:</label>
					<div class="col-sm-6 col-xs-12">
						<select class="form-control" name="productName" id="productName">
							<option value="">Select</option>
						 <?php foreach($arrGetProductInfo as $arrResult){ ?>
						 <option value="<?php echo $arrResult['product_code']; ?>" data-siteCode="<?php echo $arrResult['sitecode']; ?>" data-currency="<?php echo $arrResult['Currency']; ?>"><?php echo $arrResult['product_name']." - ".$arrResult['product_code']; ?></option>
							<?php } ?>	
						 </select>
					</div>
			 </form>
			</div>
			
		  </div>
		  
		</div>
	  </div>
	  
	</div>
	<!-- modal end-->	
  
 <?php //include('footer.php'); ?>
	<script>
 setTimeout(function () {
     $('#myModal-10').modal({backdrop: 'static', keyboard: false});
	},120);
	
		
$(document).ready(function (){
    function reposition() {
        var modal = $('#myModal-10'),
            dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        // Dividing by two centers the modal exactly, but dividing by three 
        // or four works better for larger screens.
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height() - 180) / 2));
    }
    // Reposition when a modal is shown
    $('.modal').on('show.bs.modal', reposition);
    // Reposition when the window is resized
    $(window).on('resize', function() {
        $('.modal:visible').each(reposition);
    });
	
	$("#errProductName").html('');
	
	$('#productName').change(function() {				
			var productCode = $(this).val();
			if(productCode!=''){
				var siteCode = $('#productName option:selected').attr('data-siteCode');
				var currency = $('#productName option:selected').attr('data-currency');
				//alert(productCode);alert(siteCode);alert(currency);
				window.location.href = "<?php echo base_url('dashboard'); ?>/setProductSiteCode/"+productCode+"/"+siteCode+"/"+currency;	
			}else{
				$("#errProductName").html('Please select a product name');
			}			
		});
});
 </script> 