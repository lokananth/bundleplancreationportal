<?php //include('top_header.php');?>
<?php //include('left_menu.php');?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <!--ol class="breadcrumb">	 
        <li><u><a href="<?php echo base_url();?>dashboard" title="Change Product"><i class="fa fa-dashboard"></i> <?php if(isset($_SESSION['ProductCode'])){ echo $_SESSION['ProductCode']; } ?></a></u></li>
        <li class="active">Dashboard</li>
      </ol-->
    </section>

    <!-- Main content -->
    <section class="content">
		
		<div class="row">
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
						<h4>Bundle Plan</h4>

						<!--p>Bundle Plan</p-->
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="<?php echo base_url();?>bundleplan" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<!--div class="col-lg-3 col-xs-6"-->
				<!-- small box -->
				<!--div class="small-box bg-maroon">
					<div class="inner">
						<h4>Bundle Message Reference</h4>

						<!--p>Bundle Message Reference</p-->
					<!--/div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="<?php //echo base_url();?>bundlemsgrefer" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div-->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-fuchsia">
					<div class="inner">
						<h4>Bundle Message</h4>

						<!--p>Bundle Message</p-->
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="<?php echo base_url();?>bundlemessage" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-orange">
					<div class="inner">
						<h4>Bundle Group</h4>

						<!--p>Bundle Group</p-->
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="<?php echo base_url();?>bundlegroup" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-teal">
					<div class="inner">
						<h4>Tariff Class Group</h4>

						<!--p>Tariff Class Group</p-->
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="<?php echo base_url();?>tariffclassgroup" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-olive">
					<div class="inner">
						<h4>Bundle Plan Package</h4>

						<!--p>Bundle Plan Package</p-->
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="<?php echo base_url();?>bundlepackagemodule" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-navy">
					<div class="inner">
						<h4>Bundle Plan Override Tariffclass</h4>

						<!--p>Bundle Plan Override Tariffclass</p-->
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="<?php echo base_url();?>bundleoverridetariffclass" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-maroon">
					<div class="inner">
						<h4>USSD Routing</h4>
						<!--p>Bundle Message Reference</p-->
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="<?php echo base_url();?>bundlerouting" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-purple">
					<div class="inner">
						<h4>Settings Document</h4>
						<!--p>Bundle Message Reference</p-->
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="<?php echo base_url();?>bundlesettings" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>		
		</div>		
      	<!-- /.row -->
    </section>
    <!-- /.content -->
	
	
	
  
  
  
  
 <?php //include('footer.php'); ?>