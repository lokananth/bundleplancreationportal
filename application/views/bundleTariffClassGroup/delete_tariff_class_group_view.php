<style type="text/css">	
#dt_basic_length{float:right;}
#dt_basic_wrapper .DTTT.btn-group{float:right; margin-left:10px;  margin-right:10px;}
.smart-style-5 div.DTTT .btn{margin:0px;}
@media only screen and (min-width : 480px)
{
#dt_basic_filter{float: left;width: 50%;}
}

.loader {
  width: 3em;
  height: 3em;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  border-radius: 50%;
  box-shadow: 0 0 5px #444;
 
}
.loader:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: linear-gradient(#009EE3 , black 90%);
  animation: spin .5s infinite linear;
}
.loader:after {
  content: "";
  position: absolute;
  width: 90%;
  height: 90%;
  top: 5%;
  left:5%;
  background-color:#fff;
  border-radius: 50%;
  box-shadow: inset 0 0 5px #444;
}

@keyframes spin {
  to {
    transform: rotate(360deg);
  }
}
.load-bg{
    position:absolute;
    top:0;
    left:0;   
    width:100%;
    height:100%;
    
    z-index:99999;
    display:block;
}
.state-error{ color:#ff0000;}
.datepicker + .invalid {
    left: 0;
    position: absolute;
    top: 100%;
}
.modal-body{padding:0 20px;}
.loader1{position:relative; float:left; min-height:200px; width:100%;} 

label input.checkbox[type="checkbox"] + span{float: right; position:absolute;top:0;left:0;}
table.complete-compare thead tr th,table.complete-compare tbody tr td{white-space: nowrap; !important;}
#changeUpdate{height:200px; resize: none;}
</style>
<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Tariff Class Group
        <small>Group Id with Tariff Class</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Device list</a></li-->
        <li class="active">Delete Tariff Class Group</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Delete Tariff Class Group</h3>
						<div class="box-tools pull-right">
							<!-- <span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="3 peoples are late">3</span> -->
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form class="form-horizontal" action="<?php echo base_url();?>tariffclassgroup/deleteTariffClassGroup" id="tariffClassForm" method="post">
						<input type="hidden" name="selTariffClass" id="selTariffClass" value="" />
						<div id="tariffClassFormDiv" style="display:none;"></div>
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Group Id<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<input type="text" class="form-control" value="" placeholder="Enter the GroupId" id="tariffGroupId" name="tariffGroupId" autocomplete="off" maxlength="15"  />
									</div>	
									<div class="col-sm-3 col-xs-9">
										<button type="button" id="bundleGroupIdButton" class="btn btn-info">Search</button>
									</div>	
								</div>															
							</div>
							<div class="col-sm-12 col-md-10" id="ajaxErrorMsg" style="color:red;display:none;">
							<div id="loaderResultDiv" style="display:none;"></div>
							<!--div id="loader1ResultDiv" style="display:none;"></div-->
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3 hidden-xs">&nbsp;</label>
									<div class="col-sm-5 col-md-5">
										<b>No records found</b>
									</div>	
								</div>
							</div>
							<div id="SearchPackageIdDiv" style="display:none;">	
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">TariffClass<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<select class="form-control" name="tariffClass" id="tariffClass" multiple="multiple">												
											
										</select>										
									</div>									
								</div>															
							</div>							
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3 hidden-xs">&nbsp;</label>
									<div class="col-sm-5 col-md-5">
										<button type="submit" class="btn btn-primary">Delete</button>
										<button type="button" id="cancelTariffClassButton" class="btn btn-primary">Cancel</button>
									</div>
								</div>
							</div>
						</form>	
						</div>	
					</div>
				</div>
			</div>    
        
       
      </div>
	  <?php if($this->session->flashdata('successmsg')!=''){ ?>
				<div class="alert alert-success text-center" id="successmsg" >
				  <?php echo $this->session->flashdata('successmsg'); ?>
				  </div>
		<?php } ?>
		<?php if($this->session->flashdata('errormsg')!=''){ ?>
				<div class="alert alert-error text-center" id="errormsg" >
				  <?php echo $this->session->flashdata('errormsg'); ?>
				  </div>
		<?php } ?>
      <!-- /.row -->
    </section>
    <!-- /.content -->	
	
<section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
		<div class="widget-body no-padding" id="tariffGroupTableDiv">			
			<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
				<thead>
					<tr>
						<th data-class="expand">GroupId</th>
						<th data-hide="phone">TariffClass</th>
						<th data-hide="phone">Comment</th>	
					</tr>
				</thead>
				<tbody>				
					<tr>
						<td><?php //echo $arrResult['groupid']; ?></td>
						<td><?php //echo $arrResult['tariffclass']; ?></td>																
						<td><?php //echo $arrResult['comment']; ?></td>
					</tr>					
				</tbody>
			</table>			
		</div>
		<!-- end widget content -->
	
  </div>
	<!-- end widget div -->
  </div>
  </div>
  </div>
   </section>
  
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-1 col-sm-offset-1 col-md-8 col-md-offset-2 col-xs-12">
							<form class="form-horizontal" action="">
								<div class="form-group">
									<label class="col-sm-12 col-xs-12 text-center">Assign ICCID Number</label>
									<div class="col-sm-12 col-xs-12">
										<input type="text" class="form-control" value="" placeholder="ICCID Number" />
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-12 col-xs-12 text-center">
										<input type="submit" class="btn btn-primary" value="Reset" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>				
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">OK</button>				
				</div>
			</div>
		</div>
	</div>
 <?php //include('footer.php'); ?>
 
 
 
 <script> 
 function getAllTariffGroupListInfoOnLoad(){		
		$('#tariffGroupTableDiv').html('<div class="load-bg"><div class="loader"></div></div>');		
		$.ajax({
			   type: "POST",
			   url: "<?php echo base_url(); ?>tariffclassgroup/getAllTariffGroupListInfo",
			   data: '',
			   //dataType: 'json',
			   success: function(data){ 
					//alert(data);
					if(data!=""){
						$("#tariffGroupTableDiv").html(data);							
					}else{
						 $("#tariffGroupTableDiv").html('<b><center> No records found</center></b><br><br>');							 
					 }							
			   }
		});	
	}	
	getAllTariffGroupListInfoOnLoad(); 
  
  $(document).ready(function() {
		 //alert('hai');	 	
$('#datatable_tabletools').DataTable();		 
		 $('#cancelTariffClassButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>tariffclassgroup';
		 });	 
		 
			
		$('#tariffClassForm').validate({
			submitHandler : function(form){		
				//alert('hai');
				//$('#loader1ResultDiv').html('<div class="load-bg"><div class="loader"></div></div>');
				//$("#loader1ResultDiv").show();
				var getTariffClassGroupId = $("#tariffGroupId").val();				
				var bundleId = $("#bundleId").val();
				var packageId = $("#packageId").val();
				var tariffClass = $("#tariffClass").val() || [];
				$("#selTariffClass").val(tariffClass);
				var selTariffClass = $("#selTariffClass").val();				
				//alert(selTariffClass);				
				if(selTariffClass!=='' && getTariffClassGroupId!=='')
				{	
					$('#tariffClassFormDiv').html('<div class="load-bg"><div class="loader"></div></div>');
					$('#tariffClassFormDiv').show();
					//$("#tariffClassForm").submit();														
					form.submit();
				}
				else
				{						
					getTariffClassByGroup(getTariffClassGroupId);					
				}						
			},
			rules: {
				tariffGroupId: "required",
				tariffClass: "required",				
			},
			messages: {
				tariffGroupId: "Please enter the Tariff GroupId",
				tariffClass: "Please select the Tariff Class",				
			},
		  });				
	  
	 /* $("#bundleId").blur(function(){
		  $("#bundleId").valid();
	  });	*/	  
	 
	
	$('#tariffGroupId').on('click', function() {	
		$("#errormsg").hide();
		$("#successmsg").hide();
	 });		  
    
  });  
  
  $.validator.setDefaults({
		errorElement: "span",
		errorClass: "help-block",
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		errorPlacement: function (error, element) {
			if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
			error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});	
	
	$("#bundleGroupIdButton").click(function(){
		var getTariffClassGroupId = $("#tariffGroupId").val();
		if(getTariffClassGroupId==''){
			$("#tariffGroupId").valid();
		}else{
			getTariffClassByGroup(getTariffClassGroupId);			
		}
	});
	
	$('#tariffGroupId').keyup(function() {  		   
		if (this.value.match(/[^0-9 ]/g)) {
			this.value = this.value.replace(/[^0-9 ]/g, '');
	  }
	}).on('paste', function (e) {  
				var $this = $(this);  
				setTimeout(function () {  
					$this.val($this.val().replace(/[^0-9 ]/g, '')); 
				}, 5);  
	});	 
	
	$('#tariffClass').click(function() {
			$("#tariffClass option[value='']").attr('selected', false);
		});
  
	
	function getTariffClassByGroup(getTariffClassGroupId){
		$('#loaderResultDiv').html('<div class="load-bg"><div class="loader"></div></div>');
		$("#loaderResultDiv").show();
		var dataString = 'getTariffClassGroupId='+getTariffClassGroupId;	
		 $.ajax({
			   type: "POST",
			   url: "<?php echo base_url(); ?>tariffclassgroup/getTariffClassByTariffGroupId",
			   data: dataString,
			   //dataType: 'json',
			   success: function(data){ 
					//alert(data);
					if(data!==''){						
						//var dataarray=data.packageId.split(",");
						//$("#resultTariffClassDiv").html(data.TariffClass);
						$("#tariffClass").html(data);
						$("#ajaxErrorMsg").hide();	
						$("#loaderResultDiv").hide();
						//$("#loader1ResultDiv").hide();
						$("#SearchPackageIdDiv").show();
					}else{	
						$("#SearchPackageIdDiv").hide();
						$("#loaderResultDiv").hide();
						//$("#loader1ResultDiv").hide();
						$("#ajaxErrorMsg").show();
					}
			   }
		 });	
	}
	</script>