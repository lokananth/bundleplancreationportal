<style type="text/css">	
#dt_basic_length{float:right;}
#dt_basic_wrapper .DTTT.btn-group{float:right; margin-left:10px;  margin-right:10px;}
.smart-style-5 div.DTTT .btn{margin:0px;}
@media only screen and (min-width : 480px)
{
#dt_basic_filter{float: left;width: 50%;}
}

.loader {
  width: 3em;
  height: 3em;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  border-radius: 50%;
  box-shadow: 0 0 5px #444;
 
}
.loader:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: linear-gradient(#009EE3 , black 90%);
  animation: spin .5s infinite linear;
}
.loader:after {
  content: "";
  position: absolute;
  width: 90%;
  height: 90%;
  top: 5%;
  left:5%;
  background-color:#fff;
  border-radius: 50%;
  box-shadow: inset 0 0 5px #444;
}

@keyframes spin {
  to {
    transform: rotate(360deg);
  }
}
.load-bg{
    position:absolute;
    top:0;
    left:0;   
    width:100%;
    height:100%;
    
    z-index:99999;
    display:block;
}
.state-error{ color:#ff0000;}
.datepicker + .invalid {
    left: 0;
    position: absolute;
    top: 100%;
}
.modal-body{padding:0 20px;}
.loader1{position:relative; float:left; min-height:200px; width:100%;} 

label input.checkbox[type="checkbox"] + span{float: right; position:absolute;top:0;left:0;}
table.complete-compare thead tr th,table.complete-compare tbody tr td{white-space: nowrap; !important;}
#changeUpdate{height:200px; resize: none;}

[data-tip] {
	position:relative;

}
[data-tip]:before {
	content:'';
	/* hides the tooltip when not hovered */
	display:none;
	content:'';
	border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	border-bottom: 5px solid #1a1a1a;	
	position:absolute;
	top:30px;
	left:35px;
	z-index:8;
	font-size:0;
	line-height:0;
	width:0;
	height:0;
}
[data-tip]:after {
	display:none;
	content:attr(data-tip);
	position:absolute;
	top:35px;
	left:0px;
	padding:5px 8px;
	background:#1a1a1a;
	color:#fff;
	z-index:9;
	font-size: 0.75em;
	height:18px;
	line-height:8px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	white-space:nowrap;
	word-wrap:normal;
}
[data-tip]:hover:before,
[data-tip]:hover:after {
	display:block;
}
</style>
<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Tariff Class Group
        <small>Group Id with Tariff Class</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Device list</a></li-->
        <li class="active">Add Tariff Class Group</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Add Tariff Class Group</h3>
						<div class="box-tools pull-right">
							<!-- <span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="3 peoples are late">3</span> -->
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form class="form-horizontal" action="<?php echo base_url();?>tariffclassgroup/addTariffClassGroup" id="tariffClassForm" method="post">
						<div id="tariffClassFormDiv" style="display:none;"></div>
							<!--div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Tariff Class<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<input type="text" name="tariffClass" id="tariffClass" value="" class="form-control" placeholder="Enter the Tariff Class" maxlength="4" onkeydown="restrictSpace(this.id);" autocomplete="off" />
									</div>								
								</div>															
							</div-->

							<div class="col-sm-12 col-md-10">								
								<div class="form-group">	
									<label class="control-label col-sm-4 col-md-3">Tariff Class<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5 ">
										<div class="row field_wrapper">
											<div class="col-sm-12 col-md-12">
												<div class="row"><div class="col-sm-8 col-md-10" data-tip="Type Capital letter only">
														<input type="text" id="tariffClass" name="tariffClass" class="tariffCSS tariffSpace tariffValue form-control" value="" placeholder="Enter the Tariff Class" maxlength="4" autocomplete="off" />
														<br><span id="errMessageTariffClass" style="color:red;"></span>
													</div>
													<div class="col-sm-4 col-md-2">
														<a href="javascript:void(0);" class="add_button" title="Add field"><button id="btnAdd" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span></button></a>
													</div>								
												</div>
											</div>								
										</div>								
									</div>								
								</div>															
							</div>								
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Comment<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<input type="text" name="comment" id="comment" value="" class="form-control" placeholder="Enter Comment" maxlength="50" autocomplete="off" />
									</div>									
								</div>															
							</div>
							<div id="loader1ResultDiv" style="display:none;"></div>
							<div class="col-sm-12 col-md-10">
								<!--span id="errCheckTariffClass" style="color:red;"></span-->	
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3 hidden-xs">&nbsp;</label>
									
									<div class="col-sm-5 col-md-5">
										<button type="submit" class="btn btn-primary">Add</button>
										<button type="button" id="editTariffClassButton" class="btn btn-primary">Edit</button>
										<button type="button" id="deleteTariffClassButton" class="btn btn-primary">Delete</button>										
									</div>
								</div>
							</div>
						</form>							
					</div>
				</div>
			</div>    
        
       
      </div>
	  <?php if($this->session->flashdata('successmsg')!=''){ ?>
				<div class="alert alert-success text-center" id="successmsg" >
				  <?php echo $this->session->flashdata('successmsg'); ?>
				  </div>
		<?php } ?>
		<?php if($this->session->flashdata('errormsg')!=''){ ?>
				<div class="alert alert-error text-center" id="errormsg" >
				  <?php echo $this->session->flashdata('errormsg'); ?>
				  </div>
		<?php } ?>
      <!-- /.row -->
    </section>
    <!-- /.content -->	

<section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
		<div class="widget-body no-padding" id="tariffGroupTableDiv">			
			<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
				<thead>
					<tr>
						<th data-class="expand">GroupId</th>
						<th data-hide="phone">TariffClass</th>
						<th data-hide="phone">Comment</th>	
					</tr>
				</thead>
				<tbody>				
					<tr>
						<td><?php //echo $arrResult['groupid']; ?></td>
						<td><?php //echo $arrResult['tariffclass']; ?></td>																
						<td><?php //echo $arrResult['comment']; ?></td>
					</tr>					
				</tbody>
			</table>			
		</div>
		<!-- end widget content -->
	
  </div>
	<!-- end widget div -->
  </div>
  </div>
  </div>
   </section>
  
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-1 col-sm-offset-1 col-md-8 col-md-offset-2 col-xs-12">
							<form class="form-horizontal" action="">
								<div class="form-group">
									<label class="col-sm-12 col-xs-12 text-center">Assign ICCID Number</label>
									<div class="col-sm-12 col-xs-12">
										<input type="text" class="form-control" value="" placeholder="ICCID Number" />
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-12 col-xs-12 text-center">
										<input type="submit" class="btn btn-primary" value="Reset" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>				
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">OK</button>				
				</div>
			</div>
		</div>
	</div>
 <?php //include('footer.php'); ?>
 
 
 
 <script>  
function getAllTariffGroupListInfoOnLoad(){		
		$('#tariffGroupTableDiv').html('<div class="load-bg"><div class="loader"></div></div>');		
		$.ajax({
			   type: "POST",
			   url: "<?php echo base_url(); ?>tariffclassgroup/getAllTariffGroupListInfo",
			   data: '',
			   //dataType: 'json',
			   success: function(data){ 
					//alert(data);
					if(data!=""){
						$("#tariffGroupTableDiv").html(data);							
					}else{
						 $("#tariffGroupTableDiv").html('<b><center> No records found</center></b><br><br>');							 
					 }							
			   }
		});	
	}	
	getAllTariffGroupListInfoOnLoad(); 
	
  $(document).ready(function() {
		$('#datatable_tabletools').DataTable();
		 //alert('hai');	 	 
		 $('#editTariffClassButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>tariffclassgroup/editTariffClassByGroupId/1';
		 });	
		 
		 $('#deleteTariffClassButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>tariffclassgroup/editTariffClassByGroupId/2';
		 });	
					
		var maxField = 50; //Input fields increment limitation
		var addButton = $('.add_button'); //Add button selector
		var wrapper = $('.field_wrapper'); //Input field wrapper
		
		var x = 1; //Initial field counter is 1
		
		$(addButton).click(function(){ //Once add button is clicked
			if(x < maxField){ //Check maximum number of input fields
				
				
				var fieldHTML = '<div class="col-sm-12 col-md-12 m-t-15"><div class="row"><div class="col-sm-8 col-md-10" data-tip="Type Capital letter only"><input type="text" name="field_name[]" class="tariffCSS tariffSpace tariffValue tariffValid form-control" value="" placeholder="Enter the Tariff Class" maxlength="4" autocomplete="off" /></div><div class="col-sm-4 col-md-2"><a href="javascript:void(0);" class="remove_button" title="Remove field"><button id="btnAdd" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-minus"></span></button></a></div></div></div>'; //New input field html 
				
				x++; //Increment field counter
				
				$(wrapper).append(fieldHTML); // Add field html
				
				/*$('.tariffValid').each(function () {
					$("#tariffClass"+i).rules("add", {
						required: true
					});
					i++;
				});	*/
			}
		});
	   
	   $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
			e.preventDefault();
			$(this).parent().parent().parent('div').remove(); //Remove field html
			x--; //Decrement field counter
		});		
		
		$(wrapper).on('keyup', '.tariffCSS', function(e){ 
			e.preventDefault();
			if (this.value.match(/[^A-Z0-9 ]/g)) {
					this.value = this.value.replace(/[^A-Z0-9 ]/g, '');
			}
		});	
		  
		$(wrapper).on('paste', '.tariffCSS', function(e){ 
			e.preventDefault();
			if (this.value.match(/[^A-Z0-9 ]/g)) {
					this.value = this.value.replace(/[^A-Z0-9 ]/g, '');
			}
		});	 
		
		$(wrapper).on('keydown', '.tariffSpace', function(e){ 
			if (e.ctrlKey || e.altKey) {
					e.preventDefault();
				} else {
					var key = e.keyCode;
					//alert(key);
					if (key == 32) {
					e.preventDefault();
					}
				}
		});	 		 
		
		$('#tariffClass').on('click', function() {	
			$("#errormsg").hide();
			$("#successmsg").hide();
		 });
		 
		/* $("#addTariffClassButton").on('click', function() {
			 //alert('hai');
			 $("#tariffClass").valid();
			  $("#comment").valid();
			 var tariffClass = $("#tariffClass").val();				
				if(tariffClass=='')
				{	
					$("#errMessageTariffClass").html('Please enter the tariffClass');
					$("#tariffClass").addClass("help-block");
					return false;
				}
		 });	 */

		$('#tariffClassForm').validate({
			submitHandler : function(form){	
				/*$('#loader1ResultDiv').html('<div class="load-bg"><div class="loader"></div></div>');
				$("#loader1ResultDiv").show();*/
				var tariffClass = $("#tariffClass").val();				
				if(tariffClass!=='')
				{		
					var n = tariffClass.length;
					if(n!==4){
						$("#errMessageTariffClass").html('Please enter at least 4 characters');
						return false;
					}					
						
					var tariffValue=new Array();
					 var j=0;	
					$(".tariffValue").each(function(){						
						tariffValue[j]=$(this).val();	
						j++;						
					});	
					var uniqueTariffValue = [];
					$.each(tariffValue, function(i, el){
						if($.inArray(el, uniqueTariffValue) === -1) uniqueTariffValue.push(el);
					});
					
					var tariffValueCount = tariffValue.length;
					var uniqueTariffValueCount = uniqueTariffValue.length;			
					
					if(tariffValueCount==uniqueTariffValueCount){
						$('#tariffClassFormDiv').html('<div class="load-bg"><div class="loader"></div></div>');
						$('#tariffClassFormDiv').show();
						$("#errMessageTariffClass").html('');
						//$("#tariffClassForm").submit();								
						form.submit();
					}else{
						$("#errMessageTariffClass").html('Please check the given TariffClass duplicate');
						//$("#loader1ResultDiv").hide();
						return false;
					}
																		
				}
				else
				{						
					$("#errMessageTariffClass").html('Please enter the tariffClass');
					//$("#loader1ResultDiv").hide();
					return false;	
				}						
			},
			rules: {
				//tariffClass: "required",
				tariffClass: {
				  required: true,
				  minlength: 4,
				},
				comment: "required",							
			},
			messages: {
				//tariffClass: "Please enter the Tariff Class",
				tariffClass: {
				  required: "Please enter a tariff Class",					  
				},
				comment: "Please enter the comments",								
			}
		  });	

		$("#tariffClass").blur(function(){
		  $("#tariffClass").valid();
	  });	
	  
	  $("#comment").blur(function(){
		  $("#comment").valid();
	  });	
    
  });
  
  
  
  $.validator.setDefaults({
		errorElement: "span",
		errorClass: "help-block",
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		errorPlacement: function (error, element) {
			if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
			error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});	
	
	$('.tariffCSS').keyup(function() {  		   
			if (this.value.match(/[^A-Z0-9 ]/g)) {
				this.value = this.value.replace(/[^A-Z0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^A-Z0-9 ]/g, '')); 
					}, 5);  
            });	 
			
	 $('.tariffSpace').keydown(function(e) {  			
			 if (e.ctrlKey || e.altKey) {
				e.preventDefault();
			} else {
				var key = e.keyCode;
				//alert(key);
				if (key == 32) {
				e.preventDefault();
				}
			}
      });	  
	  
	
	</script>