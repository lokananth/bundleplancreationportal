<style type="text/css">	
#dt_basic_length{float:right;}
#dt_basic_wrapper .DTTT.btn-group{float:right; margin-left:10px;  margin-right:10px;}
.smart-style-5 div.DTTT .btn{margin:0px;}
@media only screen and (min-width : 480px)
{
#dt_basic_filter{float: left;width: 50%;}
}

.loader {
  width: 3em;
  height: 3em;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  border-radius: 50%;
  box-shadow: 0 0 5px #444;
 
}
.loader:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: linear-gradient(#009EE3 , black 90%);
  animation: spin .5s infinite linear;
}
.loader:after {
  content: "";
  position: absolute;
  width: 90%;
  height: 90%;
  top: 5%;
  left:5%;
  background-color:#fff;
  border-radius: 50%;
  box-shadow: inset 0 0 5px #444;
}

@keyframes spin {
  to {
    transform: rotate(360deg);
  }
}
.load-bg{
    position:absolute;
    top:0;
    left:0;   
    width:100%;
    height:100%;
    
    z-index:99999;
    display:block;
}
.state-error{ color:#ff0000;}
.datepicker + .invalid {
    left: 0;
    position: absolute;
    top: 100%;
}
.modal-body{padding:0 20px;}
.loader1{position:relative; float:left; min-height:200px; width:100%;} 

label input.checkbox[type="checkbox"] + span{float: right; position:absolute;top:0;left:0;}
table.complete-compare thead tr th,table.complete-compare tbody tr td{white-space: nowrap; !important;}
#changeUpdate{height:200px; resize: none;}
</style>
<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>
<?php
if(isset($_SESSION['BundleStep2']['sendingMode']) && $_SESSION['BundleStep2']['sendingMode']!='') { 
	$varSendingMode = $_SESSION['BundleStep2']['sendingMode'];
}else {
	$varSendingMode = '';
}
/*if(isset($_SESSION['BundleStep2']['smsSender']) && $_SESSION['BundleStep2']['smsSender']!='') { 
	$varSmsSender = $_SESSION['BundleStep2']['smsSender'];
}else {
	$varSmsSender = '';
}
if(isset($_SESSION['BundleStep2']['smsSenderType']) && $_SESSION['BundleStep2']['smsSenderType']!='') { 
	$varSmsSenderType = $_SESSION['BundleStep2']['smsSenderType'];
}else {
	$varSmsSenderType = '';
}
if(isset($_SESSION['BundleStep2']['smsAccountId']) && $_SESSION['BundleStep2']['smsAccountId']!='') { 
	$varSmsAccountId = $_SESSION['BundleStep2']['smsAccountId'];
}else {
	$varSmsAccountId = '';
}
if(isset($_SESSION['BundleStep2']['smsURL']) && $_SESSION['BundleStep2']['smsURL']!='') { 
	$varSmsURL = $_SESSION['BundleStep2']['smsURL'];
}else {
	$varSmsURL = '';
}
if(isset($_SESSION['BundleStep2']['ussdURL']) && $_SESSION['BundleStep2']['ussdURL']!='') { 
	$varUssdURL = $_SESSION['BundleStep2']['ussdURL'];
}else {
	$varUssdURL = '';
}*/
if(isset($_SESSION['BundleStep2']['paymentFlag']) && $_SESSION['BundleStep2']['paymentFlag']!='') { 
	$varPaymentFlag = $_SESSION['BundleStep2']['paymentFlag'];
}else {
	$varPaymentFlag = '';
}

if(isset($_SESSION['BundleStep2']['topUpFlag']) && $_SESSION['BundleStep2']['topUpFlag']!='') { 
	$varTopUpFlag = $_SESSION['BundleStep2']['topUpFlag'];
}else {
	$varTopUpFlag = '';
}
if(isset($_SESSION['BundleStep2']['serviceFlag']) && $_SESSION['BundleStep2']['serviceFlag']!='') { 
	$varServiceFlag = $_SESSION['BundleStep2']['serviceFlag'];
}else {
	$varServiceFlag = '';
}
if(isset($_SESSION['BundleStep2']['mainBGroupId']) && $_SESSION['BundleStep2']['mainBGroupId']!='') { 
	$varMainBGroupId = $_SESSION['BundleStep2']['mainBGroupId'];
}else {
	$varMainBGroupId = '';
}
if(isset($_SESSION['BundleStep2']['familyGroupId']) && $_SESSION['BundleStep2']['familyGroupId']!='') { 
	$varFamilyGroupId = $_SESSION['BundleStep2']['familyGroupId'];
}else {
	$varFamilyGroupId = '';
}

if(isset($_SESSION['BundleStep2']['autoStopMode']) && $_SESSION['BundleStep2']['autoStopMode']!='') { 
	$varAutoStopMode = $_SESSION['BundleStep2']['autoStopMode'];
}else {
	$varAutoStopMode = '0';
}
if(isset($_SESSION['BundleStep2']['autoStopDelay']) && $_SESSION['BundleStep2']['autoStopDelay']!='') { 
	$varAutoStopDelay = $_SESSION['BundleStep2']['autoStopDelay'];
}else {
	$varAutoStopDelay = '0';
}
if(isset($_SESSION['BundleStep2']['resetMode']) && $_SESSION['BundleStep2']['resetMode']!='') { 
	$varResetMode = $_SESSION['BundleStep2']['resetMode'];
}else {
	$varResetMode = '';
}
if(isset($_SESSION['BundleStep2']['incentiveFlag']) && $_SESSION['BundleStep2']['incentiveFlag']!='') { 
	$varIncentiveFlag = $_SESSION['BundleStep2']['incentiveFlag'];
}else {
	$varIncentiveFlag = '';
}
if(isset($_SESSION['BundleStep2']['sendMsgFlag']) && $_SESSION['BundleStep2']['sendMsgFlag']!='') { 
	$varSendMsgFlag = $_SESSION['BundleStep2']['sendMsgFlag'];
}else {
	$varSendMsgFlag = '';
}
if(isset($_SESSION['BundleStep2']['prorataCharging']) && $_SESSION['BundleStep2']['prorataCharging']!='') { 
	$varProrataCharging = $_SESSION['BundleStep2']['prorataCharging'];
}else {
	$varProrataCharging = '';
}
if(isset($_SESSION['BundleStep2']['airTimeFlag']) && $_SESSION['BundleStep2']['airTimeFlag']!='') { 
	$varAirTimeFlag = $_SESSION['BundleStep2']['airTimeFlag'];
}else {
	$varAirTimeFlag = '';
}
/*if(isset($_SESSION['BundleStep2']['bundleSource']) && $_SESSION['BundleStep2']['bundleSource']!='') { 
	$varBundleSource = $_SESSION['BundleStep2']['bundleSource'];
}else {
	$varBundleSource = '';
}*/
if(isset($_SESSION['BundleStep2']['bundleGroupId']) && $_SESSION['BundleStep2']['bundleGroupId']!='') { 
	$varBundleGroupId = $_SESSION['BundleStep2']['bundleGroupId'];
}else {
	$varBundleGroupId = '';
}
if(isset($_SESSION['BundleStep2']['familyGroupMax']) && $_SESSION['BundleStep2']['familyGroupMax']!='') { 
	$varFamilyGroupMax = $_SESSION['BundleStep2']['familyGroupMax'];
}else {
	$varFamilyGroupMax = '0';
}
if(isset($_SESSION['BundleStep2']['bundleGroupIdBan']) && $_SESSION['BundleStep2']['bundleGroupIdBan']!='') { 
	$varBundleGroupIdBan = $_SESSION['BundleStep2']['bundleGroupIdBan'];
}else {
	$varBundleGroupIdBan = '';
}
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Bundle Plans
        <small>Add new bundle</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Enroll Data</a></li-->
        <li class="active">Create new bundle</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">ADD NEW BUNDLES</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="row">
						<!--  Add New Device Form  -->
						<div class="col-sm-12 col-md-7 col-xs-12">
							<form class="form-horizontal" action="<?php echo base_url();?>bundleplan/createBundleStep3" id="createBundleForm2" method="post">	
								<div id="hardware">
									<!--div class="form-group">
										<label class="col-sm-4 col-xs-12">Bundle ID</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="test" />
										</div>
									</div-->
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Notification Sending Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="sendingMode" id="sendingMode">
												<option value="">Select</option>
												 <?php foreach($arrSendMessageMode as $arrResult){ ?>
												 <option <?php if($varSendingMode!='') if($varSendingMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<!--div class="form-group">
										<div class="col-md-12 col-xs-12">
											<p class="form-control-static"><strong>SMS Notification</strong></p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">SMS Sender<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varSmsSender;?>" placeholder="SMS Sender" name="smsSender" id="smsSender" maxlength="80" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">SMS Sender Type</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varSmsSenderType;?>" placeholder="SMS Sender Type" name="smsSenderType" id="smsSenderType" maxlength="80" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">SMS Account ID</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varSmsAccountId;?>" placeholder="SMS Account ID" name="smsAccountId" id="smsAccountId" maxlength="80" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">SMS URL<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varSmsURL;?>" placeholder="SMS URL" name="smsURL" id="smsURL" maxlength="100" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">USSD URL</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varUssdURL;?>" placeholder="USSD URL" name="ussdURL" id="ussdURL" maxlength="100" autocomplete="off" />
										</div>
									</div-->
									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Payment Flag</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="paymentFlag" id="paymentFlag">
												<option value="">Select</option>
												 <?php foreach($arrPaymentFlag as $arrResult){ ?>
												 <option <?php if($varPaymentFlag!='') if($varPaymentFlag==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>											
										</div>
									</div>									
									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Top Up Flag</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="topUpFlag" id="topUpFlag">
												<option value="">Select</option>
												 <?php foreach($arrTopUpFlag as $arrResult){ ?>
												 <option <?php if($varTopUpFlag!='') if($varTopUpFlag==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Service Flag</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="serviceFlag" id="serviceFlag">
												<option value="">Select</option>
												 <?php foreach($arrServiceFlag as $arrResult){ ?>
												 <option <?php if($varServiceFlag!='') if($varServiceFlag==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Main Bundle Group ID</label>
										<div class="col-sm-7 col-md-7 col-xs-9">
											<select class="form-control" name="mainBGroupId" id="mainBGroupId">
												<option value="">Select</option>
												 <?php foreach($arrMainBundleGroupId as $arrResult){ ?>
												 <option <?php if($varMainBGroupId!='') if($varMainBGroupId==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Refid']; ?></option>
													<?php } ?>	
											</select>
										</div>
										<div class="col-sm-1 col-md-1 col-xs-3">
											<button type="button" id="plusBundleButton" data-toggle="modal" data-target="#myBundleModal"" class="btn btn-info plusBundleGroupIdButton">
												<i class="fa fa-plus"></i>
											</button>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Family Group ID</label>
										<div class="col-sm-7 col-md-7 col-xs-9">
											<select class="form-control" name="familyGroupId" id="familyGroupId">
												<option value="">Select</option>
												 <?php foreach($arrMainBundleGroupId as $arrResult){ ?>
												 <option <?php if($varFamilyGroupId!='') if($varFamilyGroupId==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Refid']; ?></option>
													<?php } ?>	
											</select>
										</div>
										<div class="col-sm-1 col-md-1 col-xs-3">
											<button type="button" id="plusBundleButton" data-toggle="modal" data-target="#myBundleModal"" class="btn btn-info plusBundleGroupIdButton">
												<i class="fa fa-plus"></i>
											</button>
										</div>
									</div>								
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Auto Stop Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varAutoStopMode;?>" placeholder="Auto Stop Mode" name="autoStopMode" id="autoStopMode" maxlength="25" autocomplete="off" />
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Auto Stop Delay</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varAutoStopDelay;?>" placeholder="Auto Stop Delay" name="autoStopDelay" id="autoStopDelay" maxlength="25" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Reset Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="resetMode" id="resetMode">
												<option value="">Select</option>
												<option value="1" <?php if($varResetMode!='') if($varResetMode=='1'){ ?> selected <?php } ?>>Yes</option>
												<option value="0" <?php if($varResetMode!='') if($varResetMode=='0'){ ?> selected <?php } ?>>No</option>
											</select>											
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Incentive Flag</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="incentiveFlag" id="incentiveFlag">
												<option value="">Select</option>
												<option value="1" <?php if($varIncentiveFlag!='') if($varIncentiveFlag=='1'){ ?> selected <?php } ?>>Yes</option>
												<option value="0" <?php if($varIncentiveFlag!='') if($varIncentiveFlag=='0'){ ?> selected <?php } ?>>No</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Send Message Flag</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="sendMsgFlag" id="sendMsgFlag">
												<option value="">Select</option>
												<option value="1" <?php if($varSendMsgFlag!='') if($varSendMsgFlag=='1'){ ?> selected <?php } ?>>Yes</option>
												<option value="0" <?php if($varSendMsgFlag!='') if($varSendMsgFlag=='0'){ ?> selected <?php } ?>>No</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Prorata Charging</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="prorataCharging" id="prorataCharging">
												<option value="">Select</option>
												<option value="1" <?php if($varProrataCharging!='') if($varProrataCharging=='1'){ ?> selected <?php } ?>>Yes</option>
												<option value="0" <?php if($varProrataCharging!='') if($varProrataCharging=='0'){ ?> selected <?php } ?>>No</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Airtime Flag</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="airTimeFlag" id="airTimeFlag">
												<option value="">Select</option>
												<option value="1" <?php if($varAirTimeFlag!='') if($varAirTimeFlag=='1'){ ?> selected <?php } ?>>Yes</option>
												<option value="0" <?php if($varAirTimeFlag!='') if($varAirTimeFlag=='0'){ ?> selected <?php } ?>>No</option>
											</select>
										</div>
									</div>																	
									<!--div class="form-group">
										<label class="col-sm-4 col-xs-12">Source</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varBundleSource;?>" placeholder="Source" name="bundleSource" id="bundleSource" maxlength="100" autocomplete="off" />
										</div>
									</div-->
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Bundle Group ID</label>
										<div class="col-sm-7 col-md-7 col-xs-9">
											<select class="form-control" name="bundleGroupId" id="bundleGroupId">
												<option value="">Select</option>
												 <?php foreach($arrMainBundleGroupId as $arrResult){ ?>
												 <option <?php if($varBundleGroupId!='') if($varBundleGroupId==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Refid']; ?></option>
													<?php } ?>	
											</select>
										</div>
										<div class="col-sm-1 col-md-1 col-xs-3">
											<button type="button" id="plusBundleButton" data-toggle="modal" data-target="#myBundleModal"" class="btn btn-info plusBundleGroupIdButton">
												<i class="fa fa-plus"></i>
											</button>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Family Group Maximum CTR</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varFamilyGroupMax;?>" placeholder="Family Group Maximum CTR" name="familyGroupMax" id="familyGroupMax" maxlength="25" autocomplete="off" />											
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Bundle Group Id Ban</label>
										<div class="col-sm-7 col-md-7 col-xs-9">
											<select class="form-control" name="bundleGroupIdBan" id="bundleGroupIdBan">
												<option value="">Select</option>
												 <?php foreach($arrMainBundleGroupId as $arrResult){ ?>
												 <option <?php if($varBundleGroupIdBan!='') if($varBundleGroupIdBan==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Refid']; ?></option>
													<?php } ?>	
											</select>
										</div>
										<div class="col-sm-1 col-md-1 col-xs-3">
											<button type="button" id="plusBundleButton" data-toggle="modal" data-target="#myBundleModal"" class="btn btn-info plusBundleGroupIdButton">
												<i class="fa fa-plus"></i>
											</button>
										</div>
									</div>		
									<div class="form-group">
										<label class="col-sm-4 col-xs-12 hidden-xs">&nbsp;</label>
										<div class="col-sm-7 col-md-7 col-xs-12">									
											<button type="button" id="step2BackButton" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp;Back</button>
											<button type="submit" class="btn btn-info">Next &nbsp;<i class="fa fa-arrow-right"></i></button>
										</div>
									</div>								
								</div>
							</form>
						</div>
					</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<!--  Modal Open  --->
<div class="modal fade" id="myBundleModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Get Bundle Group Information</h4>
      </div>
      <div class="modal-body">
        <div class="row">
			<!--  Add New Device Form  -->
			<div class="col-sm-12 col-md-12 col-xs-12">
				<form class="form-horizontal" id="searchBundleGroupForm" name="searchBundleGroupForm">		
					<div class="form-group">
						<label class="col-sm-3 col-xs-12 control-label">Bundle GroupId</label>
						<div class="col-sm-5 col-md-5 col-xs-9">
							<input type="text" class="form-control" value="" placeholder="Bundle GroupId" id="getBundleGroupId" name="getBundleGroupId" autocomplete="off" maxlength="15" />
						</div>
						<div class="col-sm-3 col-xs-9">
							<button type="submit" class="btn btn-info">Search</button>
						</div>
					</div>															
				</form>
			</div>
			<div class="col-sm-12 col-md-12 col-xs-12" id="resultBundleTableDiv" style="display:none;">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Heading 1</th>
							<th>Heading 2</th>
							<th>Heading 3</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>datat 1</td>
							<td>datat 2</td>
							<td>datat 3</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal End -->



 <?php //include('footer.php'); ?>

<script> 
 /*function onLoadReport(){
	$('#reportDiv').html('<div class="load-bg"><div class="loader"></div></div>');		
	var operatorId='';
	var reportType='';
	var usageType='';
	var startDate='';
	var endDate='';
	getAllOperatorProfitLossReports(operatorId,reportType,usageType,startDate,endDate);	
 }
	onLoadReport();*/
  
  $(document).ready(function() {
		 //alert('hai');	 	 
		 $('#step2BackButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundleplan/createNewBundle';
		 });

		$("#createBundleForm2").validate({			
			rules: {
				//sendingMode: "required",
				//smsSender: "required",
				//smsSenderType: "required",
				//smsAccountId: "required",
				//smsURL: "required",
				//ussdURL: "required",
				//paymentFlag: "required",
				//paymentDelay: "required",
				//dateMode: "required",
				//topUpFlag: "required",
				//serviceFlag: "required",
				//mainBGroupId: "required",
				//familyGroupId: "required",
				//tariffClassGroupId: "required",
				//renewDateMode: "required",				
			},
			messages: {
				//sendingMode: "Please select a notification sending mode",
				//smsSender: "Please enter a sms sender",
				//smsSenderType: "Please enter a sms sender type",
				//smsAccountId: "Please enter a sms account Id",
				//smsURL: "Please enter a sms URL",
				//ussdURL: "Please enter a ussd URL",
				//paymentFlag: "Please select a payment flag",
				//paymentDelay: "Please enter a payment delay",
				//dateMode: "Please select a date mode",
				//topUpFlag: "Please select a top up flag",
				//serviceFlag: "Please select a service flag",
				//mainBGroupId: "Please select a main bundle group Id",
				//familyGroupId: "Please select a family group Id",
				//tariffClassGroupId: "Please select a tariff class group Id",
				//renewDateMode: "Please select a renew date mode",					
			}			
		});	
		
		 $("#searchBundleGroupForm").validate({
			submitHandler : function(form){	
				$("#resultBundleTableDiv").show();
				$('#resultBundleTableDiv').html('<div class="load-bg"><div class="loader"></div></div>');
				var getBundleGroupId = $("#getBundleGroupId").val();									
				var dataString = 'getBundleGroupId='+getBundleGroupId;	
				 $.ajax({
					   type: "POST",
					   url: "<?php echo base_url(); ?>bundleplan/getBundleGroupInfo",
					   data: dataString,
					   //dataType: 'json',
					   success: function(data){ 
							//alert(data);
							$("#resultBundleTableDiv").html(data);							
					   }
				 });	
				//form.submit();				
			},
			rules: {
				getBundleGroupId: "required",					
			},
			messages: {
				getBundleGroupId: "Please enter a bundle group id",					
			}			
		});		
					
	  
	  /*$("#sendingMode").blur(function(){
		  $("#sendingMode").valid();
	  });		*/
	  
	  /*$("#smsSender").blur(function(){
		  $("#smsSender").valid();
	  });		*/
	  
	  /*$("#smsSenderType").blur(function(){
		  $("#smsSenderType").valid();
	  });		
	  
	  $("#smsAccountId").blur(function(){
		  $("#smsAccountId").valid();
	  });	*/
	  
	  /*$("#smsURL").blur(function(){
		  $("#smsURL").valid();
	  });		*/
	  
	  /*$("#ussdURL").blur(function(){
		  $("#ussdURL").valid();
	  });		
	  
	  $("#paymentFlag").blur(function(){
		  $("#paymentFlag").valid();
	  });*/
	  
	 /* $("#paymentDelay").blur(function(){
		  $("#paymentDelay").valid();
	  });		
	  
	  $("#dateMode").blur(function(){
		  $("#dateMode").valid();
	  });		*/
	  
	  /*$("#topUpFlag").blur(function(){
		  $("#topUpFlag").valid();
	  });
		
	  $("#serviceFlag").blur(function(){
		  $("#serviceFlag").valid();
	  });		

	  $("#mainBGroupId").blur(function(){
		  $("#mainBGroupId").valid();
	  });			
	  
	  $("#familyGroupId").blur(function(){
		  $("#familyGroupId").valid();
	  });*/
	  
	 /* $("#tariffClassGroupId").blur(function(){
		  $("#tariffClassGroupId").valid();
	  });
	  
	  $("#renewDateMode").blur(function(){
		  $("#renewDateMode").valid();
	  });*/
    
  });
  
  $.validator.setDefaults({
		errorElement: "span",
		errorClass: "help-block",
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		errorPlacement: function (error, element) {
			if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
			error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});
	
	$(".plusBundleGroupIdButton").click(function(){
		  $("#resultBundleTableDiv").html('');
		  $("#getBundleGroupId").val('');
	  });	
	
	/*$('#bundleName, #bundleDesc,  #bundleSource').keyup(function() {  		   
			if (this.value.match(/[^a-zA-Z0-9 ]/g)) {
				this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^a-zA-Z0-9 ]/g, '')); 
					}, 5);  
            });	 */
			
	$('#paymentDelay, #autoStopMode, #autoStopDelay, #resetMode, #getBundleGroupId, #familyGroupMax').keyup(function() {  		   
			if (this.value.match(/[^0-9 ]/g)) {
				this.value = this.value.replace(/[^0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^0-9 ]/g, '')); 
					}, 5);  
            });	 

	/*$('.amount').keypress(function(event) {
            if(event.which == 8 || event.which == 0){
                return true;
            }
            if(event.which < 46 || event.which > 59) {
                return false;
                //event.preventDefault();
            } // prevent if not number/dot

            if(event.which == 46 && $(this).val().indexOf('.') != -1) {
                return false;
                //event.preventDefault();
            } // prevent if already dot
			
			if ($(this).val().indexOf('.') > 0) {
				var len = $(this).val().length;
				var charAfterdot = (len + 1) - $(this).val().indexOf('.');
				if (charAfterdot > 3) {
					return false;
				}
			}
			
        });*/
  
	</script>