<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Bundle Plans
        <small>Add new bundle</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Enroll Data</a></li-->
        <li class="active">Create new bundle</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">ADD NEW BUNDLES</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="row">
						<!--  Add New Device Form  -->
						<div class="col-sm-12 col-md-7 col-xs-12">
							<form class="form-horizontal" action="<?php echo base_url();?>bundleplan/addBundleSteps" id="createBundleForm3" method="post">	
								<div id="hardware">
									<!--div class="form-group">
										<label class="col-sm-4 col-xs-12">Bundle ID</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="test" />
										</div>
									</div-->
									<div class="form-group">
										<div class="col-md-12 col-xs-12">
											<p class="form-control-static"><strong>Limit Balance Amount</strong></p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Limit Balance Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="limitBalanceMode" id="limitBalanceMode">
												<option value="">Select</option>
												 <?php foreach($arrLimitBalanceMode as $arrResult){ ?>
												 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Limit Balance Amount</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control amount" value="" placeholder="Limit Balance Amount" name="limitBalanceAmount" id="limitBalanceAmount" maxlength="25" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Limit Balance Start Date Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="limitBalanceStartDateMode" id="limitBalanceStartDateMode">
												<option value="">Select</option>
												 <?php foreach($arrLimitBalanceStartDateMode as $arrResult){ ?>
												 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<!--div class="form-group">
										<label class="col-sm-4 col-xs-12">Limit Balance Start Date</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="Limit Balance Start Date" name="limitBalanceStartDate" id="limitBalanceStartDate" maxlength="25" autocomplete="off" />
										</div>
									</div-->
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Limit Balance Start Date</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<div class="input-group date" id="datepicker">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" class="form-control pull-right" value="" id="limitBalanceStartDate" name="limitBalanceStartDate" readonly />
											</div>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">DISC Price</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control amount" value="" placeholder="DISC Price" name="discPrice" id="discPrice" maxlength="25" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">DISC Start Date Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="discStartDateMode" id="discStartDateMode">
												<option value="">Select</option>
												 <?php foreach($arrDiscStartDateMode as $arrResult){ ?>
												 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<!--div class="form-group">
										<label class="col-sm-4 col-xs-12">DISC Start Date</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="DISC Start Date" name="discStartDate" id="discStartDate" maxlength="25" autocomplete="off" />
										</div>
									</div-->
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">DISC Start Date</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<div class="input-group date" id="datepicker">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" class="form-control pull-right" value="" id="discStartDate" name="discStartDate" readonly />
											</div>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">DISC Delay Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="discDelayMode" id="discDelayMode">
												<option value="">Select</option>
												 <?php foreach($arrDiscDelayMode as $arrResult){ ?>
												 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">DISC Delay</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="30" placeholder="DISC Delay" name="discDelay" id="discDelay" maxlength="25" autocomplete="off" />
										</div>(30 days)
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Promo ID</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="0" placeholder="Promo ID" name="promoId" id="promoId" maxlength="25" autocomplete="off" />
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Saver ID</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="0" placeholder="Saver ID" name="saverId" id="saverId" maxlength="25" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">First Update From</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<div class="input-group date" id="datepicker1">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" class="form-control pull-right" value="" id="firstUpdateFrom" name="firstUpdateFrom" readonly />
											</div>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">First Update To</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<div class="input-group date" id="datepicker3">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" class="form-control pull-right" value="" id="firstUpdateTo" name="firstUpdateTo" readonly />
											</div>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Saver Ban</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="saverBan" id="saverBan">
												<option value="">Select</option>
												<option value="1">Yes</option>
												<option value="0">No</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Package Group ID Disable</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="packageGroupId" id="packageGroupId">
												<option value="">Select</option>
												 <?php foreach($arrPackageGroupId as $arrResult){ ?>
												 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Refid']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12 hidden-xs">&nbsp;</label>
										<div class="col-sm-7 col-md-7 col-xs-12">									
											<button type="button" id="step3BackButton" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp;Back</button>
											<!--button type="button" class="btn btn-info">Confirm</button-->
											<!--button type="button" class="btn btn-info">Save</button-->
											<button type="submit" class="btn btn-info">Add</button>
											<button type="button" id="step3CancelButton" class="btn btn-danger">Cancel</button>
										</div>
									</div>																																
								</div>
							</form>
						</div>
					</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<!--  Modal Open  --->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Product Information</h4>
      </div>
      <div class="modal-body">
        <div class="row">
			<!--  Add New Device Form  -->
			<div class="col-sm-12 col-md-12 col-xs-12">
				<form class="form-horizontal" action="">		
					<div class="form-group">
						<label class="col-sm-4 col-xs-12">Add Product Name</label>
						<div class="col-sm-7 col-md-7 col-xs-12">
							<input type="text" class="form-control" value="" placeholder="test" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 col-xs-12">Add Product Code</label>
						<div class="col-sm-7 col-md-7 col-xs-12">
							<input type="text" class="form-control" value="" placeholder="test" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 col-xs-12">Select category</label>
						<div class="col-sm-7 col-md-7 col-xs-12">
							<label class="radio-inline">
							<input type="radio" class="radio" value="" placeholder="test" /> Hardware
							</label>
							<label class="radio-inline">
							<input type="radio" class="radio" value="" placeholder="test" /> Software
							</label>
						</div>
					</div>															
				</form>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-info">SAVE</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal End -->

 <?php //include('footer.php'); ?>

<script> 
 /*function onLoadReport(){
	$('#reportDiv').html('<div class="load-bg"><div class="loader"></div></div>');		
	var operatorId='';
	var reportType='';
	var usageType='';
	var startDate='';
	var endDate='';
	getAllOperatorProfitLossReports(operatorId,reportType,usageType,startDate,endDate);	
 }
	onLoadReport();*/
  
  $(document).ready(function() {
		 //alert('hai');	 	 
		 $('#step3BackButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundleplan/createBundleStep2';
		 });
		 step3CancelButton
		 $('#step3CancelButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundleplan';
		 });
		 

		/*$("#createBundleForm1").validate({			
			rules: {
				//sendingMode: "required",
				smsSender: "required",
				//smsSenderType: "required",
				//smsAccountId: "required",
				smsURL: "required",
				//ussdURL: "required",
				//paymentFlag: "required",
				paymentDelay: "required",
				dateMode: "required",
				//topUpFlag: "required",
				//serviceFlag: "required",
				//mainBGroupId: "required",
				//familyGroupId: "required",
				tariffClassGroupId: "required",
				renewDateMode: "required",				
			},
			messages: {
				//sendingMode: "Please select a notification sending mode",
				smsSender: "Please enter a sms sender",
				//smsSenderType: "Please enter a sms sender type",
				//smsAccountId: "Please enter a sms account Id",
				smsURL: "Please enter a sms URL",
				//ussdURL: "Please enter a ussd URL",
				//paymentFlag: "Please select a payment flag",
				paymentDelay: "Please enter a payment delay",
				dateMode: "Please select a date mode",
				//topUpFlag: "Please select a top up flag",
				//serviceFlag: "Please select a service flag",
				//mainBGroupId: "Please select a main bundle group Id",
				//familyGroupId: "Please select a family group Id",
				tariffClassGroupId: "Please select a tariff class group Id",
				renewDateMode: "Please select a renew date mode",					
			}			
		});		*/	
					
	      
  });
  
  /*$.validator.setDefaults({
		errorElement: "span",
		errorClass: "help-block",
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		errorPlacement: function (error, element) {
			if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
			error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});	*/
	
	/*$('#discStartDate,  #discDelay').keyup(function() {  		   
			if (this.value.match(/[^a-zA-Z0-9 ]/g)) {
				this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^a-zA-Z0-9 ]/g, '')); 
					}, 5);  
          });	 */
			
	$('#limitBalanceStartDate, #discStartDate, #discDelay, #promoId, #saverId').keyup(function() {  		   
			if (this.value.match(/[^0-9 ]/g)) {
				this.value = this.value.replace(/[^0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^0-9 ]/g, '')); 
					}, 5);  
            });	 

		$('.amount').keypress(function(event) {
            if(event.which == 8 || event.which == 0){
                return true;
            }
            if(event.which < 46 || event.which > 59) {
                return false;
                //event.preventDefault();
            } // prevent if not number/dot

            if(event.which == 46 && $(this).val().indexOf('.') != -1) {
                return false;
                //event.preventDefault();
            } // prevent if already dot
			
			if ($(this).val().indexOf('.') > 0) {
				var len = $(this).val().length;
				var charAfterdot = (len + 1) - $(this).val().indexOf('.');
				if (charAfterdot > 3) {
					return false;
				}
			}
			
        });
  
	</script>