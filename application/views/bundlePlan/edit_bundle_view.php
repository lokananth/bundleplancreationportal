<style type="text/css">	
#dt_basic_length{float:right;}
#dt_basic_wrapper .DTTT.btn-group{float:right; margin-left:10px;  margin-right:10px;}
.smart-style-5 div.DTTT .btn{margin:0px;}
@media only screen and (min-width : 480px)
{
#dt_basic_filter{float: left;width: 50%;}
}

.loader {
  width: 3em;
  height: 3em;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  border-radius: 50%;
  box-shadow: 0 0 5px #444;
 
}
.loader:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: linear-gradient(#009EE3 , black 90%);
  animation: spin .5s infinite linear;
}
.loader:after {
  content: "";
  position: absolute;
  width: 90%;
  height: 90%;
  top: 5%;
  left:5%;
  background-color:#fff;
  border-radius: 50%;
  box-shadow: inset 0 0 5px #444;
}

@keyframes spin {
  to {
    transform: rotate(360deg);
  }
}
.load-bg{
    position:absolute;
    top:0;
    left:0;   
    width:100%;
    height:100%;
    
    z-index:99999;
    display:block;
}
.state-error{ color:#ff0000;}
.datepicker + .invalid {
    left: 0;
    position: absolute;
    top: 100%;
}
.modal-body{padding:0 20px;}
.loader1{position:relative; float:left; min-height:200px; width:100%;} 

label input.checkbox[type="checkbox"] + span{float: right; position:absolute;top:0;left:0;}
table.complete-compare thead tr th,table.complete-compare tbody tr td{white-space: nowrap; !important;}
#changeUpdate{height:200px; resize: none;}
</style>
<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>
<?php
//echo '<pre>';print_r($arrGetBundleInfoRes);exit; 
if(isset($_SESSION['EditBundleStep1']['bundleId']) && $_SESSION['EditBundleStep1']['bundleId']!='') { 
	$varBundleId = $_SESSION['EditBundleStep1']['bundleId'];
}else {
	$varBundleId = $arrGetBundleInfoRes[0]['Bundleid'];
}
if(isset($_SESSION['EditBundleStep1']['bundleName']) && $_SESSION['EditBundleStep1']['bundleName']!='') { 
	$varBundleName = $_SESSION['EditBundleStep1']['bundleName'];
}else {
	$varBundleName = $arrGetBundleInfoRes[0]['name'];
}
if(isset($_SESSION['EditBundleStep1']['bundlePrice']) && $_SESSION['EditBundleStep1']['bundlePrice']!='') { 
	$varBundlePrice = number_format((float) $_SESSION['EditBundleStep1']['bundlePrice'], 2, '.', '');	
}else {
	$varBundlePrice = number_format((float) $arrGetBundleInfoRes[0]['price'], 2, '.', '');
}
if(isset($_SESSION['EditBundleStep1']['bundleStatus']) && $_SESSION['EditBundleStep1']['bundleStatus']!='') { 
	$varBundleStatus = $_SESSION['EditBundleStep1']['bundleStatus'];
}else {
	$varBundleStatus = $arrGetBundleInfoRes[0]['status'];
}
if(isset($_SESSION['EditBundleStep1']['defaultFlag']) && $_SESSION['EditBundleStep1']['defaultFlag']!='') { 
	$varDefaultFlag = $_SESSION['EditBundleStep1']['defaultFlag'];
}else {
	$varDefaultFlag = $arrGetBundleInfoRes[0]['default_flag'];
}
if(isset($_SESSION['EditBundleStep1']['paymodeDefault']) && $_SESSION['EditBundleStep1']['paymodeDefault']!='') { 
	$varPaymodeDefault = $_SESSION['EditBundleStep1']['paymodeDefault'];
}else {
	$varPaymodeDefault = $arrGetBundleInfoRes[0]['paymode_default'];
}

if(isset($_SESSION['EditBundleStep1']['renewalDelay']) && $_SESSION['EditBundleStep1']['renewalDelay']!='') { 
	$varRenewalDelay = $_SESSION['EditBundleStep1']['renewalDelay'];
}else {
	$varRenewalDelay = ($arrGetBundleInfoRes[0]['renewal_delay']!='') ? $arrGetBundleInfoRes[0]['renewal_delay'] : '30';
}
if(isset($_SESSION['EditBundleStep1']['bundleDesc']) && $_SESSION['EditBundleStep1']['bundleDesc']!='') { 
	$varBundleDesc = $_SESSION['EditBundleStep1']['bundleDesc'];
}else {
	$varBundleDesc = $arrGetBundleInfoRes[0]['description'];
}
if(isset($_SESSION['EditBundleStep1']['maxDueDelay']) && $_SESSION['EditBundleStep1']['maxDueDelay']!='') { 
	$varMaxDueDelay = $_SESSION['EditBundleStep1']['maxDueDelay'];
}else {
	$varMaxDueDelay = ($arrGetBundleInfoRes[0]['maxduedelay']!='') ? $arrGetBundleInfoRes[0]['maxduedelay'] : '30';
}
if(isset($_SESSION['EditBundleStep1']['paymentDelay']) && $_SESSION['EditBundleStep1']['paymentDelay']!='') { 
	$varPaymentDelay = $_SESSION['EditBundleStep1']['paymentDelay'];
}else {
	$varPaymentDelay = ($arrGetBundleInfoRes[0]['payment_delay']!='') ? $arrGetBundleInfoRes[0]['payment_delay'] : '30';
}
if(isset($_SESSION['EditBundleStep1']['dateMode']) && $_SESSION['EditBundleStep1']['dateMode']!='') { 
	$varDateMode = $_SESSION['EditBundleStep1']['dateMode'];
}else {
	$varDateMode = $arrGetBundleInfoRes[0]['datemode'];
}
if(isset($_SESSION['EditBundleStep1']['tariffClassGroupId']) && $_SESSION['EditBundleStep1']['tariffClassGroupId']!='') { 
	$varTariffClassGroupId = $_SESSION['EditBundleStep1']['tariffClassGroupId'];
}else {
	$varTariffClassGroupId = $arrGetBundleInfoRes[0]['tariffclass_groupid'];
}
if(isset($_SESSION['EditBundleStep1']['renewDateMode']) && $_SESSION['EditBundleStep1']['renewDateMode']!='') { 
	$varRenewDateMode = $_SESSION['EditBundleStep1']['renewDateMode'];
}else {
	$varRenewDateMode = $arrGetBundleInfoRes[0]['renew_datemode'];
}
if(isset($_SESSION['EditBundleStep1']['renewalMode']) && $_SESSION['EditBundleStep1']['renewalMode']!='') { 
	$varRenewalMode = $_SESSION['EditBundleStep1']['renewalMode'];
}else {
	$varRenewalMode = $arrGetBundleInfoRes[0]['renewal_mode'];
}
?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Bundle Plans
        <small>Edit new bundle</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Enroll Data</a></li-->
        <li class="active">Edit bundle</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">EDIT BUNDLES</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="row">
						<!--  Add New Device Form  -->
						<div class="col-sm-12 col-md-7 col-xs-12">
							<form class="form-horizontal" action="<?php echo base_url();?>bundleplan/editBundleStep2" id="createBundleForm1" method="post">	
								<div id="hardware">
									<!--div class="form-group">
										<label class="col-sm-4 col-xs-12">Bundle ID</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="test" />
										</div>
									</div-->
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Bundle ID</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varBundleId;?>" placeholder="Bundle ID" name="bundleId" id="bundleId" maxlength="20" readonly autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Site Code</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $_SESSION['SiteCode']; ?>" placeholder="Site Code" name="siteCode" id="siteCode" maxlength="20" readonly autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Name<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varBundleName;?>" placeholder="Bundle Name" name="bundleName" id="bundleName" maxlength="50" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Price<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control amount" value="<?php echo $varBundlePrice;?>" placeholder="Bundle Price" name="bundlePrice" maxlength="6" id="bundlePrice" value="" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Currency</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $_SESSION['Currency']; ?>" placeholder="Site Code" name="currency" id="currency" maxlength="20" readonly autocomplete="off" />											
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Status<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="bundleStatus" id="bundleStatus">
												<option value="">Select</option>
												<option value="1" <?php if($varBundleStatus!='') if($varBundleStatus=='1'){ ?> selected <?php } ?>>Active</option>
												<option value="0" <?php if($varBundleStatus!='') if($varBundleStatus=='0'){ ?> selected <?php } ?>>Inactive</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Default Flag<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="defaultFlag" id="defaultFlag">
												<option value="">Select</option>
												<option value="1" <?php if($varDefaultFlag!='') if($varDefaultFlag=='1'){ ?> selected <?php } ?>>Active</option>
												<option value="0" <?php if($varDefaultFlag!='') if($varDefaultFlag=='0'){ ?> selected <?php } ?>>Inactive</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Paymode Default<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="paymodeDefault" id="paymodeDefault">
												<option value="">Select</option>
												 <?php foreach($arrPaymentModeDefault as $arrResult){ ?>
												 <option <?php if($varPaymodeDefault!='') if($varPaymodeDefault!='') if($varPaymodeDefault==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>								
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Renewal Delay<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varRenewalDelay;?>" placeholder="Renewal Delay" name="renewalDelay" id="renewalDelay" maxlength="50" autocomplete="off" />
										</div>(30 days)
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Description<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varBundleDesc;?>" placeholder="Bundle Description" name="bundleDesc" id="bundleDesc" maxlength="150" autocomplete="off" />
										</div>
									</div>
									<!--div class="form-group">
										<label class="col-sm-4 col-xs-12">Last Update</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="test" />
										</div>
									</div-->
									
									<!--div class="form-group">
										<label class="col-sm-4 col-xs-12">Maximum Due Delay<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<div class="input-group date" id="datepicker">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" class="form-control pull-right" value="<?php echo $varMaxDueDelay;?>" id="maxDueDelay" name="maxDueDelay" readonly />
											</div>
										</div>
									</div-->	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Maximum Due Delay<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varMaxDueDelay;?>" placeholder="Renewal Delay" name="maxDueDelay" id="maxDueDelay" maxlength="50" autocomplete="off" />
										</div>(30 days)
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Payment Delay<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varPaymentDelay;?>" placeholder="Payment Delay" name="paymentDelay" id="paymentDelay" maxlength="20" autocomplete="off" />
										</div>(30 days)
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Date Mode<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="dateMode" id="dateMode">
												<option value="">Select</option>
												 <?php foreach($arrDateMode as $arrResult){ ?>
												 <option <?php if($varDateMode!='') if($varDateMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Tariff Class Group ID<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-9">
											<select class="form-control" name="tariffClassGroupId" id="tariffClassGroupId">
												<option value="">Select</option>
												 <?php foreach($arrTariffClassGroupId as $arrResult){ ?>
												 <option <?php if($varTariffClassGroupId!='') if($varTariffClassGroupId==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Refid']; ?></option>
													<?php } ?>	
											</select>
										</div>
										<div class="col-sm-1 col-md-1 col-xs-3">
											<button type="button" id="plusButton" data-toggle="modal" data-target="#myModal"" class="btn btn-info">
												<i class="fa fa-plus"></i>
											</button>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Renew Date Mode<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="renewDateMode" id="renewDateMode">
												<option value="">Select</option>
												 <?php foreach($arrRenewDateMode as $arrResult){ ?>
												 <option <?php if($varRenewDateMode!='') if($varRenewDateMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Renewal Mode<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="renewalMode" id="renewalMode">
												<option value="">Select</option>
												 <?php foreach($arrRenewalMode as $arrResult){ ?>
												 <option <?php if($varRenewalMode!='') if($varRenewalMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12 hidden-xs">&nbsp;</label>
										<div class="col-sm-7 col-md-7 col-xs-12">									
											<button type="button" id="step1CancelButton" class="btn btn-default">Cancel</button>
											<button type="submit" class="btn btn-info">Next &nbsp;<i class="fa fa-arrow-right"></i></button>
										</div>
									</div>								
								</div>
							</form>
						</div>
					</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<!--  Modal Open  --->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Get Tariff Group Information</h4>
      </div>
      <div class="modal-body">
        <div class="row">
			<!--  Add New Device Form  -->
			<div class="col-sm-12 col-md-12 col-xs-12">
				<form class="form-horizontal" id="searchGroupForm" name="searchGroupForm">		
					<div class="form-group">
						<label class="col-sm-3 col-xs-12 control-label">Tariff GroupId</label>
						<div class="col-sm-5 col-md-5 col-xs-9">
							<input type="text" class="form-control" value="" placeholder="Tariff GroupId" id="tariffGroupId" name="tariffGroupId" autocomplete="off" maxlength="15" />
						</div>
						<div class="col-sm-3 col-xs-9">
							<button type="submit" class="btn btn-info">Search</button>
						</div>
					</div>															
				</form>
			</div>
			<div class="col-sm-12 col-md-12 col-xs-12" id="resultTableDiv" style="display:none;">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Heading 1</th>
							<th>Heading 2</th>
							<th>Heading 3</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>datat 1</td>
							<td>datat 2</td>
							<td>datat 3</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal End -->

 <?php //include('footer.php'); ?>

<script> 
 /*function onLoadReport(){
	$('#reportDiv').html('<div class="load-bg"><div class="loader"></div></div>');		
	var operatorId='';
	var reportType='';
	var usageType='';
	var startDate='';
	var endDate='';
	getAllOperatorProfitLossReports(operatorId,reportType,usageType,startDate,endDate);	
 }
	onLoadReport();*/
  
  $(document).ready(function() {
		 //alert('hai');	 	 
		 $('#step1CancelButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundleplan';
		 });

		$("#createBundleForm1").validate({			
			rules: {
				siteCode: "required",
				bundleName: "required",
				bundlePrice: "required",
				currency: "required",
				bundleStatus: "required",
				defaultFlag: "required",
				paymodeDefault: "required",
				//renewalMode: "required",
				renewalDelay: "required",
				bundleDesc: "required",
				//bundleSource: "required",
				maxDueDelay: "required",
				paymentDelay: "required",
				dateMode: "required",
				tariffClassGroupId: "required",
				renewDateMode: "required",
				renewalMode: "required",
				/*mobileNo: {
					required: true,
					number: true	
				},*/				
			},
			messages: {
				siteCode: "Please select a sitecode",
				bundleName: "Please enter a bundle name",
				bundlePrice: "Please enter a bundle price",
				currency: "Please enter a currency",
				bundleStatus: "Please select a bundle status",
				defaultFlag: "Please select a default flag",
				paymodeDefault: "Please select a default paymode",
				//renewalMode: "Please select a renewal mode",
				renewalDelay: "Please enter a renewal day",
				bundleDesc: "Please enter a bundle description",
				//bundleSource: "Please enter a bundle source",
				maxDueDelay: "Please select a maximum due delay",
				paymentDelay: "Please enter a payment delay",
				dateMode: "Please select a date mode",
				tariffClassGroupId: "Please select a tariff class group Id",
				renewDateMode: "Please select a renew date mode",	
				renewalMode: "Please select a renewal mode",	
				/*mobileNo: {
					required:"Please enter a mobile number",
					number: "Please enter only digits"
				},*/				
			}			
		});	
			
		$("#searchGroupForm").validate({
			submitHandler : function(form){	
				$("#resultTableDiv").show();
				$('#resultTableDiv').html('<div class="load-bg"><div class="loader"></div></div>');
				var tariffGroupId = $("#tariffGroupId").val();									
				var dataString = 'tariffGroupId='+tariffGroupId;	
				 $.ajax({
					   type: "POST",
					   url: "<?php echo base_url(); ?>bundleplan/getTariffGroupInfo",
					   data: dataString,
					   //dataType: 'json',
					   success: function(data){ 
							//alert(data);
							$("#resultTableDiv").html(data);							
					   }
				 });	
				//form.submit();				
			},
			rules: {
				tariffGroupId: "required",					
			},
			messages: {
				tariffGroupId: "Please enter a tariff group id",					
			}			
		});				
	  
	  $("#siteCode").blur(function(){
		  $("#siteCode").valid();
	  });		
	  
	  $("#bundleName").blur(function(){
		  $("#bundleName").valid();
	  });		
	  
	  $("#bundlePrice").blur(function(){
		  $("#bundlePrice").valid();
	  });		
	  
	  $("#currency").blur(function(){
		  $("#currency").valid();
	  });		
	  
	  $("#bundleStatus").blur(function(){
		  $("#bundleStatus").valid();
	  });		
	  
	  $("#defaultFlag").blur(function(){
		  $("#defaultFlag").valid();
	  });		
	  
	  $("#paymodeDefault").blur(function(){
		  $("#paymodeDefault").valid();
	  });		
	  
	  $("#renewalMode").blur(function(){
		  $("#renewalMode").valid();
	  });		
	  
	  $("#renewalDelay").blur(function(){
		  $("#renewalDelay").valid();
	  });		
	  
	  $("#bundleDesc").blur(function(){
		  $("#bundleDesc").valid();
	  });
		
	  $("#bundleSource").blur(function(){
		  $("#bundleSource").valid();
	  });		

	  $("#maxDueDelay").blur(function(){
		  $("#maxDueDelay").valid();
	  });	
		
	  $("#paymentDelay").blur(function(){
		  $("#paymentDelay").valid();
	  });		
	  
	  $("#dateMode").blur(function(){
		  $("#dateMode").valid();
	  });
		
	  $("#tariffClassGroupId").blur(function(){
		  $("#tariffClassGroupId").valid();
	  });
	  
	  $("#renewDateMode").blur(function(){
		  $("#renewDateMode").valid();
	  });		
	  
	  $("#renewalMode").blur(function(){
		  $("#renewalMode").valid();
	  });
    
  });
  
  $.validator.setDefaults({
		errorElement: "span",
		errorClass: "help-block",
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		errorPlacement: function (error, element) {
			if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
			error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});	
	
	$("#plusButton").click(function(){
		  $("#resultTableDiv").html('');
		  $("#tariffGroupId").val('');
	  });	
	
	$('#bundleName, #bundleDesc,  #bundleSource').keyup(function() {  		   
			if (this.value.match(/[^a-zA-Z0-9 ]/g)) {
				this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^a-zA-Z0-9 ]/g, '')); 
					}, 5);  
            });	 
			
	$('#renewalDelay, #maxDueDelay, #paymentDelay, #tariffGroupId').keyup(function() {  		   
			if (this.value.match(/[^0-9 ]/g)) {
				this.value = this.value.replace(/[^0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^0-9 ]/g, '')); 
					}, 5);  
            });	 

	$('.amount').keypress(function(event) {
            if(event.which == 8 || event.which == 0){
                return true;
            }
            if(event.which < 46 || event.which > 59) {
                return false;
                //event.preventDefault();
            } // prevent if not number/dot

            if(event.which == 46 && $(this).val().indexOf('.') != -1) {
                return false;
                //event.preventDefault();
            } // prevent if already dot
			
			if ($(this).val().indexOf('.') > 0) {
				var len = $(this).val().length;
				var charAfterdot = (len + 1) - $(this).val().indexOf('.');
				if (charAfterdot > 3) {
					return false;
				}
			}
			
        });
  
	</script>