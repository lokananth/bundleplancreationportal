<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Bundle Message
        <small>Add bundle message</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Device list</a></li-->
        <li class="active">Add Bundle Message</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Add Bundle Message</h3>
						<div class="box-tools pull-right">
							<!-- <span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="3 peoples are late">3</span> -->
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form class="form-horizontal" action="<?php echo base_url();?>bundlemessage/addBundleMessage" id="addBundleMessageForm" method="post">
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Bundle ID<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<select class="form-control" name="bundleId" id="bundleId">
											<option value="">Select</option>
											 <?php foreach($arrGetBundleInfoRes as $arrResult){ ?>
											 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name'] .'-'. $arrResult['Refid']; ?></option>
												<?php } ?>	
										</select>										
									</div>									
								</div>															
							</div>	
			<?php if(isset($arrGetBundleMessageTemplateInfo[0]['errcode']) && $arrGetBundleMessageTemplateInfo[0]['errcode']=='0'){ ?> 
			<table class="table table-striped table-bordered table-hover" width="100%">
				<thead>
					<tr>
						<th data-class="expand">MessageId</th>
						<th data-hide="phone">Comment</th>
						<th data-hide="phone">Message Text</th>	
					</tr>
				</thead>
				<tbody>
				<?php $i=0; foreach($arrGetBundleMessageTemplateInfo as $arrResult){  ?>	
				<input type="hidden" name="hidMessageId[]"  value="<?php echo $arrResult['messageid']; ?>" />
				<input type="hidden" name="hidComment[]"  value="<?php echo $arrResult['comment']; ?>" />
					<tr>
						<td><?php echo $arrResult['messageid']; ?></td>
						<td><?php echo $arrResult['comment']; ?></td>																
						<td><textarea class="sms_content" name="messageText[]"><?php echo $arrResult['message_text']; ?></textarea></td>
					</tr>
					<?php $i++; } ?>												
				</tbody>
			</table>
			<?php }else{ ?>	
			<br><br><b><center> No records found</center></b>
			<?php } ?>	
							<!--center><span id="validationError" style="display:none;color:"></span></center-->
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3 hidden-xs">&nbsp;</label>
									<div class="col-sm-5 col-md-5">
										<button type="submit" class="btn btn-primary">Add</button>
										<button type="button" id="editBundleMessageButton" class="btn btn-primary">Edit</button>
									</div>
								</div>
							</div>
						</form>							
					</div>
				</div>
			</div>    
        
       
      </div>
	  <div id="resultDiv">
	  <?php if($this->session->flashdata('successmsg')!=''){ ?>
				<div class="alert alert-success text-center" id="successmsg" >
				  <?php echo $this->session->flashdata('successmsg'); ?>
				  </div>
		<?php } ?>
		<?php if($this->session->flashdata('errormsg')!=''){ ?>
				<div class="alert alert-error text-center" id="errormsg" >
				  <?php echo $this->session->flashdata('errormsg'); ?>
				  </div>
		<?php } ?>
		</div>
      <!-- /.row -->
    </section>
    <!-- /.content -->	
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
		<!-- widget content -->
		<div class="widget-body no-padding" id="bundlesDiv">
			<?php if(isset($arrGetAllBundleMessageInfoRes[0]['errcode']) && $arrGetAllBundleMessageInfoRes[0]['errcode']=='0'){ ?> 
			<table id="datatable_tabletools" class="table table-bordered table-striped nowrap dt-responsive" width="100%">
				<thead>
					<tr>
						<th data-class="expand">BundleId</th>
						<th data-hide="phone">MessageId</th>
						<th data-hide="phone">Comment</th>
						<th data-hide="phone">MessageText</th>
						<!--th data-hide="phone,tablet">Comment</th-->
						<!--th data-hide="phone,tablet">Activation Flag</th-->											
					</tr>
				</thead>
				<tbody>
				<?php $i=1; foreach($arrGetAllBundleMessageInfoRes as $arrResult){  ?>	
					<tr>
						<td><?php echo $arrResult['bundleid']; ?></td>
						<td><?php echo $arrResult['messageid']; ?></td>
						<td><?php echo $arrResult['comment']; ?></td>
						<td style="max-width:250px;white-space: inherit;"><?php echo $arrResult['message_text']; ?></td>							
						<!--td><?php //echo $arrResult['comment']; ?></td-->
						<!--td><?php //echo $arrResult['activation_flag']; ?></td-->												
					</tr>
					<?php $i++; } ?>												
				</tbody>
			</table>
			<?php }else{ ?>	
			<br><br><b><center> No records found</center></b>
			<?php } ?>
		</div>
		<!-- end widget content -->

	</div>
	<!-- end widget div -->
  </div>
  </div>
  </div>
   </section>
  <!-- /.content-wrapper -->
  
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-1 col-sm-offset-1 col-md-8 col-md-offset-2 col-xs-12">
							<form class="form-horizontal" action="">
								<div class="form-group">
									<label class="col-sm-12 col-xs-12 text-center">Assign ICCID Number</label>
									<div class="col-sm-12 col-xs-12">
										<input type="text" class="form-control" value="" placeholder="ICCID Number" />
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-12 col-xs-12 text-center">
										<input type="submit" class="btn btn-primary" value="Reset" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>				
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">OK</button>				
				</div>
			</div>
		</div>
	</div>
 <?php //include('footer.php'); ?>
 
 
 
 <script> 
 /*function onLoadReport(){
	$('#reportDiv').html('<div class="load-bg"><div class="loader"></div></div>');		
	var operatorId='';
	var reportType='';
	var usageType='';
	var startDate='';
	var endDate='';
	getAllOperatorProfitLossReports(operatorId,reportType,usageType,startDate,endDate);	
 }
	onLoadReport();*/
  
  $(document).ready(function() {
		 //alert('hai');	 	 
		 $('#datatable_tabletools').DataTable();
		 $('#editBundleMessageButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundlemessage/getBundleMessageById';
		 });		 
		 
			
		$('#addBundleMessageForm').validate({			
			rules:{
				bundleId:{
					required : true
				},					
			},
			messages:{
				bundleId:"Please select the bundle Id",					
			}
		  });				
	  
	 /* $("#bundleId").blur(function(){
		  $("#bundleId").valid();
	  });	*/
	
	$('#bundleId').on('click', function() {	
		$("#errormsg").hide();
		$("#successmsg").hide();
	 });	
    
  });
  
  $.validator.setDefaults({
		errorElement: "span",
		errorClass: "help-block",
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		errorPlacement: function (error, element) {
			if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
			error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});		
  
	</script>