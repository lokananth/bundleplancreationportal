<style type="text/css">	
#dt_basic_length{float:right;}
#dt_basic_wrapper .DTTT.btn-group{float:right; margin-left:10px;  margin-right:10px;}
.smart-style-5 div.DTTT .btn{margin:0px;}
@media only screen and (min-width : 480px)
{
#dt_basic_filter{float: left;width: 50%;}
}

.loader {
  width: 3em;
  height: 3em;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  border-radius: 50%;
  box-shadow: 0 0 5px #444;
 
}
.loader:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: linear-gradient(#009EE3 , black 90%);
  animation: spin .5s infinite linear;
}
.loader:after {
  content: "";
  position: absolute;
  width: 90%;
  height: 90%;
  top: 5%;
  left:5%;
  background-color:#fff;
  border-radius: 50%;
  box-shadow: inset 0 0 5px #444;
}

@keyframes spin {
  to {
    transform: rotate(360deg);
  }
}
.load-bg{
    position:absolute;
    top:0;
    left:0;   
    width:100%;
    height:100%;
    
    z-index:99999;
    display:block;
}
.state-error{ color:#ff0000;}
.datepicker + .invalid {
    left: 0;
    position: absolute;
    top: 100%;
}
.modal-body{padding:0 20px;}
.loader1{position:relative; float:left; min-height:200px; width:100%;} 

label input.checkbox[type="checkbox"] + span{float: right; position:absolute;top:0;left:0;}
table.complete-compare thead tr th,table.complete-compare tbody tr td{white-space: nowrap; !important;}
#changeUpdate{height:200px; resize: none;}
</style>
<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Bundle Message
        <small>Edit bundle message</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Device list</a></li-->
        <li class="active">Edit Bundle Message</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Edit Bundle Message</h3>
						<div class="box-tools pull-right">
							<!-- <span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="3 peoples are late">3</span> -->
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form class="form-horizontal" action="<?php echo base_url();?>bundlemessage/editBundleMessage" id="editBundleMessageForm" method="post">
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Bundle ID<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<select class="form-control" name="bundleId" id="bundleId" onchange="getBundleMessageByBundleId(this.value)">
											<option value="">Select</option>
											 <?php foreach($arrGetBundleInfoRes as $arrResult){ ?>
											 <option value="<?php echo $arrResult['bundleid']; ?>"><?php echo $arrResult['bundleid']; ?></option>
												<?php } ?>	
										</select>										
									</div>									
								</div>															
							</div>	
			<div class="col-sm-12 col-md-10" id="ajaxErrorMsg" style="color:red;display:none;">
			<div id="loaderResultDiv" style="display:none;"></div>
				<div class="form-group">
					<label class="control-label col-sm-4 col-md-3 hidden-xs">&nbsp;</label>
					<div class="col-sm-5 col-md-5">
						<b>No records found</b>
					</div>	
				</div>
			</div>
			<div id="SearchBundleIdDiv" style="display:none;">				
			<?php if(isset($arrGetBundleMessageTemplateInfo[0]['errcode']) && $arrGetBundleMessageTemplateInfo[0]['errcode']=='0'){ ?> 
			<table class="table table-striped table-bordered table-hover" width="100%">
				<thead>
					<tr>
						<th data-class="expand">MessageId</th>
						<th data-hide="phone">Comment</th>
						<th data-hide="phone">Message Text</th>	
					</tr>
				</thead>
				<tbody>
				<?php $i=0; foreach($arrGetBundleMessageTemplateInfo as $arrResult){  ?>	
				<input type="hidden" name="hidMessageId[]"  value="<?php echo $arrResult['messageid']; ?>" />
				<input type="hidden" name="hidComment[]"  value="<?php echo $arrResult['comment']; ?>" />
					<tr>
						<td><?php echo $arrResult['messageid']; ?></td>
						<td><?php echo $arrResult['comment']; ?></td>																
						<td><textarea rows="4" cols="50" name="messageText[]"><?php echo $arrResult['message_text']; ?></textarea></td>
					</tr>
					<?php $i++; } ?>												
				</tbody>
			</table>
			<?php }else{ ?>	
			<br><br><b><center> No records found</center></b>
			<?php } ?>	
			</div>
							<!--center><span id="validationError" style="display:none;color:"></span></center-->
							<!--div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3 hidden-xs">&nbsp;</label>
									<div class="col-sm-5 col-md-5">
										<button type="submit" class="btn btn-primary">Edit</button>
										<button type="button" id="cancelBundleMessageButton" class="btn btn-primary">Cancel</button>
									</div>
								</div>
							</div-->
						</form>							
					</div>
				</div>
			</div>    
        
       
      </div>
	  <div id="resultDiv">
	  <?php if($this->session->flashdata('successmsg')!=''){ ?>
				<div class="alert alert-success text-center" id="successmsg" >
				  <?php echo $this->session->flashdata('successmsg'); ?>
				  </div>
		<?php } ?>
		<?php if($this->session->flashdata('errormsg')!=''){ ?>
				<div class="alert alert-error text-center" id="errormsg" >
				  <?php echo $this->session->flashdata('errormsg'); ?>
				  </div>
		<?php } ?>
		</div>
      <!-- /.row -->
    </section>
    <!-- /.content -->	
	
	 
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-1 col-sm-offset-1 col-md-8 col-md-offset-2 col-xs-12">
							<form class="form-horizontal" action="">
								<div class="form-group">
									<label class="col-sm-12 col-xs-12 text-center">Assign ICCID Number</label>
									<div class="col-sm-12 col-xs-12">
										<input type="text" class="form-control" value="" placeholder="ICCID Number" />
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-12 col-xs-12 text-center">
										<input type="submit" class="btn btn-primary" value="Reset" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>				
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">OK</button>				
				</div>
			</div>
		</div>
	</div>
 <?php //include('footer.php'); ?>
 
 
 <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
 
 <script> 
 /*function onLoadReport(){
	$('#reportDiv').html('<div class="load-bg"><div class="loader"></div></div>');		
	var operatorId='';
	var reportType='';
	var usageType='';
	var startDate='';
	var endDate='';
	getAllOperatorProfitLossReports(operatorId,reportType,usageType,startDate,endDate);	
 }
	onLoadReport();*/
  
  $(document).ready(function() {
	  $('#datatable_tabletools').DataTable();
		 //alert('hai');	 	 
		 $('#cancelBundleMessageButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundlemessage';
		 });		 
		 
			
		$('#editBundleMessageForm').validate({			
			rules:{
				bundleId:{
					required : true
				},					
			},
			messages:{
				bundleId:"Please select the bundle Id",					
			}
		  });				
	  
	 /* $("#bundleId").blur(function(){
		  $("#bundleId").valid();
	  });	*/
	
	$('#bundleId').on('click', function() {	
		$("#errormsg").hide();
		$("#successmsg").hide();
	 });	
    
  });
  
  $.validator.setDefaults({
		errorElement: "span",
		errorClass: "help-block",
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		errorPlacement: function (error, element) {
			if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
			error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});	
		
	function getBundleMessageByBundleId(bundleId){
		$('#loaderResultDiv').html('<div class="load-bg"><div class="loader"></div></div>');
		$("#loaderResultDiv").show();		
		var dataString = 'bundleId='+bundleId;	
		 $.ajax({
			   type: "POST",
			   url: "<?php echo base_url(); ?>bundlemessage/getBundleMessageByBundleId",
			   data: dataString,
			   //dataType: 'json',
			   success: function(data){ 
					//alert(data);
					if(data!==''){						
						$("#SearchBundleIdDiv").html(data);						
						$("#ajaxErrorMsg").hide();	
						$("#loaderResultDiv").hide();
						$("#SearchBundleIdDiv").show();
					}else{	
						$("#SearchBundleIdDiv").hide();
						$("#loaderResultDiv").hide();
						$("#ajaxErrorMsg").show();
					}
			   }
		 });	
	}  
	
	 $("#SearchBundleIdDiv").on('click', '.remove_button', function(e){ //Once remove button is clicked				
			var dynamicId = $(this).attr('id');
			//alert(dynamicId);			
			var editSubmitButtonText = $("#"+dynamicId).text();
			if(editSubmitButtonText=="Edit"){						 
				 $("#messageText"+dynamicId).prop("disabled",false);				 
				 $("#"+dynamicId).text("Save");
				return false;				
		  }else if(editSubmitButtonText=="Save"){
				var bundleId = $("#bundleId").val();
				var messageId = $("#hidMessageId"+dynamicId).val();
				var comment = $("#hidComment"+dynamicId).val();
				//comment = comment.trim();	
				var messageText = $("#messageText"+dynamicId).val();
				//messageText = messageText.trim();
				if(messageText==''){					
					$("#errMessageText"+dynamicId).html('Please enter the message text');					
					return false;					
				}else{
					$("#errMessageText"+dynamicId).html('');									
					//swal("Confirm Successfully");
					//$("#resultRoller").show();
					$('#SearchBundleIdDiv').html('<div class="load-bg"><div class="loader"></div></div>');
					$("#SearchBundleIdDiv").show();
					var dataString = 'bundleId='+bundleId+'&messageId='+messageId+'&comment='+comment+'&messageText='+encodeURIComponent(messageText);	
					 $.ajax({
						   type: "POST",
						   url: "<?php echo base_url(); ?>bundlemessage/editBundleMessageById",
						   data: dataString,
						   dataType: 'json',
						   success: function(data){ 
								//alert(data);
								if(data.errcode=='0'){						
									swal(data.errmsg);
									 $("#messageText"+dynamicId).prop("disabled",true);									
									$("#"+dynamicId).text("Edit");		
									//window.location.reload();	
									getBundleMessageByBundleId(bundleId);
								}else{	
									sweetAlert(data.errmsg, '', 'error');									
								}
								
						   }
					 });	
					
				}
		}
	});		
	</script>