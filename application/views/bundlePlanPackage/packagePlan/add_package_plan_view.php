<style type="text/css">	
#dt_basic_length{float:right;}
#dt_basic_wrapper .DTTT.btn-group{float:right; margin-left:10px;  margin-right:10px;}
.smart-style-5 div.DTTT .btn{margin:0px;}
@media only screen and (min-width : 480px)
{
#dt_basic_filter{float: left;width: 50%;}
}

.loader {
  width: 3em;
  height: 3em;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  border-radius: 50%;
  box-shadow: 0 0 5px #444;
 
}
.loader:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: linear-gradient(#009EE3 , black 90%);
  animation: spin .5s infinite linear;
}
.loader:after {
  content: "";
  position: absolute;
  width: 90%;
  height: 90%;
  top: 5%;
  left:5%;
  background-color:#fff;
  border-radius: 50%;
  box-shadow: inset 0 0 5px #444;
}

@keyframes spin {
  to {
    transform: rotate(360deg);
  }
}
.load-bg{
    position:absolute;
    top:0;
    left:0;   
    width:100%;
    height:100%;
    
    z-index:99999;
    display:block;
}
.state-error{ color:#ff0000;}
.datepicker + .invalid {
    left: 0;
    position: absolute;
    top: 100%;
}
.modal-body{padding:0 20px;}
.loader1{position:relative; float:left; min-height:200px; width:100%;} 

label input.checkbox[type="checkbox"] + span{float: right; position:absolute;top:0;left:0;}
table.complete-compare thead tr th,table.complete-compare tbody tr td{white-space: nowrap; !important;}
#changeUpdate{height:200px; resize: none;}
</style>
<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Bundle Plan Package
        <small>Add package to Bundle Id</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Device list</a></li-->
        <li class="active">Add Bundle Plan Package</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Add Bundle Plan Package</h3>
						<div class="box-tools pull-right">
							<!-- <span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="3 peoples are late">3</span> -->
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form class="form-horizontal" action="<?php echo base_url();?>bundlepackage/addBundlePackagePlan" id="addPackagePlanForm" method="post">
						<div id="addPackagePlanFormDiv" style="display:none;"></div>	
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Bundle ID<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<select class="form-control" name="bundleId" id="bundleId">
											<option value="">Select</option>
											 <?php foreach($arrGetBundleInfoRes as $arrResult){ ?>
											 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']. ' (Bundle-id: '.$arrResult['Refid'].')'; ?></option>
												<?php } ?>	
										</select>										
									</div>									
								</div>															
							</div>	
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Package ID<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<select class="form-control" name="packageId" id="packageId">
											<option value="">Select</option>
											 <?php foreach($arrGetBundlePackagesInfo as $arrResult){ ?>
											 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']. ' (Package-id: '.$arrResult['Refid'].')'; ?></option>
												<?php } ?>	
										</select>										
									</div>									
								</div>															
							</div>			
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Update Mode<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<select class="form-control" name="updateMode" id="updateMode">
											<option value="">Select</option>
											 <?php foreach($arrUpdateMode as $arrResult){ ?>
											 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
												<?php } ?>	
										</select>
									</div>
								</div>
							</div>	
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Package Balance<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<input type="text" name="packBalance" id="packBalance" value="" class="form-control amount" placeholder="Package Balance" maxlength="6" autocomplete="off" />
									</div>									
								</div>															
							</div>	
							<!--div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Renewal Mode<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<select class="form-control" name="renewalMode" id="renewalMode">
											<option value="">Select</option>
											 <?php foreach($arrRenewalMode as $arrResult){ ?>
											 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
												<?php } ?>	
										</select>
									</div>
								</div>
							</div-->
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3 hidden-xs">&nbsp;</label>
									<div class="col-sm-5 col-md-5">
										<button type="submit" class="btn btn-primary">Add</button>
										<button type="button" id="editPackagePlanButton" class="btn btn-primary">Edit</button>
										<button type="button" id="deletePackagePlanButton" class="btn btn-primary">Delete</button>
									</div>
								</div>
							</div>
						</form>							
					</div>
				</div>
			</div>    
        
       
      </div>
	  <?php if($this->session->flashdata('successmsg')!=''){ ?>
				<div class="alert alert-success text-center" id="successmsg" >
				  <?php echo $this->session->flashdata('successmsg'); ?>
				  </div>
		<?php } ?>
		<?php if($this->session->flashdata('errormsg')!=''){ ?>
				<div class="alert alert-error text-center" id="errormsg" >
				  <?php echo $this->session->flashdata('errormsg'); ?>
				  </div>
		<?php } ?>
      <!-- /.row -->
    </section>
    <!-- /.content -->	
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
		<!-- widget content -->
		<div class="widget-body no-padding" id="bundlePlanPackageTableDiv">			
			<table id="datatable_tabletools" class="table table-bordered table-striped nowrap dt-responsive" width="100%">
				<thead>
					<tr>
						<th data-class="expand">BundleId</th>
						<th data-hide="phone">PackageId</th>
						<th data-hide="phone">Update Mode</th>
						<th data-hide="phone">Package Balance</th>
						<!--th data-hide="phone,tablet">Renewal Mode</th-->
						<!--th data-hide="phone,tablet">Description</th-->											
					</tr>
				</thead>
				<tbody>				
					<tr>
						<td><?php echo $arrResult['bundleid']; ?></td>
						<td><?php echo $arrResult['packageid']; ?></td>
						<td><?php echo $arrResult['updatemode']; ?></td>
						<td><?php echo number_format((float) $arrResult['packagebalance'], 2, '.', ''); ?></td>						
						<!--td><?php //echo $arrResult['renewal_mode']; ?></td-->
						<!--td><?php //echo $arrResult['deskription']; ?></td-->												
					</tr>																
				</tbody>
			</table>			
		</div>
		<!-- end widget content -->

	</div>
	<!-- end widget div -->
  </div>
  </div>
  </div>
   </section>
  <!-- /.content-wrapper -->
  
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-1 col-sm-offset-1 col-md-8 col-md-offset-2 col-xs-12">
							<form class="form-horizontal" action="">
								<div class="form-group">
									<label class="col-sm-12 col-xs-12 text-center">Assign ICCID Number</label>
									<div class="col-sm-12 col-xs-12">
										<input type="text" class="form-control" value="" placeholder="ICCID Number" />
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-12 col-xs-12 text-center">
										<input type="submit" class="btn btn-primary" value="Reset" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>				
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">OK</button>				
				</div>
			</div>
		</div>
	</div>
 <?php //include('footer.php'); ?>
 
 
 
 <script> 
 function getAllBundlePlanPackageOnLoad(){		
		$('#bundlePlanPackageTableDiv').html('<div class="load-bg"><div class="loader"></div></div>');		
		$.ajax({
			   type: "POST",
			   url: "<?php echo base_url(); ?>bundlepackage/getAllBundlePlanPackageInfo",
			   data: '',
			   //dataType: 'json',
			   success: function(data){ 
					//alert(data);
					if(data!=""){
						$("#bundlePlanPackageTableDiv").html(data);							
					}else{
						 $("#bundlePlanPackageTableDiv").html('<b><center> No records found</center></b><br><br>');							 
					 }							
			   }
		});	
	}	
	getAllBundlePlanPackageOnLoad();
  
  $(document).ready(function() {
		 //alert('hai');	 	 
		 $('#datatable_tabletools').DataTable();
		$('#editPackagePlanButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundlepackage/getPackagePlanByBundleId/1';
		 });
		 
		 $('#deletePackagePlanButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundlepackage/getPackagePlanByBundleId/2';
		 });
		 
		 $('#addPackagePlanForm').validate({	
			submitHandler : function(form){									
				$('#addPackagePlanFormDiv').html('<div class="load-bg"><div class="loader"></div></div>');
				$('#addPackagePlanFormDiv').show();
				//$("#addPackagePlanForm").submit();													
				form.submit();
			},
			rules: {
				bundleId: "required",
				packageId: "required",
				updateMode: "required",							
				packBalance: "required",
				//renewalMode: "required",
			},
			messages: {
				bundleId: "Please select the bundle Id",
				packageId: "Please select the package Id",
				updateMode: "Please select the update mode",								
				packBalance: "Please enter the package balance",
				//renewalMode: "Please select the renewal mode",
			}	
		  });	
					
	  
	  $("#bundleId").blur(function(){
		  $("#bundleId").valid();
	  });
	  
	  $("#packageId").blur(function(){
		  $("#packageId").valid();
	  });
	  
	  $("#updateMode").blur(function(){
		  $("#updateMode").valid();
	  });
	  
	  $("#packBalance").blur(function(){
		  $("#packBalance").valid();
	  });
	  
	  /*$("#renewalMode").blur(function(){
		  $("#renewalMode").valid();
	  });*/
	  
	  $('#packageId').click(function() {
		 $("#packageId option[value='']").attr('selected', false);
	  });
	
	/*$('#bundleId').on('click', function() {	
		$("#errormsg").hide();
		$("#successmsg").hide();
	 });*/
    
  });
  
  $.validator.setDefaults({
		errorElement: "span",
		errorClass: "help-block",
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		errorPlacement: function (error, element) {
			if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
			error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});			  
	
	$('.amount').keypress(function(event) {
            if(event.which == 8 || event.which == 0){
                return true;
            }
            if(event.which < 46 || event.which > 59) {
                return false;
                //event.preventDefault();
            } // prevent if not number/dot

            if(event.which == 46 && $(this).val().indexOf('.') != -1) {
                return false;
                //event.preventDefault();
            } // prevent if already dot
			
			if ($(this).val().indexOf('.') > 0) {
				var len = $(this).val().length;
				var charAfterdot = (len + 1) - $(this).val().indexOf('.');
				if (charAfterdot > 3) {
					return false;
				}
			}
			
        });
		
		
  
	</script>