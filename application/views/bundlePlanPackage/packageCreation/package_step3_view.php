<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Package Creation
        <small>Add new package</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Enroll Data</a></li-->
        <li class="active">Create new package</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">ADD NEW PACKAGE</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="row">
						<!--  Add New Device Form  -->
						<div class="col-sm-12 col-md-7 col-xs-12">
							<form class="form-horizontal" action="<?php echo base_url();?>bundlepackagecreation/addPackageSteps" id="createPackageForm3" method="post">	
								<div id="hardware">
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Usage Notification Threshold</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="Usage Notification Threshold" name="usageNotIfThreshold" id="usageNotIfThreshold" maxlength="13" autocomplete="off" />
										</div>
									</div>									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Info Order</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="Info Order" name="infoOrder" id="infoOrder" maxlength="4" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Prompt Not Ok Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="promptNotOkMode" id="promptNotOkMode">
												<option value="">Select</option>
												 <?php foreach($arrPromptNotOkMode as $arrResult){ ?>
												 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Prompt Balance Zero</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="Prompt Balance Zero" name="pBalanceZero" id="pBalanceZero" maxlength="10" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Prompt Expiry</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="Prompt Expiry" name="promptExpiry" id="promptExpiry" maxlength="10" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Prompt Inactive</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="Prompt Inactive" name="promptInactive" id="promptInactive" maxlength="10" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Flag To Buy Bundle</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="flagBuyBundle" id="flagBuyBundle">
												<option value="">Select</option>
												 <?php foreach($arrFlagBuyBundle as $arrResult){ ?>
												 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Flag To Buy AirTime</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="flagBuyAirTime" id="flagBuyAirTime">
												<option value="">Select</option>
												 <?php foreach($arrFlagBuyAirTime as $arrResult){ ?>
												 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Flag To Pay Connection Charge</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="flagPayConn" id="flagPayConn">
												<option value="">Select</option>
												 <?php foreach($arrFlagPayConn as $arrResult){ ?>
												 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Breakage Charge Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="breakageChargeMode" id="breakageChargeMode">
												<option value="">Select</option>
												 <?php foreach($arrBreakageChargeMode as $arrResult){ ?>
												 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">ACC Minimum Balance</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control amount" value="" placeholder="ACC Minimum Balance" name="accMinBalance" id="accMinBalance" maxlength="6" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Auth Destination Flag</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="authDestinationFlag" id="authDestinationFlag">
												<option value="">Select</option>
												 <?php foreach($arrAuthDestinationFlag as $arrResult){ ?>
												 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Auth Destination CTR</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="Auth Destination CTR" name="authDestinationCTR" id="authDestinationCTR" maxlength="38" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Auth Destination Delay</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="Auth Destination Delay" name="authDestinationDelay" id="authDestinationDelay" maxlength="38" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Auth Destination Action</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="authDestinationAction" id="authDestinationAction">
												<option value="">Select</option>
												 <?php foreach($arrAuthDestinationAction as $arrResult){ ?>
												 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Activate Minimum Balance Master</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control amount" value="" placeholder="Activate Minimum Balance Master" name="activateMinBalMaster" id="activateMinBalMaster" maxlength="6" autocomplete="off" />
										</div>
									</div>									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Auth Destination Prompt</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="Auth Destination Prompt" name="authDestinationPrompt" id="authDestinationPrompt" maxlength="38" autocomplete="off" />
										</div>
									</div>
									<!--div class="form-group">
										<label class="col-sm-4 col-xs-12">Auth Destination Group</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="Auth Destination Group" name="authDestinationGroup" id="authDestinationGroup" maxlength="10" autocomplete="off" />
										</div>
									</div-->
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Use Limit Action</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="useLimitAction" id="useLimitAction">
												<option value="">Select</option>
												 <?php foreach($arrUseLimitAction as $arrResult){ ?>
												 <option value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Destination Pool ID</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="Destination Pool ID" name="destPoolId" id="destPoolId" maxlength="4" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">OBD Balance Threshold</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control amount" value="" placeholder="OBD Balance Threshold" name="balanceThreshold" id="balanceThreshold" maxlength="6" autocomplete="off" />
										</div>
									</div>											
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Activate Package Group ID</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="Activate Package Group ID" name="actPackageGroupId" id="actPackageGroupId" maxlength="4" autocomplete="off" />
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Promo Pack Group ID</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="Promo Pack Group ID" name="promoPackageGroupId" id="promoPackageGroupId" maxlength="4" autocomplete="off" />
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12 hidden-xs">&nbsp;</label>
										<div class="col-sm-7 col-md-7 col-xs-12">									
											<button type="button" id="step3BackButton" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp;Back</button>
											<!--button type="button" class="btn btn-info">Confirm</button-->
											<!--button type="button" class="btn btn-info">Save</button-->
											<button type="submit" class="btn btn-info">Add</button>
											<button type="button" id="step3CancelButton" class="btn btn-danger">Cancel</button>
										</div>
									</div>																																
								</div>
							</form>
						</div>
					</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<!--  Modal Open  --->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Product Information</h4>
      </div>
      <div class="modal-body">
        <div class="row">
			<!--  Add New Device Form  -->
			<div class="col-sm-12 col-md-12 col-xs-12">
				<form class="form-horizontal" action="">		
					<div class="form-group">
						<label class="col-sm-4 col-xs-12">Add Product Name</label>
						<div class="col-sm-7 col-md-7 col-xs-12">
							<input type="text" class="form-control" value="" placeholder="test" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 col-xs-12">Add Product Code</label>
						<div class="col-sm-7 col-md-7 col-xs-12">
							<input type="text" class="form-control" value="" placeholder="test" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 col-xs-12">Select category</label>
						<div class="col-sm-7 col-md-7 col-xs-12">
							<label class="radio-inline">
							<input type="radio" class="radio" value="" placeholder="test" /> Hardware
							</label>
							<label class="radio-inline">
							<input type="radio" class="radio" value="" placeholder="test" /> Software
							</label>
						</div>
					</div>															
				</form>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-info">SAVE</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal End -->

 <?php //include('footer.php'); ?>

<script> 
   
  //$(document).ready(function() {
		 //alert('hai');	 	 
		 $('#step3BackButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundlepackagecreation/createPackageStep2';
		 });
		// step3CancelButton
		 $('#step3CancelButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundlepackagecreation';
		 });
		 
			
	$('#pBalanceZero,  #promptExpiry, #promptInactive, #usageNotIfThreshold, #authDestinationPrompt').keyup(function() {  		   
			if (this.value.match(/[^a-zA-Z0-9 ]/g)) {
				this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^a-zA-Z0-9 ]/g, '')); 
					}, 5);  
     });
			
	$('#infoOrder, #destPoolId, #actPackageGroupId, #promoPackageGroupId').keyup(function() {  		   
			if (this.value.match(/[^0-9 ]/g)) {
				this.value = this.value.replace(/[^0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^0-9 ]/g, '')); 
					}, 5);  
            });	 

		$('.amount').keypress(function(event) {
            if(event.which == 8 || event.which == 0){
                return true;
            }
            if(event.which < 46 || event.which > 59) {
                return false;
                //event.preventDefault();
            } // prevent if not number/dot

            if(event.which == 46 && $(this).val().indexOf('.') != -1) {
                return false;
                //event.preventDefault();
            } // prevent if already dot
			
			if ($(this).val().indexOf('.') > 0) {
				var len = $(this).val().length;
				var charAfterdot = (len + 1) - $(this).val().indexOf('.');
				if (charAfterdot > 3) {
					return false;
				}
			}
			
        });
		
	//});	
  
	</script>