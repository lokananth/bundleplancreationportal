<style type="text/css">	
#dt_basic_length{float:right;}
#dt_basic_wrapper .DTTT.btn-group{float:right; margin-left:10px;  margin-right:10px;}
.smart-style-5 div.DTTT .btn{margin:0px;}
@media only screen and (min-width : 480px)
{
#dt_basic_filter{float: left;width: 50%;}
}

.loader {
  width: 3em;
  height: 3em;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  border-radius: 50%;
  box-shadow: 0 0 5px #444;
 
}
.loader:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: linear-gradient(#009EE3 , black 90%);
  animation: spin .5s infinite linear;
}
.loader:after {
  content: "";
  position: absolute;
  width: 90%;
  height: 90%;
  top: 5%;
  left:5%;
  background-color:#fff;
  border-radius: 50%;
  box-shadow: inset 0 0 5px #444;
}

@keyframes spin {
  to {
    transform: rotate(360deg);
  }
}
.load-bg{
    position:absolute;
    top:0;
    left:0;   
    width:100%;
    height:100%;
    
    z-index:99999;
    display:block;
}
.state-error{ color:#ff0000;}
.datepicker + .invalid {
    left: 0;
    position: absolute;
    top: 100%;
}
.modal-body{padding:0 20px;}
.loader1{position:relative; float:left; min-height:200px; width:100%;} 

label input.checkbox[type="checkbox"] + span{float: right; position:absolute;top:0;left:0;}
table.complete-compare thead tr th,table.complete-compare tbody tr td{white-space: nowrap; !important;}
#changeUpdate{height:200px; resize: none;}

[data-tip] {
	position:relative;

}
[data-tip]:before {
	content:'';
	/* hides the tooltip when not hovered */
	display:none;
	content:'';
	border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	border-bottom: 5px solid #1a1a1a;	
	position:absolute;
	top:30px;
	left:35px;
	z-index:8;
	font-size:0;
	line-height:0;
	width:0;
	height:0;
}
[data-tip]:after {
	display:none;
	content:attr(data-tip);
	position:absolute;
	top:35px;
	left:0px;
	padding:5px 8px;
	background:#1a1a1a;
	color:#fff;
	z-index:9;
	font-size: 0.75em;
	height:18px;
	line-height:8px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	white-space:nowrap;
	word-wrap:normal;
}
[data-tip]:hover:before,
[data-tip]:hover:after {
	display:block;
}
</style>
<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>
<?php
if(isset($_SESSION['PackageStep1']['packageName']) && $_SESSION['PackageStep1']['packageName']!='') { 
	$varPackageName = $_SESSION['PackageStep1']['packageName'];
}else {
	$varPackageName = '';
}
if(isset($_SESSION['PackageStep1']['packageTypeId']) && $_SESSION['PackageStep1']['packageTypeId']!='') { 
	$varPackageTypeId = $_SESSION['PackageStep1']['packageTypeId'];
}else {
	$varPackageTypeId = '';
}
if(isset($_SESSION['PackageStep1']['packageDescription']) && $_SESSION['PackageStep1']['packageDescription']!='') { 
	$varPackageDescription = $_SESSION['PackageStep1']['packageDescription'];
}else {
	$varPackageDescription = '';
}
if(isset($_SESSION['PackageStep1']['packagePrice']) && $_SESSION['PackageStep1']['packagePrice']!='') { 
	$varPackagePrice = number_format((float) $_SESSION['PackageStep1']['packagePrice'], 2, '.', '');	
}else {
	$varPackagePrice = '';
}
if(isset($_SESSION['PackageStep1']['duration']) && $_SESSION['PackageStep1']['duration']!='') { 
	$varDuration = $_SESSION['PackageStep1']['duration'];
}else {
	$varDuration = '';
}
if(isset($_SESSION['PackageStep1']['tariffClass']) && $_SESSION['PackageStep1']['tariffClass']!='') { 
	$varTariffClass = $_SESSION['PackageStep1']['tariffClass'];
}else {
	$varTariffClass = '';
}
if(isset($_SESSION['PackageStep1']['pricePerUnit']) && $_SESSION['PackageStep1']['pricePerUnit']!='') { 
	$varPricePerUnit = number_format((float) $_SESSION['PackageStep1']['pricePerUnit'], 2, '.', '');	
}else {
	$varPricePerUnit = '';
}
if(isset($_SESSION['PackageStep1']['ussdInfoType']) && $_SESSION['PackageStep1']['ussdInfoType']!='') { 
	$varUssdInfoType = $_SESSION['PackageStep1']['ussdInfoType'];
}else {
	$varUssdInfoType = '';
}
if(isset($_SESSION['PackageStep1']['ussdInfoText']) && $_SESSION['PackageStep1']['ussdInfoText']!='') { 
	$varUssdInfoText = $_SESSION['PackageStep1']['ussdInfoText'];
}else {
	$varUssdInfoText = '';
}
if(isset($_SESSION['PackageStep1']['prompt']) && $_SESSION['PackageStep1']['prompt']!='') { 
	$varPrompt = $_SESSION['PackageStep1']['prompt'];
}else {
	$varPrompt = '';
}
if(isset($_SESSION['PackageStep1']['promptBalance']) && $_SESSION['PackageStep1']['promptBalance']!='') { 
	$varPromptBalance = $_SESSION['PackageStep1']['promptBalance'];
}else {
	$varPromptBalance = '';
}
if(isset($_SESSION['PackageStep1']['promptUnit']) && $_SESSION['PackageStep1']['promptUnit']!='') { 
	$varPromptUnit = $_SESSION['PackageStep1']['promptUnit'];
}else {
	$varPromptUnit = '';
}
if(isset($_SESSION['PackageStep1']['priority']) && $_SESSION['PackageStep1']['priority']!='') { 
	$varPriority = $_SESSION['PackageStep1']['priority'];
}else {
	$varPriority = '';
}
if(isset($_SESSION['PackageStep1']['cardId']) && $_SESSION['PackageStep1']['cardId']!='') { 
	$varCardId = $_SESSION['PackageStep1']['cardId'];
}else {
	$varCardId = '';
}
if(isset($_SESSION['PackageStep1']['infoType']) && $_SESSION['PackageStep1']['infoType']!='') { 
	$varInfoType = $_SESSION['PackageStep1']['infoType'];
}else {
	$varInfoType = '';
}
if(isset($_SESSION['PackageStep1']['infoText']) && $_SESSION['PackageStep1']['infoText']!='') { 
	$varInfoText = $_SESSION['PackageStep1']['infoText'];
}else {
	$varInfoText = '';
}
if(isset($_SESSION['PackageStep1']['playPrompt']) && $_SESSION['PackageStep1']['playPrompt']!='') { 
	$varPlayPrompt = $_SESSION['PackageStep1']['playPrompt'];
}else {
	$varPlayPrompt = '';
}
if(isset($_SESSION['PackageStep1']['timeANCMode']) && $_SESSION['PackageStep1']['timeANCMode']!='') { 
	$varTimeANCMode = $_SESSION['PackageStep1']['timeANCMode'];
}else {
	$varTimeANCMode = '';
}
if(isset($_SESSION['PackageStep1']['promptTimeANC']) && $_SESSION['PackageStep1']['promptTimeANC']!='') { 
	$varPromptTimeANC = $_SESSION['PackageStep1']['promptTimeANC'];
}else {
	$varPromptTimeANC = '';
}
if(isset($_SESSION['PackageStep1']['promptInfo']) && $_SESSION['PackageStep1']['promptInfo']!='') { 
	$varPromptInfo = $_SESSION['PackageStep1']['promptInfo'];
}else {
	$varPromptInfo = '';
}
if(isset($_SESSION['PackageStep1']['usedMode']) && $_SESSION['PackageStep1']['usedMode']!='') { 
	$varUsedMode = $_SESSION['PackageStep1']['usedMode'];
}else {
	$varUsedMode = '';
}
if(isset($_SESSION['PackageStep1']['limitMode']) && $_SESSION['PackageStep1']['limitMode']!='') { 
	$varLimitMode = $_SESSION['PackageStep1']['limitMode'];
}else {
	$varLimitMode = '';
}
if(isset($_SESSION['PackageStep1']['limitDelay']) && $_SESSION['PackageStep1']['limitDelay']!='') { 
	$varLimitDelay = $_SESSION['PackageStep1']['limitDelay'];
}else {
	$varLimitDelay = '';
}
if(isset($_SESSION['PackageStep1']['limitAmount']) && $_SESSION['PackageStep1']['limitAmount']!='') { 
	$varLimitAmount = number_format((float) $_SESSION['PackageStep1']['limitAmount'], 2, '.', '');	
}else {
	$varLimitAmount = '';
}
if(isset($_SESSION['PackageStep1']['limitMinutes']) && $_SESSION['PackageStep1']['limitMinutes']!='') { 
	$varLimitMinutes = $_SESSION['PackageStep1']['limitMinutes'];
}else {
	$varLimitMinutes = '';
}
if(isset($_SESSION['PackageStep1']['limitCall']) && $_SESSION['PackageStep1']['limitCall']!='') { 
	$varLimitCall = $_SESSION['PackageStep1']['limitCall'];
}else {
	$varLimitCall = '';
}
if(isset($_SESSION['PackageStep1']['infoPrice']) && $_SESSION['PackageStep1']['infoPrice']!='') { 
	$varInfoPrice = number_format((float) $_SESSION['PackageStep1']['infoPrice'], 2, '.', '');	
}else {
	$varInfoPrice = '';
}
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Package Creation
        <small>Add new package</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Enroll Data</a></li-->
        <li class="active">Create new package</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">ADD NEW PACKAGE</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="row">
						<!--  Add New Device Form  -->
						<div class="col-sm-12 col-md-7 col-xs-12">
							<form class="form-horizontal" action="<?php echo base_url();?>bundlepackagecreation/createPackageStep2" id="createPackageForm1" method="post">	
								<div id="hardware">
									<!--div class="form-group">
										<label class="col-sm-4 col-xs-12">Bundle ID</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="test" />
										</div>
									</div-->
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Site Code</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $_SESSION['SiteCode']; ?>" placeholder="Site Code" name="siteCode" id="siteCode" maxlength="20" readonly autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Name<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varPackageName;?>" placeholder="Package Name" name="packageName" id="packageName" maxlength="60" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Package Type Id<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="packageTypeId" id="packageTypeId">
												<option value="">Select</option>
												 <?php foreach($arrPackageTypeId as $arrResult){ ?>
												 <option <?php if($varPackageTypeId!='') if($varPackageTypeId==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Package Description</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varPackageDescription;?>" placeholder="Package Description" name="packageDescription" maxlength="60" id="packageDescription" value="" autocomplete="off" />
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Price<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control amount" value="<?php echo $varPackagePrice;?>" placeholder="Package Price" name="packagePrice" maxlength="6" id="packagePrice" value="" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Currency Code</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $_SESSION['Currency']; ?>" placeholder="Currency Code" name="currency" id="currency" maxlength="20" readonly autocomplete="off" />											
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Duration(Days)<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varDuration; ?>" placeholder="Duration" name="duration" id="duration" maxlength="4" autocomplete="off" />											
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Tariff Class<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12" data-tip="Type Capital letter only">										
											<input type="text" class="form-control" value="<?php echo $varTariffClass;?>" placeholder="Tariff Class" name="tariffClass" id="tariffClass" maxlength="4" onkeydown="restrictSpace(this.id);" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Price Per Unit<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control amount" value="<?php echo $varPricePerUnit;?>" placeholder="Price Per Unit" name="pricePerUnit" id="pricePerUnit" maxlength="6" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">USSD Info Type<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="ussdInfoType" id="ussdInfoType">
												<option value="">Select</option>
												 <?php foreach($arrUSSDInfoType as $arrResult){ ?>
												 <option <?php if($varUssdInfoType!='') if($varUssdInfoType==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>		
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">USSD Info Text<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varUssdInfoText;?>" placeholder="USSD Info Text" name="ussdInfoText" id="ussdInfoText" maxlength="155" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Priority<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varPriority;?>" placeholder="Priority" name="priority" id="priority" maxlength="4" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Prompt</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varPrompt;?>" placeholder="Prompt" name="prompt" id="prompt" maxlength="60" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Prompt Balance</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varPromptBalance;?>" placeholder="Prompt Balance" name="promptBalance" id="promptBalance" maxlength="60" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Prompt Unit</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varPromptUnit;?>" placeholder="Prompt Unit" name="promptUnit" id="promptUnit" maxlength="60" autocomplete="off" />
										</div>
									</div>									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Card Id</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varCardId;?>" placeholder="Card Id" name="cardId" id="cardId" maxlength="20" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Info Type</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="infoType" id="infoType">
												<option value="">Select</option>
												 <?php foreach($arrUSSDInfoType as $arrResult){ ?>
												 <option <?php if($varInfoType!='') if($varInfoType==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>		
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Info Text</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varInfoText;?>" placeholder="Info Text" name="infoText" id="infoText" maxlength="155" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">PlayPrompt</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="playPrompt" id="playPrompt">
												<option value="">Select</option>
												 <?php foreach($arrPlayPrompt as $arrResult){ ?>
												 <option <?php if($varPlayPrompt!='') if($varPlayPrompt==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<!--div class="form-group">
										<label class="col-sm-4 col-xs-12">Time ANC Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="timeANCMode" id="timeANCMode">
												<option value="">Select</option>
												 <?php foreach($arrTimeANCMode as $arrResult){ ?>
												 <option <?php if($varTimeANCMode!='') if($varTimeANCMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div-->
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Prompt Time ANC</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varPromptTimeANC;?>" placeholder="Prompt Time ANC" name="promptTimeANC" id="promptTimeANC" maxlength="60" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Prompt Info</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varPromptInfo;?>" placeholder="Prompt Info" name="promptInfo" id="promptInfo" maxlength="60" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Used Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="usedMode" id="usedMode">
												<option value="">Select</option>
												 <?php foreach($arrUsedMode as $arrResult){ ?>
												 <option <?php if($varUsedMode!='') if($varUsedMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Limit Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="limitMode" id="limitMode">
												<option value="">Select</option>
												 <?php foreach($arrUsedLimitMode as $arrResult){ ?>
												 <option <?php if($varLimitMode!='') if($varLimitMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Limit Delay</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varLimitDelay;?>" placeholder="Limit Delay" name="limitDelay" id="limitDelay" maxlength="4" autocomplete="off" />
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Limit Amount</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control amount" value="<?php echo $varLimitAmount;?>" placeholder="Limit Amount" name="limitAmount" id="limitAmount" maxlength="6" autocomplete="off" />
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Limit Minutes</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varLimitMinutes;?>" placeholder="Limit Minutes" name="limitMinutes" id="limitMinutes" maxlength="4" autocomplete="off" />
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Limit Call</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varLimitCall;?>" placeholder="Limit Call" name="limitCall" id="limitCall" maxlength="4" autocomplete="off" />
										</div>
									</div>		
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Info Price</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control amount" value="<?php echo $varInfoPrice;?>" placeholder="Info Price" name="infoPrice" id="infoPrice" maxlength="6" autocomplete="off" />
										</div>
									</div>	
									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12 hidden-xs">&nbsp;</label>
										<div class="col-sm-7 col-md-7 col-xs-12">									
											<button type="button" id="step1CancelButton" class="btn btn-default">Cancel</button>
											<button type="submit" class="btn btn-info">Next &nbsp;<i class="fa fa-arrow-right"></i></button>
										</div>
									</div>								
								</div>
							</form>
						</div>
					</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<!--  Modal Open  --->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Get Tariff Group Information</h4>
      </div>
      <div class="modal-body">
        <div class="row">
			<!--  Add New Device Form  -->
			<div class="col-sm-12 col-md-12 col-xs-12">
				<form class="form-horizontal" id="searchGroupForm" name="searchGroupForm">		
					<div class="form-group">
						<label class="col-sm-3 col-xs-12 control-label">Tariff GroupId</label>
						<div class="col-sm-5 col-md-5 col-xs-9">
							<input type="text" class="form-control" value="" placeholder="Tariff GroupId" id="tariffGroupId" name="tariffGroupId" autocomplete="off" maxlength="15" />
						</div>
						<div class="col-sm-3 col-xs-9">
							<button type="submit" class="btn btn-info">Search</button>
						</div>
					</div>															
				</form>
			</div>
			<div class="col-sm-12 col-md-12 col-xs-12" id="resultTableDiv" style="display:none;">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Heading 1</th>
							<th>Heading 2</th>
							<th>Heading 3</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>datat 1</td>
							<td>datat 2</td>
							<td>datat 3</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal End -->

 <?php //include('footer.php'); ?>

<script> 
 /*function onLoadReport(){
	$('#reportDiv').html('<div class="load-bg"><div class="loader"></div></div>');		
	var operatorId='';
	var reportType='';
	var usageType='';
	var startDate='';
	var endDate='';
	getAllOperatorProfitLossReports(operatorId,reportType,usageType,startDate,endDate);	
 }
	onLoadReport();*/
  
  $(document).ready(function() {
		 //alert('hai');	 	 
		 $('#step1CancelButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundlepackagecreation';
		 });

		$("#createPackageForm1").validate({			
			rules: {
				siteCode: "required",
				packageName: "required",
				packageTypeId: "required",
				packagePrice: "required",
				currency: "required",
				duration: "required",
				//tariffClass: "required",
					tariffClass: {
					  required: true,
					  minlength: 4
					},
				pricePerUnit: "required",
				ussdInfoType: "required",
				//renewalMode: "required",
				ussdInfoText: "required",	
				priority: "required",	
			},
			messages: {
				siteCode: "Please select a sitecode",
				packageName: "Please enter a package name",
				packageTypeId: "Please select a package type",
				packagePrice: "Please enter a package price",
				currency: "Please enter a currency",
				duration: "Please enter a duration",
				//tariffClass: "Please enter a tariff Class",
				tariffClass: {
					  required: "Please enter a tariff Class",					  
					},
				pricePerUnit: "Please enter a price Per Unit",
				ussdInfoType: "Please select a ussd Info Type",
				//renewalMode: "Please select a renewal mode",
				ussdInfoText: "Please enter a ussdInfo Text",						
				priority: "Please enter a priority",
			}			
		});	
		
		
		
		/*$("#searchGroupForm").validate({
			submitHandler : function(form){	
				$("#resultTableDiv").show();
				$('#resultTableDiv').html('<div class="load-bg"><div class="loader"></div></div>');
				var tariffGroupId = $("#tariffGroupId").val();									
				var dataString = 'tariffGroupId='+tariffGroupId;	
				 $.ajax({
					   type: "POST",
					   url: "<?php echo base_url(); ?>bundleplan/getTariffGroupInfo",
					   data: dataString,
					   //dataType: 'json',
					   success: function(data){ 
							//alert(data);
							$("#resultTableDiv").html(data);							
					   }
				 });	
				//form.submit();				
			},
			rules: {
				tariffGroupId: "required",					
			},
			messages: {
				tariffGroupId: "Please enter a tariff group id",					
			}			
		});	*/
					
	  
	  $("#siteCode").blur(function(){
		  $("#siteCode").valid();
	  });		
	  
	  $("#packageName").blur(function(){
		  $("#packageName").valid();
	  });		
	  
	  $("#packageTypeId").blur(function(){
		  $("#packageTypeId").valid();
	  });		
	  
	  $("#packagePrice").blur(function(){
		  $("#packagePrice").valid();
	  });		
	  
	  $("#currency").blur(function(){
		  $("#currency").valid();
	  });		
	  
	  $("#tariffClass").blur(function(){
		  $("#tariffClass").valid();
	  });		
	  
	  $("#pricePerUnit").blur(function(){
		  $("#pricePerUnit").valid();
	  });		
	  
	  $("#ussdInfoType").blur(function(){
		  $("#ussdInfoType").valid();
	  });		
	  
	  $("#ussdInfoText").blur(function(){
		  $("#ussdInfoText").valid();
	  });		  
	  
	  $("#priority").blur(function(){
		  $("#priority").valid();
	  });	 
	  			
  });
  
  
  $.validator.setDefaults({
		errorElement: "span",
		errorClass: "help-block",
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		errorPlacement: function (error, element) {
			if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
			error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});	
	
	$("#plusButton").click(function(){
		  $("#resultTableDiv").html('');
		  $("#tariffGroupId").val('');
	  });	
	
	$('#prompt, #promptBalance, #promptUnit, #promptTimeANC').keyup(function() {  		   
			if (this.value.match(/[^a-zA-Z0-9 ]/g)) {
				this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^a-zA-Z0-9 ]/g, '')); 
					}, 5);  
            });	 
			
	$('#tariffClass').keyup(function() {  		   
		if (this.value.match(/[^A-Z0-9 ]/g)) {
			this.value = this.value.replace(/[^A-Z0-9 ]/g, '');
	  }
	}).on('paste', function (e) {  
				var $this = $(this);  
				setTimeout(function () {  
					$this.val($this.val().replace(/[^A-Z0-9 ]/g, '')); 
				}, 5);  
		});	 
			
	$('#limitDelay, #limitMinutes, #limitCall, #priority, #cardId, #duration').keyup(function() {  		   
			if (this.value.match(/[^0-9 ]/g)) {
				this.value = this.value.replace(/[^0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^0-9 ]/g, '')); 
					}, 5);  
            });	 

	$('.amount').keypress(function(event) {
            if(event.which == 8 || event.which == 0){
                return true;
            }
            if(event.which < 46 || event.which > 59) {
                return false;
                //event.preventDefault();
            } // prevent if not number/dot

            if(event.which == 46 && $(this).val().indexOf('.') != -1) {
                return false;
                //event.preventDefault();
            } // prevent if already dot
			
			if ($(this).val().indexOf('.') > 0) {
				var len = $(this).val().length;
				var charAfterdot = (len + 1) - $(this).val().indexOf('.');
				if (charAfterdot > 3) {
					return false;
				}
			}
			
        });
		
		function restrictSpace(getId) {
		$('#'+getId).keydown(function (e) {	
			if (e.ctrlKey || e.altKey) {
			e.preventDefault();
			} else {
			var key = e.keyCode;
			//alert(key);
			if (key == 32) {
			e.preventDefault();
			}
			}
			});
		} 
  
	</script>