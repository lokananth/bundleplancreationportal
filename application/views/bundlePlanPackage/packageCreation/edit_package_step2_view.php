<style type="text/css">	
#dt_basic_length{float:right;}
#dt_basic_wrapper .DTTT.btn-group{float:right; margin-left:10px;  margin-right:10px;}
.smart-style-5 div.DTTT .btn{margin:0px;}
@media only screen and (min-width : 480px)
{
#dt_basic_filter{float: left;width: 50%;}
}

.loader {
  width: 3em;
  height: 3em;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  border-radius: 50%;
  box-shadow: 0 0 5px #444;
 
}
.loader:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: linear-gradient(#009EE3 , black 90%);
  animation: spin .5s infinite linear;
}
.loader:after {
  content: "";
  position: absolute;
  width: 90%;
  height: 90%;
  top: 5%;
  left:5%;
  background-color:#fff;
  border-radius: 50%;
  box-shadow: inset 0 0 5px #444;
}

@keyframes spin {
  to {
    transform: rotate(360deg);
  }
}
.load-bg{
    position:absolute;
    top:0;
    left:0;   
    width:100%;
    height:100%;
    
    z-index:99999;
    display:block;
}
.state-error{ color:#ff0000;}
.datepicker + .invalid {
    left: 0;
    position: absolute;
    top: 100%;
}
.modal-body{padding:0 20px;}
.loader1{position:relative; float:left; min-height:200px; width:100%;} 

label input.checkbox[type="checkbox"] + span{float: right; position:absolute;top:0;left:0;}
table.complete-compare thead tr th,table.complete-compare tbody tr td{white-space: nowrap; !important;}
#changeUpdate{height:200px; resize: none;}
</style>
<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>
<?php
//echo '<pre>';print_r($arrGetPackageInfoRes);exit; 
if(isset($_SESSION['EditPackageStep2']['packConnection']) && $_SESSION['EditPackageStep2']['packConnection']!='') { 
	$varPackConnection = $_SESSION['EditPackageStep2']['packConnection'];
}else {
	$varPackConnection = trim($arrGetPackageInfoRes[0]['ispackcnxcharge']);
}
if(isset($_SESSION['EditPackageStep2']['packFreeFlag']) && $_SESSION['EditPackageStep2']['packFreeFlag']!='') { 
	$varPackFreeFlag = $_SESSION['EditPackageStep2']['packFreeFlag'];
}else {
	$varPackFreeFlag = trim($arrGetPackageInfoRes[0]['packfree_flag']);
}
if(isset($_SESSION['EditPackageStep2']['maxTalkTime']) && $_SESSION['EditPackageStep2']['maxTalkTime']!='') { 
	$varMaxTalkTime = $_SESSION['EditPackageStep2']['maxTalkTime'];
}else {
	$varMaxTalkTime = $arrGetPackageInfoRes[0]['call_maxtalktime'];
}
if(isset($_SESSION['EditPackageStep2']['checkDestination']) && $_SESSION['EditPackageStep2']['checkDestination']!='') { 
	$varCheckDestination = $_SESSION['EditPackageStep2']['checkDestination'];
}else {
	$varCheckDestination = trim($arrGetPackageInfoRes[0]['check_dest']);
}
if(isset($_SESSION['EditPackageStep2']['maxDestination']) && $_SESSION['EditPackageStep2']['maxDestination']!='') { 
	$varMaxDestination = $_SESSION['EditPackageStep2']['maxDestination'];
}else {
	$varMaxDestination = $arrGetPackageInfoRes[0]['max_dest'];
}
if(isset($_SESSION['EditPackageStep2']['destinationDelay']) && $_SESSION['EditPackageStep2']['destinationDelay']!='') { 
	$varDestinationDelay = $_SESSION['EditPackageStep2']['destinationDelay'];
}else {
	$varDestinationDelay = $arrGetPackageInfoRes[0]['dest_delay'];
}
if(isset($_SESSION['EditPackageStep2']['maxTalkTimeMode']) && $_SESSION['EditPackageStep2']['maxTalkTimeMode']!='') { 
	$varMaxTalkTimeMode = $_SESSION['EditPackageStep2']['maxTalkTimeMode'];
}else {
	$varMaxTalkTimeMode = trim($arrGetPackageInfoRes[0]['call_maxtalktime_mode']);
}
/*if(isset($_SESSION['PackageStep2']['packEndTime']) && $_SESSION['PackageStep2']['packEndTime']!='') { 
	$varPackEndTime = $_SESSION['PackageStep2']['packEndTime'];
}else {
	$varPackEndTime = '';
}*/
if(isset($_SESSION['EditPackageStep2']['packStartDate']) && $_SESSION['EditPackageStep2']['packStartDate']!='') { 
	$varPackStartDate = @date('Y-m-d',@strtotime(str_replace("/", "-",$_SESSION['EditPackageStep2']['packStartDate'])));	
}else {
	$varPackStartDate = ($arrGetPackageInfoRes[0]['pack_startdate']!='') ? @date('Y-m-d',@strtotime(str_replace("/", "-",$arrGetPackageInfoRes[0]['pack_startdate']))) : '';
}
if(isset($_SESSION['EditPackageStep2']['packEndDate']) && $_SESSION['EditPackageStep2']['packEndDate']!='') { 
	$varPackEndDate = @date('Y-m-d',@strtotime(str_replace("/", "-",$_SESSION['EditPackageStep2']['packEndDate'])));	
}else {
	$varPackEndDate = ($arrGetPackageInfoRes[0]['pack_enddate']!='') ? @date('Y-m-d',@strtotime(str_replace("/", "-",$arrGetPackageInfoRes[0]['pack_enddate']))) : '';
}
if(isset($_SESSION['EditPackageStep2']['subPackage']) && $_SESSION['EditPackageStep2']['subPackage']!='') { 
	$varSubPackage = $_SESSION['EditPackageStep2']['subPackage'];
}else {
	$varSubPackage = trim($arrGetPackageInfoRes[0]['isSubPackage']);
}
if(isset($_SESSION['EditPackageStep2']['promoBalance']) && $_SESSION['EditPackageStep2']['promoBalance']!='') { 
	$varPromoBalance = $_SESSION['EditPackageStep2']['promoBalance'];
}else {
	$varPromoBalance = ($arrGetPackageInfoRes[0]['promobal_ratio']!='') ? number_format((float) $arrGetPackageInfoRes[0]['promobal_ratio'], 2, '.', '') : '';	
}
if(isset($_SESSION['EditPackageStep2']['extensionMode']) && $_SESSION['EditPackageStep2']['extensionMode']!='') { 
	$varExtensionMode = $_SESSION['EditPackageStep2']['extensionMode'];
}else {
	$varExtensionMode = trim($arrGetPackageInfoRes[0]['extension_mode']);
}
if(isset($_SESSION['EditPackageStep2']['infoDayCode']) && $_SESSION['EditPackageStep2']['infoDayCode']!='') { 
	$varInfoDayCode = $_SESSION['EditPackageStep2']['infoDayCode'];
}else {
	$varInfoDayCode = $arrGetPackageInfoRes[0]['info_daycode'];
}
if(isset($_SESSION['EditPackageStep2']['noPriceUnit']) && $_SESSION['EditPackageStep2']['noPriceUnit']!='') { 
	$varNoPriceUnit = $_SESSION['EditPackageStep2']['noPriceUnit'];
}else {
	$varNoPriceUnit = $arrGetPackageInfoRes[0]['nof_priceperunit'];
}
if(isset($_SESSION['EditPackageStep2']['pricePerUnitList']) && $_SESSION['EditPackageStep2']['pricePerUnitList']!='') { 
	$varPricePerUnitList = $_SESSION['EditPackageStep2']['pricePerUnitList'];
}else {
	$varPricePerUnitList = $arrGetPackageInfoRes[0]['priceperunit_list'];
}
if(isset($_SESSION['EditPackageStep2']['masterNotif']) && $_SESSION['EditPackageStep2']['masterNotif']!='') { 
	$varMasterNotif = $_SESSION['EditPackageStep2']['masterNotif'];
}else {
	$varMasterNotif = $arrGetPackageInfoRes[0]['master_notif'];
}
if(isset($_SESSION['EditPackageStep2']['prefixMode']) && $_SESSION['EditPackageStep2']['prefixMode']!='') { 
	$varPrefixMode = $_SESSION['EditPackageStep2']['prefixMode'];
}else {
	$varPrefixMode = trim($arrGetPackageInfoRes[0]['Prefix_mode']);
}
if(isset($_SESSION['EditPackageStep2']['usageNotifMode']) && $_SESSION['EditPackageStep2']['usageNotifMode']!='') { 
	$varUsageNotifMode = $_SESSION['EditPackageStep2']['usageNotifMode'];
}else {
	$varUsageNotifMode = $arrGetPackageInfoRes[0]['usagenotif_mode'];
}
if(isset($_SESSION['EditPackageStep2']['usageNotifType']) && $_SESSION['EditPackageStep2']['usageNotifType']!='') { 
	$varUsageNotifType = $_SESSION['EditPackageStep2']['usageNotifType'];
}else {
	$varUsageNotifType = $arrGetPackageInfoRes[0]['usagenotif_type'];
}
if(isset($_SESSION['EditPackageStep2']['usageNotifText']) && $_SESSION['EditPackageStep2']['usageNotifText']!='') { 
	$varUsageNotifText = $_SESSION['EditPackageStep2']['usageNotifText'];
}else {
	$varUsageNotifText = $arrGetPackageInfoRes[0]['usagenotif_text'];
}
if(isset($_SESSION['EditPackageStep2']['usageNotifPrompt']) && $_SESSION['EditPackageStep2']['usageNotifPrompt']!='') { 
	$varUsageNotifPrompt = $_SESSION['EditPackageStep2']['usageNotifPrompt'];
}else {
	$varUsageNotifPrompt = $arrGetPackageInfoRes[0]['usagenotif_prompt'];
}
if(isset($_SESSION['EditPackageStep2']['prefixDest']) && $_SESSION['EditPackageStep2']['prefixDest']!='') { 
	$varPrefixDest = $_SESSION['EditPackageStep2']['prefixDest'];
}else {
	$varPrefixDest = $arrGetPackageInfoRes[0]['prefixdest'];
}
if(isset($_SESSION['EditPackageStep2']['prefixDestMode']) && $_SESSION['EditPackageStep2']['prefixDestMode']!='') { 
	$varPrefixDestMode = $_SESSION['EditPackageStep2']['prefixDestMode'];
}else {
	$varPrefixDestMode = $arrGetPackageInfoRes[0]['prefixdest_mode'];
}
if(isset($_SESSION['EditPackageStep2']['prefixCLIMode']) && $_SESSION['EditPackageStep2']['prefixCLIMode']!='') { 
	$varPrefixCLIMode = $_SESSION['EditPackageStep2']['prefixCLIMode'];
}else {
	$varPrefixCLIMode = trim($arrGetPackageInfoRes[0]['prefixcli_mode']);
}
if(isset($_SESSION['EditPackageStep2']['checkCLI']) && $_SESSION['EditPackageStep2']['checkCLI']!='') { 
	$varCheckCLI = $_SESSION['EditPackageStep2']['checkCLI'];
}else {
	$varCheckCLI = $arrGetPackageInfoRes[0]['check_cli'];
}
if(isset($_SESSION['EditPackageStep2']['maximumCLI']) && $_SESSION['EditPackageStep2']['maximumCLI']!='') { 
	$varMaxCLI = $_SESSION['EditPackageStep2']['maximumCLI'];
}else {
	$varMaxCLI = $arrGetPackageInfoRes[0]['max_cli'];
}
if(isset($_SESSION['EditPackageStep2']['maxAccount']) && $_SESSION['EditPackageStep2']['maxAccount']!='') { 
	$varMaxAccount = $_SESSION['EditPackageStep2']['maxAccount'];
}else {
	$varMaxAccount = (trim($arrGetPackageInfoRes[0]['max_account'])!='') ? number_format((float) trim($arrGetPackageInfoRes[0]['max_account']), 2, '.', '') : '';	
}
if(isset($_SESSION['EditPackageStep2']['checkAccount']) && $_SESSION['EditPackageStep2']['checkAccount']!='') { 
	$varCheckAccount = $_SESSION['EditPackageStep2']['checkAccount'];
}else {
	$varCheckAccount = (trim($arrGetPackageInfoRes[0]['check_account'])!='') ? number_format((float) trim($arrGetPackageInfoRes[0]['check_account']), 2, '.', '') : '';	
}
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Package Creation
        <small>Edit package</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Enroll Data</a></li-->
        <li class="active">Edit package</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit PACKAGE - <?php echo $varPackageId;?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="row">
						<!--  Add New Device Form  -->
						<div class="col-sm-12 col-md-7 col-xs-12">
							<form class="form-horizontal" action="<?php echo base_url();?>bundlepackagecreation/editPackageStep3" id="editPackageForm2" method="post">	
								<div id="hardware">									
									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Pack Connection Charge</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="packConnection" id="packConnection">
												<option value="">Select</option>
												 <?php foreach($arrPackConnectionCharge as $arrResult){ ?>
												 <option <?php if($varPackConnection!='') if($varPackConnection==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Pack Free Flag</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="packFreeFlag" id="packFreeFlag">
												<option value="">Select</option>
												 <?php foreach($arrPackFreeFlag as $arrResult){ ?>
												 <option <?php if($varPackFreeFlag!='') if($varPackFreeFlag==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Call Max TalkTime</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varMaxTalkTime;?>" placeholder="Call Max TalkTime" name="maxTalkTime" id="maxTalkTime" maxlength="4" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Check Destination</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="checkDestination" id="checkDestination">
												<option value="">Select</option>
												 <?php foreach($arrCheckDestination as $arrResult){ ?>
												 <option <?php if($varCheckDestination!='') if($varCheckDestination==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Maximum Destination</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varMaxDestination;?>" placeholder="Maximum Destination" name="maxDestination" id="maxDestination" maxlength="4" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Destination Delay</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varDestinationDelay;?>" placeholder="Destination Delay" name="destinationDelay" id="destinationDelay" maxlength="4" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Call Max Talk Time Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="maxTalkTimeMode" id="maxTalkTimeMode">
												<option value="">Select</option>
												 <?php foreach($arrCallMaxTalkTimeMode as $arrResult){ ?>
												 <option <?php if($varMaxTalkTimeMode!='') if($varMaxTalkTimeMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Pack Start Date</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<div class="input-group date" id="datepicker1">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" class="form-control pull-right" value="<?php echo $varPackStartDate;?>" id="packStartDate" name="packStartDate" readonly />
											</div>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Pack End Date</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<div class="input-group date" id="datepicker3">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" class="form-control pull-right" value="<?php echo $varPackEndDate;?>" id="packEndDate" name="packEndDate" readonly />
											</div>
										</div>
									</div>	
									<!--div class="form-group">
										<label class="col-sm-4 col-xs-12">Pack End Time</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php //echo $varPackEndTime;?>" placeholder="Pack End Time" name="packEndTime" id="packEndTime" maxlength="10" autocomplete="off" />
										</div>
									</div-->
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Sub Package</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="subPackage" id="subPackage">
												<option value="">Select</option>
												 <?php foreach($arrSubPackage as $arrResult){ ?>
												 <option <?php if($varSubPackage!='') if($varSubPackage==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Promo Balance Ratio</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control amount" value="<?php echo $varPromoBalance;?>" placeholder="Promo Balance Ratio" name="promoBalance" id="promoBalance" maxlength="6" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Extension Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="extensionMode" id="extensionMode">
												<option value="">Select</option>
												 <?php foreach($arrExtensionMode as $arrResult){ ?>
												 <option <?php if($varExtensionMode!='') if($varExtensionMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Info Day Code</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varInfoDayCode;?>" placeholder="Info Day Code" name="infoDayCode" id="infoDayCode" maxlength="10" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Number of Price/Unit</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varNoPriceUnit;?>" placeholder="Number of Price/Unit" name="noPriceUnit" id="noPriceUnit" maxlength="4" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Price Per Unit List</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varPricePerUnitList;?>" placeholder="Price Per Unit List" name="pricePerUnitList" id="pricePerUnitList" maxlength="45" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Master Notification</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varMasterNotif;?>" placeholder="Master Notification" name="masterNotif" id="masterNotif" maxlength="4" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Prefix Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="prefixMode" id="prefixMode">
												<option value="">Select</option>
												 <?php foreach($arrPrefixMode as $arrResult){ ?>
												 <option <?php if($varPrefixMode!='') if($varPrefixMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Usage Notification Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="usageNotifMode" id="usageNotifMode">
												<option value="">Select</option>
												 <?php foreach($arrUsageNotificationMode as $arrResult){ ?>
												 <option <?php if($varUsageNotifMode!='') if($varUsageNotifMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Usage Notification Type</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="usageNotifType" id="usageNotifType">
												<option value="">Select</option>
												 <?php foreach($arrUsageNotificationType as $arrResult){ ?>
												 <option <?php if($varUsageNotifType!='') if($varUsageNotifType==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Usage Notification Text</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varUsageNotifText;?>" placeholder="Usage Notification Text" name="usageNotifText" id="usageNotifText" maxlength="160" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Usage Notification Prompt</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varUsageNotifPrompt;?>" placeholder="Usage Notification Prompt" name="usageNotifPrompt" id="usageNotifPrompt" maxlength="50" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Prefix Destination</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varPrefixDest;?>" placeholder="Prefix Destination" name="prefixDest" id="prefixDest" maxlength="50" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Prefix Destination Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varPrefixDestMode;?>" placeholder="Prefix Destination Mode" name="prefixDestMode" id="prefixDestMode" maxlength="4" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Prefix CLI Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="prefixCLIMode" id="prefixCLIMode">
												<option value="">Select</option>
												 <?php foreach($arrPrefixCLIMode as $arrResult){ ?>
												 <option <?php if($varPrefixCLIMode!='') if($varPrefixCLIMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Check CLI</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varCheckCLI;?>" placeholder="Check CLI" name="checkCLI" id="checkCLI" maxlength="20" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Maximum CLI</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varMaxCLI;?>" placeholder="Maximum CLI" name="maximumCLI" id="maximumCLI" maxlength="20" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Maximum Account</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control amount" value="<?php echo $varMaxAccount;?>" placeholder="Maximum Account" name="maxAccount" id="maxAccount" maxlength="6" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Check Account</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control amount" value="<?php echo $varCheckAccount;?>" placeholder="Check Account" name="checkAccount" id="checkAccount" maxlength="6" autocomplete="off" />
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12 hidden-xs">&nbsp;</label>
										<div class="col-sm-7 col-md-7 col-xs-12">									
											<button type="button" id="step2BackButton" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp;Back</button>
											<button type="submit" class="btn btn-info">Next &nbsp;<i class="fa fa-arrow-right"></i></button>
										</div>
									</div>								
								</div>
							</form>
						</div>
					</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<!--  Modal Open  --->
<div class="modal fade" id="myBundleModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Get Bundle Group Information</h4>
      </div>
      <div class="modal-body">
        <div class="row">
			<!--  Add New Device Form  -->
			<div class="col-sm-12 col-md-12 col-xs-12">
				<form class="form-horizontal" id="searchBundleGroupForm" name="searchBundleGroupForm">		
					<div class="form-group">
						<label class="col-sm-3 col-xs-12 control-label">Bundle GroupId</label>
						<div class="col-sm-5 col-md-5 col-xs-9">
							<input type="text" class="form-control" value="" placeholder="Bundle GroupId" id="getBundleGroupId" name="getBundleGroupId" autocomplete="off" maxlength="15" />
						</div>
						<div class="col-sm-3 col-xs-9">
							<button type="submit" class="btn btn-info">Search</button>
						</div>
					</div>															
				</form>
			</div>
			<div class="col-sm-12 col-md-12 col-xs-12" id="resultBundleTableDiv" style="display:none;">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Heading 1</th>
							<th>Heading 2</th>
							<th>Heading 3</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>datat 1</td>
							<td>datat 2</td>
							<td>datat 3</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal End -->



 <?php //include('footer.php'); ?>

<script> 
 /*function onLoadReport(){
	$('#reportDiv').html('<div class="load-bg"><div class="loader"></div></div>');		
	var operatorId='';
	var reportType='';
	var usageType='';
	var startDate='';
	var endDate='';
	getAllOperatorProfitLossReports(operatorId,reportType,usageType,startDate,endDate);	
 }
	onLoadReport();*/
  
  $(document).ready(function() {
		 //alert('hai');	 	 
		 $('#step2BackButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundlepackagecreation/searchPackagePlan';
		 });		
		
		/* $("#searchBundleGroupForm").validate({
			submitHandler : function(form){	
				$("#resultBundleTableDiv").show();
				$('#resultBundleTableDiv').html('<div class="load-bg"><div class="loader"></div></div>');
				var getBundleGroupId = $("#getBundleGroupId").val();									
				var dataString = 'getBundleGroupId='+getBundleGroupId;	
				 $.ajax({
					   type: "POST",
					   url: "<?php echo base_url(); ?>bundleplan/getBundleGroupInfo",
					   data: dataString,
					   //dataType: 'json',
					   success: function(data){ 
							//alert(data);
							$("#resultBundleTableDiv").html(data);							
					   }
				 });	
				//form.submit();				
			},
			rules: {
				getBundleGroupId: "required",					
			},
			messages: {
				getBundleGroupId: "Please enter a bundle group id",					
			}			
		});		*/	 
		
		$('#datepicker1').datepicker({		  
			  format: 'yyyy-mm-dd',
			  autoclose: true,			  
		}).on('changeDate', function(selected){	
			// set the "toDate" start to not be later than "fromDate" ends:
			var startDate = new Date(selected.date.valueOf());
			$('#datepicker3').datepicker('setStartDate', startDate);
		});	
		
		$('#datepicker3').datepicker({		  
			 format: 'yyyy-mm-dd',
			autoclose: true		  		
		}).on('changeDate', function(selected){
			// set the "fromDate" end to not be later than "toDate" starts:
			var endDate = new Date(selected.date.valueOf());
			$('#datepicker1').datepicker('setEndDate', endDate);
		});	
    
  });
  
  $.validator.setDefaults({
		errorElement: "span",
		errorClass: "help-block",
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		errorPlacement: function (error, element) {
			if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
			error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});
	
	$(".plusBundleGroupIdButton").click(function(){
		  $("#resultBundleTableDiv").html('');
		  $("#getBundleGroupId").val('');
	  });	
	
	$('#pricePerUnitList, #infoDayCode, #usageNotifPrompt, #prefixDest').keyup(function() {  		   
			if (this.value.match(/[^a-zA-Z0-9 ]/g)) {
				this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^a-zA-Z0-9 ]/g, '')); 
					}, 5);  
            });	 
			
	$('#maxTalkTime, #maxDestination, #destinationDelay, #noPriceUnit, #masterNotif, #prefixDestMode, #maximumCLI, #checkCLI, #maxAccount, #checkAccount').keyup(function() {  		   
			if (this.value.match(/[^0-9 ]/g)) {
				this.value = this.value.replace(/[^0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^0-9 ]/g, '')); 
					}, 5);  
            });	 

	$('.amount').keypress(function(event) {
            if(event.which == 8 || event.which == 0){
                return true;
            }
            if(event.which < 46 || event.which > 59) {
                return false;
                //event.preventDefault();
            } // prevent if not number/dot

            if(event.which == 46 && $(this).val().indexOf('.') != -1) {
                return false;
                //event.preventDefault();
            } // prevent if already dot
			
			if ($(this).val().indexOf('.') > 0) {
				var len = $(this).val().length;
				var charAfterdot = (len + 1) - $(this).val().indexOf('.');
				if (charAfterdot > 3) {
					return false;
				}
			}
			
        });
  
	</script>