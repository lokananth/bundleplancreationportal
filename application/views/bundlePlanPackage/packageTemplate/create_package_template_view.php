<style type="text/css">	
#dt_basic_length{float:right;}
#dt_basic_wrapper .DTTT.btn-group{float:right; margin-left:10px;  margin-right:10px;}
.smart-style-5 div.DTTT .btn{margin:0px;}
@media only screen and (min-width : 480px)
{
#dt_basic_filter{float: left;width: 50%;}
}

.loader {
  width: 3em;
  height: 3em;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  border-radius: 50%;
  box-shadow: 0 0 5px #444;
 
}
.loader:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: linear-gradient(#009EE3 , black 90%);
  animation: spin .5s infinite linear;
}
.loader:after {
  content: "";
  position: absolute;
  width: 90%;
  height: 90%;
  top: 5%;
  left:5%;
  background-color:#fff;
  border-radius: 50%;
  box-shadow: inset 0 0 5px #444;
}

@keyframes spin {
  to {
    transform: rotate(360deg);
  }
}
.load-bg{
    position:absolute;
    top:0;
    left:0;   
    width:100%;
    height:100%;
    
    z-index:99999;
    display:block;
}
.state-error{ color:#ff0000;}
.datepicker + .invalid {
    left: 0;
    position: absolute;
    top: 100%;
}
.modal-body{padding:0 20px;}
.loader1{position:relative; float:left; min-height:200px; width:100%;} 

label input.checkbox[type="checkbox"] + span{float: right; position:absolute;top:0;left:0;}
table.complete-compare thead tr th,table.complete-compare tbody tr td{white-space: nowrap; !important;}
#changeUpdate{height:200px; resize: none;}

[data-tip] {
	position:relative;

}
[data-tip]:before {
	content:'';
	/* hides the tooltip when not hovered */
	display:none;
	content:'';
	border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	border-bottom: 5px solid #1a1a1a;	
	position:absolute;
	top:30px;
	left:35px;
	z-index:8;
	font-size:0;
	line-height:0;
	width:0;
	height:0;
}
[data-tip]:after {
	display:none;
	content:attr(data-tip);
	position:absolute;
	top:35px;
	left:0px;
	padding:5px 8px;
	background:#1a1a1a;
	color:#fff;
	z-index:9;
	font-size: 0.75em;
	height:18px;
	line-height:8px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	white-space:nowrap;
	word-wrap:normal;
}
[data-tip]:hover:before,
[data-tip]:hover:after {
	display:block;
}
</style>
<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>
<?php
$varPackageId = $varInsertPackageId;
$varPackageTypeId = '';
$varTariffClass = '';
//$varAccessType='';
$varAccessDid='';
$varComment='';
$varMaxAccess='';
$varDaysValid = '';
$varPackConnection = '';
$varExpiredDate = '';
$varDestinationCheck = '';
$varRenewalDelay = '';
$varMaxTalkTime = '';
$varExpiredDateMode = '';
$varRenewalMode = '';
$varExpiredDateParam = '';
$varUsageMode = '';
$varPackFreeFlag = '';
$varPromoStartDateMode = '';
$varPromoStartDateParam = '';
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Package Template
        <small>Add new package template</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Enroll Data</a></li-->
        <li class="active">Create new package template</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">ADD NEW PACKAGE</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="row">
						<!--  Add New Device Form  -->
						<div class="col-sm-12 col-md-7 col-xs-12">
							<form class="form-horizontal" action="<?php echo base_url();?>bundlepackagetemplate/addPackageTemplate" id="createPackageForm1" method="post">	
								<div id="hardware">
									<!--div class="form-group">
										<label class="col-sm-4 col-xs-12">Bundle ID</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="" placeholder="test" />
										</div>
									</div-->
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Site Code</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $_SESSION['SiteCode']; ?>" placeholder="Site Code" name="siteCode" id="siteCode" maxlength="20" readonly autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Package ID<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varPackageId;?>" placeholder="Package ID" name="packageId" id="packageId" <?php if($varPackageId!=''){ ?> readonly <?php } ?> maxlength="4" autocomplete="off" />
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Package Type Id<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="packageTypeId" id="packageTypeId">
												<option value="">Select</option>
												 <?php foreach($arrPackageTypeId as $arrResult){ ?>
												 <option <?php if($varPackageTypeId!='') if($varPackageTypeId==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Currency Code</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $_SESSION['Currency']; ?>" placeholder="Currency Code" name="currency" id="currency" maxlength="20" readonly autocomplete="off" />											
										</div>
									</div>									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Tariff Class<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12" data-tip="Type Capital letter only">
											<input type="text" class="form-control" value="<?php echo $varTariffClass;?>" placeholder="Tariff Class" name="tariffClass" id="tariffClass" maxlength="4" onkeydown="restrictSpace(this.id);" autocomplete="off" />
										</div>
									</div>
									<!--div class="form-group">
										<label class="col-sm-4 col-xs-12">Access Type</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php //echo $varAccessType;?>" placeholder="Access Type" name="accessType" id="accessType" maxlength="4" autocomplete="off" />
										</div>
									</div-->									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Access Did</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varAccessDid;?>" placeholder="Access Did" name="accessDid" id="accessDid" maxlength="16" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Comment</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varComment;?>" placeholder="Comment" name="comment" id="comment" maxlength="30" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Maximum Access</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varMaxAccess;?>" placeholder="Maximum Access" name="maxAccess" id="maxAccess" maxlength="4" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Days Valid<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varDaysValid;?>" placeholder="Days Valid" name="daysValid" id="daysValid" maxlength="4" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Pack Connection Charge</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="packConnection" id="packConnection">
												<option value="">Select</option>
												 <?php foreach($arrPackConnectionCharge as $arrResult){ ?>
												 <option <?php if($varPackConnection!='') if($varPackConnection==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Expired Date</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<div class="input-group date" id="datepicker1">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" class="form-control pull-right" value="<?php echo $varExpiredDate;?>" id="expiredDate" name="expiredDate" readonly />
											</div>
										</div>
									</div>									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Expired Date Mode<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="expiredDateMode" id="expiredDateMode">
												<option value="">Select</option>
												 <?php foreach($arrExpiredDateMode as $arrResult){ ?>
												 <option <?php if($varExpiredDateMode!='') if($varExpiredDateMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Expired Date Param</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varExpiredDateParam;?>" placeholder="Expired Date Param" name="expiredDateParam" id="expiredDateParam" maxlength="25" autocomplete="off" />
										</div>
										<div class="col-sm-7 col-md-7 col-xs-12">
										 <?php foreach($arrExpiredDateParam as $arrResult){ ?>
											<?php echo $arrResult['Refid'].' - '.$arrResult['Name'].'<br>'; ?>
										 <?php } ?>	
										 </div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Usage Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="usageMode" id="usageMode">
												<option value="">Select</option>
												 <?php foreach($arrUsageMode as $arrResult){ ?>
												 <option <?php if($varUsageMode!='') if($varUsageMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>		
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Pack Free Flag</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="packFreeFlag" id="packFreeFlag">
												<option value="">Select</option>
												 <?php foreach($arrPackFreeFlag as $arrResult){ ?>
												 <option <?php if($varPackFreeFlag!='') if($varPackFreeFlag==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Renewal Mode<span style="color:red;">*</span></label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="renewalMode" id="renewalMode">
												<option value="">Select</option>
												 <?php foreach($arrRenewalMode as $arrResult){ ?>
												 <option <?php if($varRenewalMode!='') if($varRenewalMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Renewal Delay</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varRenewalDelay;?>" placeholder="Renewal Delay" name="renewalDelay" id="renewalDelay" maxlength="4" autocomplete="off" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Call Max TalkTime</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varMaxTalkTime;?>" placeholder="Call Max TalkTime" name="maxTalkTime" id="maxTalkTime" maxlength="4" autocomplete="off" />
										</div>
									</div>										
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Destination Check</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varDestinationCheck;?>" placeholder="Destination Check" name="destinationCheck" id="destinationCheck" maxlength="4" autocomplete="off" />
										</div>
									</div>									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Promo Start Date Mode</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<select class="form-control" name="promoStartDateMode" id="promoStartDateMode">
												<option value="">Select</option>
												 <?php foreach($arrPromoStartDateMode as $arrResult){ ?>
												 <option <?php if($varPromoStartDateMode!='') if($varPromoStartDateMode==$arrResult['Refid']){ ?> selected <?php } ?> value="<?php echo $arrResult['Refid']; ?>"><?php echo $arrResult['Name']; ?></option>
													<?php } ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 col-xs-12">Promo Start Date Param</label>
										<div class="col-sm-7 col-md-7 col-xs-12">
											<input type="text" class="form-control" value="<?php echo $varPromoStartDateParam;?>" placeholder="Promo Start Date Param" name="promoStartDateParam" id="promoStartDateParam" maxlength="25" autocomplete="off" />
										</div>
										<div class="col-sm-7 col-md-7 col-xs-12">
										 <?php foreach($arrPromoStartDateParam as $arrResult){ ?>
											<?php echo $arrResult['Refid'].' - '.$arrResult['Name'].'<br>'; ?>
										 <?php } ?>	
										 </div>
									</div>	
									
									<div class="form-group">
										<label class="col-sm-4 col-xs-12 hidden-xs">&nbsp;</label>
										<div class="col-sm-7 col-md-7 col-xs-12">									
											<button type="submit" class="btn btn-info">Add</button>
											<button type="button" id="step1CancelButton" class="btn btn-default">Cancel</button>
											
										</div>
									</div>								
								</div>
							</form>
						</div>
					</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<!--  Modal Open  --->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Get Tariff Group Information</h4>
      </div>
      <div class="modal-body">
        <div class="row">
			<!--  Add New Device Form  -->
			<div class="col-sm-12 col-md-12 col-xs-12">
				<form class="form-horizontal" id="searchGroupForm" name="searchGroupForm">		
					<div class="form-group">
						<label class="col-sm-3 col-xs-12 control-label">Tariff GroupId</label>
						<div class="col-sm-5 col-md-5 col-xs-9">
							<input type="text" class="form-control" value="" placeholder="Tariff GroupId" id="tariffGroupId" name="tariffGroupId" autocomplete="off" maxlength="15" />
						</div>
						<div class="col-sm-3 col-xs-9">
							<button type="submit" class="btn btn-info">Search</button>
						</div>
					</div>															
				</form>
			</div>
			<div class="col-sm-12 col-md-12 col-xs-12" id="resultTableDiv" style="display:none;">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Heading 1</th>
							<th>Heading 2</th>
							<th>Heading 3</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>datat 1</td>
							<td>datat 2</td>
							<td>datat 3</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal End -->

 <?php //include('footer.php'); ?>

<script> 
 /*function onLoadReport(){
	$('#reportDiv').html('<div class="load-bg"><div class="loader"></div></div>');		
	var operatorId='';
	var reportType='';
	var usageType='';
	var startDate='';
	var endDate='';
	getAllOperatorProfitLossReports(operatorId,reportType,usageType,startDate,endDate);	
 }
	onLoadReport();*/
  
  $(document).ready(function() {
		 //alert('hai');	 	 
		 $('#step1CancelButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundlepackagetemplate';
		 });

		$("#createPackageForm1").validate({			
			rules: {
				siteCode: "required",
				packageId: "required",
				packageTypeId: "required",				
				currency: "required",
				//tariffClass: "required",
				tariffClass: {
				  required: true,
				  minlength: 4
				},
				daysValid: "required",
				expiredDateMode: "required",
				renewalMode: "required",				
			},
			messages: {
				siteCode: "Please select a sitecode",
				packageId: "Please enter a package id",
				packageTypeId: "Please select a package type",				
				currency: "Please enter a currency",
				//tariffClass: "Please enter a tariff Class",
				tariffClass: {
					  required: "Please enter a tariff Class",					  
					},
				daysValid: "Please enter a days valid",
				expiredDateMode: "Please select a Expired Date Mode",
				renewalMode: "Please select a renewal mode",				
			}			
		});	
		
		/*$("#searchGroupForm").validate({
			submitHandler : function(form){	
				$("#resultTableDiv").show();
				$('#resultTableDiv').html('<div class="load-bg"><div class="loader"></div></div>');
				var tariffGroupId = $("#tariffGroupId").val();									
				var dataString = 'tariffGroupId='+tariffGroupId;	
				 $.ajax({
					   type: "POST",
					   url: "<?php echo base_url(); ?>bundleplan/getTariffGroupInfo",
					   data: dataString,
					   //dataType: 'json',
					   success: function(data){ 
							//alert(data);
							$("#resultTableDiv").html(data);							
					   }
				 });	
				//form.submit();				
			},
			rules: {
				tariffGroupId: "required",					
			},
			messages: {
				tariffGroupId: "Please enter a tariff group id",					
			}			
		});	*/
					
	  
	  $("#siteCode").blur(function(){
		  $("#siteCode").valid();
	  });		
	  
	  $("#packageId").blur(function(){
		  $("#packageId").valid();
	  });		
	  
	  $("#packageTypeId").blur(function(){
		  $("#packageTypeId").valid();
	  });		
	  
	  $("#currency").blur(function(){
		  $("#currency").valid();
	  });		
	  
	  $("#tariffClass").blur(function(){
		  $("#tariffClass").valid();
	  });		
	  
	  $("#daysValid").blur(function(){
		  $("#daysValid").valid();
	  });		
	  
	  $("#expiredDateMode").blur(function(){
		  $("#expiredDateMode").valid();
	  });			  		  
	  
	  $("#renewalMode").blur(function(){
		  $("#renewalMode").valid();
	  });
	  			
  });
  
  
  $.validator.setDefaults({
		errorElement: "span",
		errorClass: "help-block",
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		errorPlacement: function (error, element) {
			if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
			error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});	
	
	$("#plusButton").click(function(){
		  $("#resultTableDiv").html('');
		  $("#tariffGroupId").val('');
	  });	
	
	$('#expiredDateParam, #promoStartDateParam, #accessDid').keyup(function() {  		   
			if (this.value.match(/[^a-zA-Z0-9 ]/g)) {
				this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^a-zA-Z0-9 ]/g, '')); 
					}, 5);  
            });	

	$('#tariffClass').keyup(function() {  		   
			if (this.value.match(/[^A-Z0-9 ]/g)) {
				this.value = this.value.replace(/[^A-Z0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^A-Z0-9 ]/g, '')); 
					}, 5);  
            });	 			
			
	$('#packageId, #daysValid, #renewalDelay, #maxTalkTime, #destinationCheck, #maxAccess').keyup(function() {  		   
			if (this.value.match(/[^0-9 ]/g)) {
				this.value = this.value.replace(/[^0-9 ]/g, '');
		  }
		}).on('paste', function (e) {  
					var $this = $(this);  
					setTimeout(function () {  
						$this.val($this.val().replace(/[^0-9 ]/g, '')); 
					}, 5);  
            });	 

	$('.amount').keypress(function(event) {
            if(event.which == 8 || event.which == 0){
                return true;
            }
            if(event.which < 46 || event.which > 59) {
                return false;
                //event.preventDefault();
            } // prevent if not number/dot

            if(event.which == 46 && $(this).val().indexOf('.') != -1) {
                return false;
                //event.preventDefault();
            } // prevent if already dot
			
			if ($(this).val().indexOf('.') > 0) {
				var len = $(this).val().length;
				var charAfterdot = (len + 1) - $(this).val().indexOf('.');
				if (charAfterdot > 3) {
					return false;
				}
			}
			
        });
		
		function restrictSpace(getId) {
		$('#'+getId).keydown(function (e) {	
			if (e.ctrlKey || e.altKey) {
			e.preventDefault();
			} else {
			var key = e.keyCode;
			//alert(key);
			if (key == 32) {
			e.preventDefault();
			}
			}
			});
		} 
  
	</script>