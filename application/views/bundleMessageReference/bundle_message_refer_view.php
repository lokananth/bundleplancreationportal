<?php //include('top_header.php'); ?>
<?php //include('left_menu.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Bundle Message Reference
        <small>Add bundle message</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>bundlemodule"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--li><a href="#">Device list</a></li-->
        <li class="active">Bundle Message Reference</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Bundle Message Reference</h3>
						<div class="box-tools pull-right">
							<!-- <span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="3 peoples are late">3</span> -->
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form class="form-horizontal" action="<?php echo base_url();?>bundlemsgrefer/addBundleMsgReference" id="addBundleMsgReferenceForm" method="post">
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Message ID<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<input type="text" name="messageId" id="messageId" value="" class="form-control" placeholder="Enter message ID" maxlength="6" autocomplete="off" />
									</div>									
								</div>															
							</div>
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3">Comment<span style="color:red;">*</span></label>
									<div class="col-sm-5 col-md-5">
										<input type="text" name="comment" id="comment" value="" class="form-control" placeholder="Enter Comment" maxlength="50" autocomplete="off" />
									</div>									
								</div>															
							</div>	
							<div class="col-sm-12 col-md-10">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-3 hidden-xs">&nbsp;</label>
									<div class="col-sm-5 col-md-5">
										<button type="submit" class="btn btn-primary">Add</button>
										<button type="button" id="editBundleMessageReferButton" class="btn btn-primary">Edit</button>
									</div>
								</div>
							</div>
						</form>							
					</div>
				</div>
			</div>    
        
       
      </div>
	  <?php if($this->session->flashdata('successmsg')!=''){ ?>
				<div class="alert alert-success text-center" id="successmsg" >
				  <?php echo $this->session->flashdata('successmsg'); ?>
				  </div>
		<?php } ?>
		<?php if($this->session->flashdata('errormsg')!=''){ ?>
				<div class="alert alert-error text-center" id="errormsg" >
				  <?php echo $this->session->flashdata('errormsg'); ?>
				  </div>
		<?php } ?>
      <!-- /.row -->
    </section>
    <!-- /.content -->	
	
<section class="content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
		<!-- widget content -->
		<div class="widget-body no-padding" id="bundlesDiv">
			<?php if(isset($arrGetBundleMessageReference[0]['errcode']) && $arrGetBundleMessageReference[0]['errcode']=='0'){ ?> 
			<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
				<thead>
					<tr>
						<th data-class="expand">MessageId</th>
						<th data-hide="phone">Comments</th>																
					</tr>
				</thead>
				<tbody>
				<?php $i=1; foreach($arrGetBundleMessageReference as $arrResult){  ?>	
					<tr>
						<td><?php echo $arrResult['messageid']; ?></td>
						<td><?php echo $arrResult['comment']; ?></td>																
					</tr>
					<?php $i++; } ?>												
				</tbody>
			</table>
			<?php }else{ ?>	
			<br><br><b><center> No records found</center></b>
			<?php } ?>
		</div>
		<!-- end widget content -->

	</div>
	<!-- end widget div -->
  </div>
  </div>
  </div>
   </section>
  <!-- /.content-wrapper -->
  
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-1 col-sm-offset-1 col-md-8 col-md-offset-2 col-xs-12">
							<form class="form-horizontal" action="">
								<div class="form-group">
									<label class="col-sm-12 col-xs-12 text-center">Assign ICCID Number</label>
									<div class="col-sm-12 col-xs-12">
										<input type="text" class="form-control" value="" placeholder="ICCID Number" />
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-12 col-xs-12 text-center">
										<input type="submit" class="btn btn-primary" value="Reset" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>				
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">OK</button>				
				</div>
			</div>
		</div>
	</div>
 <?php //include('footer.php'); ?>
 
 
 
 <script>   
  $(document).ready(function() {
		 //alert('hai');	 	 
		$('#datatable_tabletools').DataTable();	
		$('#editBundleMessageReferButton').on('click', function() {	
			window.location.href = '<?php echo base_url();?>bundlemsgrefer/getByBundleMsgReferenceId';
		 });
		 
		 $("#addBundleMsgReferenceForm").validate({			
			rules: {
				messageId: "required",
				comment: "required",							
			},
			messages: {
				messageId: "Please enter the bundle Id",
				comment: "Please enter the comments",								
			}			
		});						
	  
	 $("#messageId").blur(function(){
		  $("#messageId").valid();
	  });
	  
	  $("#comment").blur(function(){
		  $("#comment").valid();
	  });
	
		$('#messageId').on('click', function() {	
			$("#errormsg").hide();
			$("#successmsg").hide();
		 });	
    
  });
  
  $.validator.setDefaults({
			errorElement: "span",
			errorClass: "help-block",
			highlight: function(element) {
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			},
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
				error.insertAfter(element.parent());
				} else {
					error.insertAfter(element);
				}
			}
		});		
	
	$('#messageId').keyup(function() {  		   
		if (this.value.match(/[^0-9 ]/g)) {
			this.value = this.value.replace(/[^0-9 ]/g, '');
	  }
	}).on('paste', function (e) {  
				var $this = $(this);  
				setTimeout(function () {  
					$this.val($this.val().replace(/[^0-9 ]/g, '')); 
				}, 5);  
	});	 
	</script>