 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div> -->
      <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" id="sidebar-menu">
        <li class="header">ADMIN</li>
        <li>
          <a href="<?php echo base_url();?>bundlemodule">
            <i class="fa fa-home"></i> <span>Home</span>
          </a>
        </li>        
<!--         <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Device List</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="hardware.php"><i class="fa fa-plus text-aqua"></i>Hardware</a></li>
            <li><a href="software.php"><i class="fa fa-table text-green"></i>Software</a></li>
          </ul>
        </li>
		<li>
          <a href="issue_list.php">
            <i class="fa fa-warning"></i> <span>Issue Tracking</span><span class="pull-right-container">
              <small class="label pull-right bg-yellow" >12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span>
          </a>
        </li>	
 
		<li>
          <a href="javascript:void(0);">
            <i class="fa fa-dashboard"></i> <span>Software Device</span>
          </a>
        </li>	


		<li>
          <a href="javascript:void(0);">
            <i class="fa fa-dashboard"></i> <span>Hardware Device</span>
          </a>
        </li>			
       <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>REPORTS</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="lateness.php"><i class="fa fa-clock-o text-yellow"></i>Lateness</a></li>
            <li><a href="absence.php"><i class="fa fa-user-times text-red"></i>Absence</a></li>
            <li class="active"><a href="productivity.php"><i class="fa fa-hourglass-half text-aqua"></i>Weekly Hours</a></li>
          </ul>
        </li>  
        <li class="treeview">
          <a href="notifications.php">
            <i class="fa fa-gear"></i> <span>Settings</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-clock-o text-yellow"></i>Department List</a></li>
            <li><a href="#"><i class="fa fa-user-times text-red"></i>Managers List</a></li>
            <li class="active"><a href="#"><i class="fa fa-hourglass-half text-aqua"></i>Shift List</a></li>
          </ul>
        </li>    -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>