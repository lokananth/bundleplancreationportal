<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bundlepackage extends CI_Controller {	
	function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {
				session_start();
		}		
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{				
		$data = array();		
		
		//Bundle list (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'32');
		$arrGetBundleInfoRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrGetBundleInfoRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrGetBundleInfoRes'] = $arrGetBundleInfoRes;
		
		//BundleId
		/*$params = array('sitecode'=>$_SESSION['SiteCode'],'Bundleid'=>'-1');
		$arrGetBundleInfoRes = ApiPostHeader($this->config->item('SearchBundlePlan'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundleInfoRes);echo $this->config->item('SearchBundlePlan');exit;			
		$data['arrGetBundleInfoRes']=$arrGetBundleInfoRes;*/
		
		//PackageId list (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'33');
		$arrGetBundlePackagesInfoRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrGetBundlePackagesInfoRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrGetBundlePackagesInfo'] = $arrGetBundlePackagesInfoRes;
		
		//PackageId
		/*$params = array('sitecode'=>$_SESSION['SiteCode'],'package_id'=>'-1');
		$arrGetBundlePackagesInfoRes = ApiPostHeader($this->config->item('GetBundlePackages'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundlePackagesInfoRes);echo $this->config->item('GetBundlePackages');exit;			
		$data['arrGetBundlePackagesInfo']=$arrGetBundlePackagesInfoRes;*/
		
		//Update Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'29');
		$arrUpdateModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrUpdateModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrUpdateMode'] = $arrUpdateModeRes;
		
		//Renewal Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'27');
		$arrRenewalModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrRenewalModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrRenewalMode'] = $arrRenewalModeRes;
		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlanPackage/packagePlan/add_package_plan_view',$data);
		$this->load->view('footer_view');
	}
	
	public function getAllBundlePlanPackageInfo(){
		ini_set("memory_limit", "-1");
		ini_set('max_execution_time', 900);										
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>'-1','packageid'=>'-1');		
		$arrGetAllPackageIdRes = ApiPostHeader($this->config->item('GetBundlePackagePlan'), $params);		
		//echo '<pre>';print_r($params);print_r($arrGetAllPackageIdRes);echo $this->config->item('GetBundlePackagePlan');exit;				
		
		$varResult='';
		if((isset($arrGetAllPackageIdRes[0]['errcode']) && $arrGetAllPackageIdRes[0]['errcode']=='0') || isset($arrGetAllPackageIdRes['errcode']) && $arrGetAllPackageIdRes['errcode']=='0'){	
			$varResult .= '<script type="text/javascript">
				$("#datatable_tabletools").DataTable();	
				</script>';
				
			$varResult .= '<table id="datatable_tabletools" class="table table-bordered table-striped nowrap dt-responsive" width="100%">
				<thead>
					<tr>
						<th data-class="expand">BundleId</th>
						<th data-hide="phone">PackageId</th>
						<th data-hide="phone">Update Mode</th>
						<th data-hide="phone">Package Balance</th>										
					</tr>
				</thead>
				<tbody>';
			foreach($arrGetAllPackageIdRes as $arrResult){														  
				 $varResult .= "<tr><td>".$arrResult['bundleid']."</td>
								<td>".$arrResult['packageid']."</td>								
								<td>".$arrResult['updatemode']."</td>
								<td>".number_format((float) $arrResult['packagebalance'], 2, '.', '')."</td>	
								</tr>";								
			}				
			$varResult .=	'</tbody>
								</table>';		
		}else{
			$varResult = '';			
		}
		echo $varResult;exit;
	}
	
	public function addBundlePackagePlan(){
		//echo '<pre>';print_r($_REQUEST);exit;
		$varBundleId = trim($_REQUEST['bundleId']);
		$varPackageId = trim($_REQUEST['packageId']);
		$varUpdateMode = trim($_REQUEST['updateMode']);
		$varPackageBalance = trim($_REQUEST['packBalance']);
		//$varRenewalMode = trim($_REQUEST['renewalMode']);		
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>$varBundleId,'packageid'=>$varPackageId,'updatemode'=>$varUpdateMode,'packagebalance'=>$varPackageBalance,'renewal_mode'=>'','process_flag'=>'1','ip_address'=>getClientIp());
		$arrAddBundlePackagePlanId = ApiPostHeader($this->config->item('InsertUpdatePackagePlan'), $params);
		//echo '<pre>';print_r($params);print_r($arrAddBundlePackagePlanId);echo $this->config->item('InsertUpdatePackagePlan');exit;
		
		if($arrAddBundlePackagePlanId[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrAddBundlePackagePlanId[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrAddBundlePackagePlanId[0]['errmsg']);
		}			
		redirect('bundlepackage');
	}
	
	public function getPackagePlanByBundleId($varCondition='')
	{	
		$data = array();		
		//BundleId
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Bundleid'=>'-1');
		$arrGetBundleInfoRes = ApiPostHeader($this->config->item('SearchBundlePlan'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundleInfoRes);echo $this->config->item('SearchBundlePlan');exit;			
		$data['arrGetBundleInfoRes']=$arrGetBundleInfoRes;
		
		//PackageId
		$params = array('sitecode'=>$_SESSION['SiteCode'],'package_id'=>'-1');
		$arrGetBundlePackagesInfoRes = ApiPostHeader($this->config->item('GetBundlePackages'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundlePackagesInfoRes);echo $this->config->item('GetBundlePackages');exit;			
		$data['arrGetBundlePackagesInfo']=$arrGetBundlePackagesInfoRes;
		
		//Update Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'29');
		$arrUpdateModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrUpdateModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrUpdateMode'] = $arrUpdateModeRes;
		
		//Renewal Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'27');
		$arrRenewalModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrRenewalModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrRenewalMode'] = $arrRenewalModeRes;
		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		if($varCondition=='1'){
			$this->load->view('bundlePlanPackage/packagePlan/edit_package_plan_view',$data);
		}else if($varCondition=='2'){
			$this->load->view('bundlePlanPackage/packagePlan/delete_package_plan_view',$data);
		}
		$this->load->view('footer_view');
	}
	
	public function getPackageIdByBundleId(){
		$varBundleId =  $this->input->post('bundleId');						
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>$varBundleId,'packageid'=>'-1');		
		$arrGetPackageIdRes = ApiPostHeader($this->config->item('GetBundlePackagePlan'), $params);		
		//echo '<pre>';print_r($params);print_r($arrGetPackageIdRes);echo $this->config->item('GetBundlePackagePlan');exit;		
		$varResult='';
		if((isset($arrGetPackageIdRes[0]['errcode']) && $arrGetPackageIdRes[0]['errcode']=='0') || isset($arrGetPackageIdRes['errcode']) && $arrGetPackageIdRes['errcode']=='0'){
			$varResult .='<option value="">Select</option>';
			foreach($arrGetPackageIdRes as $arrResult){ 
				$varResult .= "<option value=".$arrResult['packageid'].">".$arrResult['packageid']."</option>";
			}
			echo $varResult;exit;	
		}else{
			echo $varResult;exit;	
		}		
	}
	
	public function getBundlePackageIdInfo(){
		$varBundleId =  $this->input->post('bundleId');				
		$varPackageId =  $this->input->post('packageId');
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>$varBundleId,'packageid'=>$varPackageId);		
		$arrGetPackageIdRes = ApiPostHeader($this->config->item('GetBundlePackagePlan'), $params);		
		//echo '<pre>';print_r($params);print_r($arrGetPackageIdRes);echo $this->config->item('GetBundlePackagePlan');exit;		
		$varResult='';
		if((isset($arrGetPackageIdRes[0]['errcode']) && $arrGetPackageIdRes[0]['errcode']=='0') || isset($arrGetPackageIdRes['errcode']) && $arrGetPackageIdRes['errcode']=='0'){						
											
			$arrResult['bundleid'] = $arrGetPackageIdRes[0]['bundleid'];
			$arrResult['packageid'] = $arrGetPackageIdRes[0]['packageid'];					
			$arrResult['updatemode'] = $arrGetPackageIdRes[0]['updatemode'];
			$arrResult['packagebalance'] = $arrGetPackageIdRes[0]['packagebalance'];
			//$arrResult['renewal_mode'] = $arrGetPackageIdRes[0]['renewal_mode'];
		}else{			
			$arrResult = '';			
		}
		echo json_encode($arrResult);exit;	
	}
	
	public function updateBundlePackagePlan(){
		//echo '<pre>';print_r($_REQUEST);exit;
		$varBundleId = trim($_REQUEST['bundleId']);
		$varPackageId = trim($_REQUEST['packageId']);
		$varUpdateMode = trim($_REQUEST['updateMode']);
		$varPackageBalance = trim($_REQUEST['packBalance']);
		//$varRenewalMode = trim($_REQUEST['renewalMode']);		
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>$varBundleId,'packageid'=>$varPackageId,'updatemode'=>$varUpdateMode,'packagebalance'=>$varPackageBalance,'renewal_mode'=>'','process_flag'=>'2','ip_address'=>getClientIp());
		$arrAddBundlePackagePlanId = ApiPostHeader($this->config->item('InsertUpdatePackagePlan'), $params);
		//echo '<pre>';print_r($params);print_r($arrAddBundlePackagePlanId);echo $this->config->item('InsertUpdatePackagePlan');exit;
		
		if($arrAddBundlePackagePlanId[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrAddBundlePackagePlanId[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrAddBundlePackagePlanId[0]['errmsg']);
		}			
		redirect('bundlepackage');
	}
	
	public function deleteBundlePackagePlan(){
		//echo '<pre>';print_r($_REQUEST);exit;
		$varBundleId = trim($_REQUEST['bundleId']);
		$varPackageId = trim($_REQUEST['selPackageId']);			
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>$varBundleId,'packageid'=>$varPackageId,'ip_address'=>getClientIp());
		$arrDeleteBundlePackagePlanId = ApiPostHeader($this->config->item('DeleteBundlePackagePlan'), $params);
		//echo '<pre>';print_r($params);print_r($arrDeleteBundlePackagePlanId);echo $this->config->item('DeleteBundlePackagePlan');exit;
		
		if($arrDeleteBundlePackagePlanId[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrDeleteBundlePackagePlanId[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrDeleteBundlePackagePlanId[0]['errmsg']);
		}			
		redirect('bundlepackage');
	}
}
?>