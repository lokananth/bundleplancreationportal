<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bundlerouting extends CI_Controller {	
	function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {
				session_start();
		}		
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{				
		$data = array();		
		//Service Type (Drop Down)
		$params = array('sitecode'=>$_SESSION['SiteCode'],'process_flag'=>'1');		
		$arrGetServiceTypeRes = ApiPostHeader($this->config->item('GetServiceType'), $params);		
		//echo '<pre>';print_r($params);print_r($arrGetServiceTypeRes);echo $this->config->item('GetServiceType');exit;	
		$data['arrGetServiceType']=$arrGetServiceTypeRes;				
		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundleRouting/add_ussd_routing_view',$data);
		$this->load->view('footer_view');
	}
	
	public function getAllBundleRoutingList(){
		ini_set("memory_limit", "-1");
		ini_set('max_execution_time', 900);										
		
		//Get all USSD Routing
		$params = array('sitecode'=>$_SESSION['SiteCode'],'ussd_prefix'=>'-1');
		$arrGetAllUSSDRoutingRes = ApiPostHeader($this->config->item('GetUSSDRouting'), $params);
		//echo '<pre>';print_r($arrGetAllUSSDRoutingRes);print_r($_SESSION);echo $this->config->item('GetUSSDRouting');exit;								
		
		$varResult='';
		if((isset($arrGetAllUSSDRoutingRes[0]['errcode']) && $arrGetAllUSSDRoutingRes[0]['errcode']=='0') || isset($arrGetAllUSSDRoutingRes['errcode']) && $arrGetAllUSSDRoutingRes['errcode']=='0'){	
			$varResult .= '<script type="text/javascript">
				$("#datatable_tabletools").DataTable();	
				</script>';
				
			$varResult .= '<table id="datatable_tabletools" class="table table-bordered table-striped nowrap dt-responsive" width="100%">
				<thead>
					<tr>
						<th data-class="expand">USSD Prefix</th>
						<th data-hide="phone">Bundle Id/Group Id</th>
						<th data-hide="phone">Service Type</th>
						<th data-hide="phone">Sitecode</th>										
					</tr>
				</thead>
				<tbody>';
			foreach($arrGetAllUSSDRoutingRes as $arrResult){														  
				 $varResult .= "<tr><td>".$arrResult['USSD_PREFIX']."</td>
								<td>".$arrResult['DESTINATION_ADDRESS']."</td>								
								<td>".$arrResult['servicetype']."</td>								
								<td>".$arrResult['sitecode']."</td>
								</tr>";								
			}				
			$varResult .=	'</tbody>
								</table>';		
		}else{
			$varResult = '';			
		}
		echo $varResult;exit;
	}
	
	public function addBundleUSSDRouting(){
		//echo '<pre>';print_r($_REQUEST);exit;
		$varServiceTypeText = trim($_REQUEST['serviceTypeText']);
		$varUSSDPrefix = trim($_REQUEST['ussdPrefix']);
		$varBundleId = trim($_REQUEST['bundleId']);			
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'servicetype'=>$varServiceTypeText,'ussd_prefix'=>$varUSSDPrefix,'destination_address'=>$varBundleId,'process_flag'=>'1','ip_address'=>getClientIp());
		$arrAddUSSDRouting = ApiPostHeader($this->config->item('InsertUpdateUSSDRouting'), $params);
		//echo '<pre>';print_r($params);print_r($arrAddUSSDRouting);echo $this->config->item('InsertUpdateUSSDRouting');exit;
		
		if($arrAddUSSDRouting[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrAddUSSDRouting[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrAddUSSDRouting[0]['errmsg']);
		}			
		redirect('bundlerouting');
	}
	
	public function getBundleUSSDRouting($varCondition='')
	{	
		$data = array();		
		//Service Type (Drop Down)
		$params = array('sitecode'=>$_SESSION['SiteCode'],'process_flag'=>'1');		
		$arrGetServiceTypeRes = ApiPostHeader($this->config->item('GetServiceType'), $params);		
		//echo '<pre>';print_r($params);print_r($arrGetServiceTypeRes);echo $this->config->item('GetServiceType');exit;	
		$data['arrGetServiceType']=$arrGetServiceTypeRes;				
		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		if($varCondition=='1'){
			$this->load->view('bundleRouting/edit_ussd_routing_view',$data);
		}else if($varCondition=='2'){
			$this->load->view('bundleRouting/delete_ussd_routing_view',$data);
		}
		$this->load->view('footer_view');
	}
	
	public function getServiceTypeByPrefix(){
		//echo '<pre>';print_r($_REQUEST);exit;
		$varUSSDPrefix =  trim($this->input->post('ussdPrefix'));						
		$params = array('sitecode'=>$_SESSION['SiteCode'],'ussd_prefix'=>$varUSSDPrefix);		
		$arrGetUSSDRoutingRes = ApiPostHeader($this->config->item('GetUSSDRouting'), $params);		
		//echo '<pre>';print_r($params);print_r($arrGetUSSDRoutingRes);echo $this->config->item('GetUSSDRouting');exit;		
		$arrResult=array();
		if((isset($arrGetUSSDRoutingRes[0]['errcode']) && $arrGetUSSDRoutingRes[0]['errcode']=='0') || isset($arrGetUSSDRoutingRes['errcode']) && $arrGetUSSDRoutingRes['errcode']=='0'){				
			$arrResult['errcode']= $arrGetUSSDRoutingRes[0]['errcode'];
			$arrResult['servicetype']= base64_encode($arrGetUSSDRoutingRes[0]['servicetype']);
			$arrResult['bundleId']= $arrGetUSSDRoutingRes[0]['DESTINATION_ADDRESS'];
			//echo $varResult;exit;	
		}else{
			$arrResult['errcode']='';
			//echo $varResult;exit;	
		}
		echo json_encode($arrResult);exit;	
	}
	
	public function updateBundleUSSDRouting(){
		//echo '<pre>';print_r($_REQUEST);exit;
		$varServiceTypeText = base64_decode(trim($_REQUEST['serviceType']));
		$varUSSDPrefix = trim($_REQUEST['ussdPrefix']);
		$varBundleId = trim($_REQUEST['bundleId']);			

		$params = array('sitecode'=>$_SESSION['SiteCode'],'servicetype'=>$varServiceTypeText,'ussd_prefix'=>$varUSSDPrefix,'destination_address'=>$varBundleId,'process_flag'=>'2','ip_address'=>getClientIp());
		$arrUpdateUSSDRouting = ApiPostHeader($this->config->item('InsertUpdateUSSDRouting'), $params);
		//echo '<pre>';print_r($params);print_r($arrUpdateUSSDRouting);echo $this->config->item('InsertUpdateUSSDRouting');exit;
		
		if($arrUpdateUSSDRouting[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrUpdateUSSDRouting[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrUpdateUSSDRouting[0]['errmsg']);
		}			
		redirect('bundlerouting');
	}
	
	public function deleteBundleUSSDRouting(){
		//echo '<pre>';print_r($_REQUEST);exit;
		$varUSSDPrefix = trim($_REQUEST['ussdPrefix']);			
		$params = array('sitecode'=>$_SESSION['SiteCode'],'ussd_prefix'=>$varUSSDPrefix,'ip_address'=>getClientIp());
		$arrDeleteUSSDRouting = ApiPostHeader($this->config->item('DeleteUSSDRouting'), $params);
		//echo '<pre>';print_r($params);print_r($arrDeleteUSSDRouting);echo $this->config->item('DeleteUSSDRouting');exit;		
		if($arrDeleteUSSDRouting[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrDeleteUSSDRouting[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrDeleteUSSDRouting[0]['errmsg']);
		}			
		redirect('bundlerouting');
	}
}
?>