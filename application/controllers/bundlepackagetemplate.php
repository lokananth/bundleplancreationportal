<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bundlepackagetemplate extends CI_Controller {	
	function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {
				session_start();
		}		
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlanPackage/packageTemplate/package_template_search_view');
		$this->load->view('footer_view');
	}
	
	public function getAllPackagesTemplateInfo(){
		ini_set("memory_limit", "-1");
		ini_set('max_execution_time', 900);										
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'packageid'=>'-1');
		$arrGetPackagesTemplateInfoRes = ApiPostHeader($this->config->item('GetPackagesTemplateInfo'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetPackagesTemplateInfoRes);echo $this->config->item('GetPackagesTemplateInfo');exit;		
		
		$varResult='';
		if((isset($arrGetPackagesTemplateInfoRes[0]['errcode']) && $arrGetPackagesTemplateInfoRes[0]['errcode']=='0') || isset($arrGetPackagesTemplateInfoRes['errcode']) && $arrGetPackagesTemplateInfoRes['errcode']=='0'){	
			$varResult .= '<script type="text/javascript">
				$("#datatable_tabletools").DataTable();	
				</script>';
				
			$varResult .= '<table id="datatable_tabletools" class="table table-bordered table-striped nowrap dt-responsive" width="100%">
				<thead>
					<tr>
						<th data-class="expand">PackageId</th>
						<th data-hide="phone">PackageType</th>
						<th data-hide="phone">CustCode</th>
						<th data-hide="phone">RenewalMode</th>
						<th data-hide="phone,tablet">Tariff Class</th>
						<th data-hide="phone,tablet">ExpiryDateMode</th>											
					</tr>
				</thead>
				<tbody>';
			foreach($arrGetPackagesTemplateInfoRes as $arrResult){														  
				 $varResult .= "<tr><td>".$arrResult['packageid']."</td>
								<td>".$arrResult['packagetype']."</td>
								<td>".$arrResult['custcode']."</td>
								<td>".$arrResult['renewal_mode']."</td>
								<td>".$arrResult['trffclass']."</td>
								<td>".$arrResult['expdate_mode']."</td></tr>";								
			}				
			$varResult .=	'</tbody>
								</table>';		
		}else{
			$varResult = '';			
		}
		echo $varResult;exit;
	}
	
	public function searchPackageTemplatePlan(){		
		//echo '<pre>';print_r($_REQUEST);exit;
		if($_POST){
			$varPackageId = trim($_REQUEST['packageId']);			
			$params = array('sitecode'=>$_SESSION['SiteCode'],'packageid'=>$varPackageId);
			$arrGetPackageTemplateInfoRes = ApiPostHeader($this->config->item('GetPackagesTemplateInfo'), $params);
			//echo '<pre>';print_r($params);print_r($arrGetPackageTemplateInfoRes);echo $this->config->item('GetPackagesTemplateInfo');exit;				
		}		
		if((isset($arrGetPackageTemplateInfoRes[0]['errcode']) && $arrGetPackageTemplateInfoRes[0]['errcode']=='0') || isset($arrGetPackageTemplateInfoRes['errcode']) && $arrGetPackageTemplateInfoRes['errcode']=='0'){						
					
			$data['arrGetPackageTemplateInfo'] = $arrGetPackageTemplateInfoRes;
			$data['varInsertPackageId'] = $varPackageId;
			
			//Package Type Id (Drop Down)
			$arrPackageTypeIdRes = array();
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'1');
			$arrPackageTypeIdRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
			//echo '<pre>';print_r($arrPackageTypeIdRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
			$data['arrPackageTypeId'] = $arrPackageTypeIdRes;	

			//Pack Connection Charge (Drop Down)		
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'7');
			$arrPackConnectionChargeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
			//echo '<pre>';print_r($arrPackConnectionChargeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
			$data['arrPackConnectionCharge'] = $arrPackConnectionChargeRes;	
				
			//Expired Date Mode (Drop Down)		
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'25');
			$arrExpiredDateModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
			//echo '<pre>';print_r($arrExpiredDateModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
			$data['arrExpiredDateMode'] = $arrExpiredDateModeRes;
			
			//Expired Date Param (Drop Down)		
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'30');
			$arrExpiredDateParamRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
			//echo '<pre>';print_r($arrExpiredDateParamRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
			$data['arrExpiredDateParam'] = $arrExpiredDateParamRes;
			
			//Usage Mode (Drop Down)		
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'26');
			$arrUsageModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
			//echo '<pre>';print_r($arrUsageModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
			$data['arrUsageMode'] = $arrUsageModeRes;
			
			//Pack Free Flag (Drop Down)		
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'8');
			$arrPackFreeFlagRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
			//echo '<pre>';print_r($arrPackFreeFlagRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
			$data['arrPackFreeFlag'] = $arrPackFreeFlagRes;		
			
			//Renewal Mode (Drop Down)		
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'27');
			$arrRenewalModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
			//echo '<pre>';print_r($arrRenewalModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
			$data['arrRenewalMode'] = $arrRenewalModeRes;
			
			//Promo Start Date Mode (Drop Down)		
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'28');
			$arrPromoStartDateModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
			//echo '<pre>';print_r($arrPromoStartDateModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
			$data['arrPromoStartDateMode'] = $arrPromoStartDateModeRes;
			
			//Promo Start Date Param (Drop Down)		
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'31');
			$arrPromoStartDateParamRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
			//echo '<pre>';print_r($arrPromoStartDateParamRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
			$data['arrPromoStartDateParam'] = $arrPromoStartDateParamRes;
			
			$this->load->view('top_header_view');
			$this->load->view('left_menu_view');
			$this->load->view('bundlePlanPackage/packageTemplate/edit_package_template_view',$data);
			$this->load->view('footer_view');
		}else{
			$this->session->set_flashdata('errormsg',$arrGetPackageTemplateInfoRes[0]['errmsg']);
			redirect('bundlepackagetemplate');	
		}
	}	
	
	public function editPackageTemplate(){
		//echo '<pre>';print_r($_REQUEST);exit;
		
		//Edit Bundle Plan
		$params = array(			
			'sitecode'=>trim($_SESSION['SiteCode']),
			'packageid'=>trim($_REQUEST['packageId']),
			'packagetype'=>trim($_REQUEST['packageTypeId']),			
			'currcode'=>trim($_REQUEST['currency']),
			'trffclass'=>trim($_REQUEST['tariffClass']),			
			'daysvalid'=>trim($_REQUEST['daysValid']),
			'access_did'=>trim($_REQUEST['accessDid']),
			'comment'=>trim($_REQUEST['comment']),
			'maxaccess'=>trim($_REQUEST['maxAccess']),
			'expireddate'=>trim($_REQUEST['expiredDate']),
			'ispackcnxcharge'=>trim($_REQUEST['packConnection']),
			'expdate_mode'=>trim($_REQUEST['expiredDateMode']),
			'expdate_param'=>trim($_REQUEST['expiredDateParam']),
			'usagemode'=>trim($_REQUEST['usageMode']),
			'packfree_flag'=>trim($_REQUEST['packFreeFlag']),
			'renewal_mode'=>trim($_REQUEST['renewalMode']),
			'renewal_delay'=>trim($_REQUEST['renewalDelay']),
			'call_maxtalktime'=>trim($_REQUEST['maxTalkTime']),
			'check_dest'=>trim($_REQUEST['destinationCheck']),
			'promostartdate_mode'=>trim($_REQUEST['promoStartDateMode']),
			'promostartdate_param'=>trim($_REQUEST['promoStartDateParam']),
			'ip_address'=>getClientIp()	
		);
		
		$arrUpdatePackageTemplate = ApiPostHeader($this->config->item('UpdatePackageTemplate'), $params);
		//echo '<pre>';print_r($params);print_r($arrUpdatePackageTemplate);echo $this->config->item('UpdatePackageTemplate');exit;			
		if($arrUpdatePackageTemplate[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrUpdatePackageTemplate[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrUpdatePackageTemplate[0]['errmsg']);
		}			
		redirect('bundlepackagetemplate');			
	}
	
	public function createNewPackageTemplate($varCondition=''){		
		//Package Type Id (Drop Down)
		$arrPackageTypeIdRes = array();
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'1');
		$arrPackageTypeIdRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPackageTypeIdRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPackageTypeId'] = $arrPackageTypeIdRes;	

		//Pack Connection Charge (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'7');
		$arrPackConnectionChargeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPackConnectionChargeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPackConnectionCharge'] = $arrPackConnectionChargeRes;	
			
		//Expired Date Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'25');
		$arrExpiredDateModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrExpiredDateModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrExpiredDateMode'] = $arrExpiredDateModeRes;
		
		//Expired Date Param (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'30');
		$arrExpiredDateParamRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrExpiredDateParamRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrExpiredDateParam'] = $arrExpiredDateParamRes;
		
		//Usage Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'26');
		$arrUsageModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrUsageModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrUsageMode'] = $arrUsageModeRes;
		
		//Pack Free Flag (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'8');
		$arrPackFreeFlagRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPackFreeFlagRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPackFreeFlag'] = $arrPackFreeFlagRes;		
		
		//Renewal Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'27');
		$arrRenewalModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrRenewalModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrRenewalMode'] = $arrRenewalModeRes;
		
		//Promo Start Date Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'28');
		$arrPromoStartDateModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPromoStartDateModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPromoStartDateMode'] = $arrPromoStartDateModeRes;
		
		//Promo Start Date Param (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'31');
		$arrPromoStartDateParamRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPromoStartDateParamRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPromoStartDateParam'] = $arrPromoStartDateParamRes;
		
		if($varCondition=="0"){			
			$data['varInsertPackageId'] = '';	
		}else{
			$data['varInsertPackageId'] = $varCondition;	
		}		
		
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlanPackage/packageTemplate/create_package_template_view',$data);
		$this->load->view('footer_view');
	}	
	
	public function addPackageTemplate(){
		//echo '<pre>';print_r($_REQUEST);exit;
		
		//Insert Bundle Plan
		$params = array(			
			'sitecode'=>trim($_SESSION['SiteCode']),
			'packageid'=>trim($_REQUEST['packageId']),
			'packagetype'=>trim($_REQUEST['packageTypeId']),			
			'currcode'=>trim($_REQUEST['currency']),
			'trffclass'=>trim($_REQUEST['tariffClass']),			
			'daysvalid'=>trim($_REQUEST['daysValid']),
			'access_did'=>trim($_REQUEST['accessDid']),
			'comment'=>trim($_REQUEST['comment']),
			'maxaccess'=>trim($_REQUEST['maxAccess']),
			'expireddate'=>trim($_REQUEST['expiredDate']),
			'ispackcnxcharge'=>trim($_REQUEST['packConnection']),
			'expdate_mode'=>trim($_REQUEST['expiredDateMode']),
			'expdate_param'=>trim($_REQUEST['expiredDateParam']),
			'usagemode'=>trim($_REQUEST['usageMode']),
			'packfree_flag'=>trim($_REQUEST['packFreeFlag']),
			'renewal_mode'=>trim($_REQUEST['renewalMode']),
			'renewal_delay'=>trim($_REQUEST['renewalDelay']),
			'call_maxtalktime'=>trim($_REQUEST['maxTalkTime']),
			'check_dest'=>trim($_REQUEST['destinationCheck']),
			'promostartdate_mode'=>trim($_REQUEST['promoStartDateMode']),
			'promostartdate_param'=>trim($_REQUEST['promoStartDateParam']),
			'ip_address'=>getClientIp()	
		);
		
		$arrInsertPackageTemplate = ApiPostHeader($this->config->item('InsertPackageTemplate'), $params);
		//echo '<pre>';print_r($params);print_r($arrInsertPackageTemplate);echo $this->config->item('InsertPackageTemplate');exit;			
		if($arrInsertPackageTemplate[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrInsertPackageTemplate[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrInsertPackageTemplate[0]['errmsg']);
		}			
		redirect('bundlepackagetemplate');				
	}	
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */