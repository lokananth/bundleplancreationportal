<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bundleoverridetariffclass extends CI_Controller {	
	function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {
				session_start();
		}		
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{				
		$data = array();
		
		//BundleId
		/*$params = array('sitecode'=>$_SESSION['SiteCode'],'Bundleid'=>'-1');
		$arrGetBundleInfoRes = ApiPostHeader($this->config->item('SearchBundlePlan'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundleInfoRes);echo $this->config->item('SearchBundlePlan');exit;			
		$data['arrGetBundleInfoRes']=$arrGetBundleInfoRes;*/
		
		//Bundle list (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'32');
		$arrGetBundleInfoRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrGetBundleInfoRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrGetBundleInfoRes'] = $arrGetBundleInfoRes;
		
		//PackageId
		/*$params = array('sitecode'=>$_SESSION['SiteCode'],'package_id'=>'-1');
		$arrGetBundlePackagesInfoRes = ApiPostHeader($this->config->item('GetBundlePackages'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundlePackagesInfoRes);echo $this->config->item('GetBundlePackages');exit;			
		$data['arrGetBundlePackagesInfo']=$arrGetBundlePackagesInfoRes;*/

		//PackageId list (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'33');
		$arrGetBundlePackagesInfoRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrGetBundlePackagesInfoRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrGetBundlePackagesInfo'] = $arrGetBundlePackagesInfoRes;	
		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlanOverrideTariffclass/add_bundle_override_tariff_class_view',$data);
		$this->load->view('footer_view');
	}
	
	public function getAllBundleOverRideList(){
		ini_set("memory_limit", "-1");
		ini_set('max_execution_time', 900);										
		
		//Get All Bundle Plan Override TariffClass
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>'-1','packid'=>'-1','acc_tariffclass'=>'','process_flag'=>'4');
		$arrGetAllBundleAccTariffClassRes = ApiPostHeader($this->config->item('GetBundlePlanOverrideTariffClass'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetAllBundleAccTariffClassRes);echo $this->config->item('GetBundlePlanOverrideTariffClass');exit;		
		
		$varResult='';
		if((isset($arrGetAllBundleAccTariffClassRes[0]['errcode']) && $arrGetAllBundleAccTariffClassRes[0]['errcode']=='0') || isset($arrGetAllBundleAccTariffClassRes['errcode']) && $arrGetAllBundleAccTariffClassRes['errcode']=='0'){	
			$varResult .= '<script type="text/javascript">
				$("#datatable_tabletools").DataTable();	
				</script>';
				
			$varResult .= '<table id="datatable_tabletools" class="table table-bordered table-striped nowrap dt-responsive" width="100%">
				<thead>
					<tr>
						<th data-class="expand">BundleId</th>
						<th data-hide="phone">PackageId</th>
						<th data-hide="phone">Acc TariffClass</th>
						<th data-hide="phone">New TariffClass</th>
						<th data-hide="phone,tablet">Comment</th>
						<th data-hide="phone,tablet">Activation Flag</th>									
					</tr>
				</thead>
				<tbody>';
			foreach($arrGetAllBundleAccTariffClassRes as $arrResult){														  
				 $varResult .= "<tr><td>".$arrResult['bundleid']."</td>
								<td>".$arrResult['packid']."</td>								
								<td>".$arrResult['acc_tariffclass']."</td>								
								<td>".$arrResult['new_tariffclass']."</td>
								<td>".$arrResult['comment']."</td>
								<td>".$arrResult['activation_flag']."</td>
								</tr>";								
			}				
			$varResult .=	'</tbody>
								</table>';		
		}else{
			$varResult = '';			
		}
		echo $varResult;exit;
	}
	
	public function addBundleOverrideTariffClass(){
		//echo '<pre>';print_r($_REQUEST);exit;
		$varBundleId = trim($_REQUEST['bundleId']);
		$varPackageId = trim($_REQUEST['packageId']);
		$varAccTariffClass = trim($_REQUEST['selAccTariffClass']);
		$varNewtariffclass = trim($_REQUEST['newtariffclass']);
		$varComment = trim($_REQUEST['comment']);		
		$varActivationFlag = trim($_REQUEST['activationFlag']);
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>$varBundleId,'packid'=>$varPackageId,'acc_tariffclass'=>$varAccTariffClass,'new_tariffclass'=>$varNewtariffclass,'comment'=>$varComment,'activation_flag'=>$varActivationFlag,'process_flag'=>'1');
		$arrAddBundlePlanOverrideTariffClass = ApiPostHeader($this->config->item('AddBundlePlanOverrideTariffClass'), $params);
		//echo '<pre>';print_r($params);print_r($arrAddBundlePlanOverrideTariffClass);echo $this->config->item('AddBundlePlanOverrideTariffClass');exit;
		
		if($arrAddBundlePlanOverrideTariffClass[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrAddBundlePlanOverrideTariffClass[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrAddBundlePlanOverrideTariffClass[0]['errmsg']);
		}			
		redirect('bundleoverridetariffclass');
	}
	
	public function getBundleAccTariffClassByBundleId()
	{	
		$data = array();
		
		//BundleId
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>'-1','packid'=>'-1','acc_tariffclass'=>'','process_flag'=>'1');
		$arrGetBundleInfoRes = ApiPostHeader($this->config->item('GetBundlePlanOverrideTariffClass'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundleInfoRes);echo $this->config->item('GetBundlePlanOverrideTariffClass');exit;			
		$data['arrGetBundleInfoRes']=$arrGetBundleInfoRes;			
		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');		
		$this->load->view('bundlePlanOverrideTariffclass/delete_bundle_override_tariff_class_view',$data);		
		$this->load->view('footer_view');
	}
	
	public function getBundleAccTariffClass(){
		$varBundleId =  $this->input->post('bundleId');								
		//PackageId
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>$varBundleId);
		$arrGetBundleAccTariffClassRes = ApiPostHeader($this->config->item('GetBundleAccTariffClass'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundleAccTariffClassRes);echo $this->config->item('GetBundleAccTariffClass');exit;
		
		$varResult='';
		if((isset($arrGetBundleAccTariffClassRes[0]['errcode']) && $arrGetBundleAccTariffClassRes[0]['errcode']=='0') || isset($arrGetBundleAccTariffClassRes['errcode']) && $arrGetBundleAccTariffClassRes['errcode']=='0'){
			$varResult .='<option value="">Select</option>';
			foreach($arrGetBundleAccTariffClassRes as $arrResult){ 
				$varResult .= "<option value=".$arrResult['tariffclass'].">".$arrResult['tariffclass']."</option>";
			}
			echo $varResult;exit;	
		}else{
			echo $varResult;exit;	
		}		
	}
	
	public function getPackageIdOverrideTariffClass(){
		$varBundleId =  $this->input->post('bundleId');						
		
		//PackageId
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>$varBundleId,'packid'=>'-1','acc_tariffclass'=>'','process_flag'=>'2');
		$arrGetPackageInfoRes = ApiPostHeader($this->config->item('GetBundlePlanOverrideTariffClass'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetPackageInfoRes);echo $this->config->item('GetBundlePlanOverrideTariffClass');exit;			
		
		$varResult='';
		if((isset($arrGetPackageInfoRes[0]['errcode']) && $arrGetPackageInfoRes[0]['errcode']=='0') || isset($arrGetPackageInfoRes['errcode']) && $arrGetPackageInfoRes['errcode']=='0'){
			$varResult .='<option value="">Select</option>';
			foreach($arrGetPackageInfoRes as $arrResult){ 
				$varResult .= "<option value=".$arrResult['packid'].">".$arrResult['packid']."</option>";
			}
			echo $varResult;exit;	
		}else{
			echo $varResult;exit;	
		}		
	}
	
	public function getBundlePackageAccTariffClass(){
		$varBundleId =  $this->input->post('bundleId');						
		$varPackageId =  $this->input->post('packageId');
		//PackageId
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>$varBundleId,'packid'=>$varPackageId,'acc_tariffclass'=>'','process_flag'=>'3');
		$arrGetBundleAccTariffClassRes = ApiPostHeader($this->config->item('GetBundlePlanOverrideTariffClass'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundleAccTariffClassRes);echo $this->config->item('GetBundlePlanOverrideTariffClass');exit;
		
		$varResult='';
		if((isset($arrGetBundleAccTariffClassRes[0]['errcode']) && $arrGetBundleAccTariffClassRes[0]['errcode']=='0') || isset($arrGetBundleAccTariffClassRes['errcode']) && $arrGetBundleAccTariffClassRes['errcode']=='0'){
			$varResult .='<option value="">Select</option>';
			foreach($arrGetBundleAccTariffClassRes as $arrResult){ 
				$varResult .= "<option value=".$arrResult['acc_tariffclass'].">".$arrResult['acc_tariffclass']."</option>";
			}
			echo $varResult;exit;	
		}else{
			echo $varResult;exit;	
		}		
	}
	
	public function deleteBundleOverrideTariffClass(){
		//echo '<pre>';print_r($_REQUEST);exit;
		$varBundleId = trim($_REQUEST['bundleId']);
		$varPackageId = trim($_REQUEST['packageId']);
		$varAccTariffClass = trim($_REQUEST['selAccTariffClass']);		
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>$varBundleId,'packid'=>$varPackageId,'acc_tariffclass'=>$varAccTariffClass);
		$arrDeleteBundlePlanOverrideTariffClass = ApiPostHeader($this->config->item('DeleteBundleAccTariffClass'), $params);
		//echo '<pre>';print_r($params);print_r($arrDeleteBundlePlanOverrideTariffClass);echo $this->config->item('DeleteBundleAccTariffClass');exit;
		
		if($arrDeleteBundlePlanOverrideTariffClass[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrDeleteBundlePlanOverrideTariffClass[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrDeleteBundlePlanOverrideTariffClass[0]['errmsg']);
		}			
		redirect('bundleoverridetariffclass');
	}
}
?>