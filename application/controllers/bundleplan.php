<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bundleplan extends CI_Controller {	
	function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {
				session_start();
		}		
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');	
		$this->load->view('bundlePlan/bundle_plan_search_view');
		$this->load->view('footer_view');
	}
	
	public function getAllBundleListInfo(){
		ini_set("memory_limit", "-1");
		ini_set('max_execution_time', 900);										
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Bundleid'=>'-1');
		$arrGetBundleInfoRes = ApiPostHeader($this->config->item('SearchBundlePlan'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundleInfoRes);echo $this->config->item('SearchBundlePlan');exit;	
		
		$varResult='';
		if((isset($arrGetBundleInfoRes[0]['errcode']) && $arrGetBundleInfoRes[0]['errcode']=='0') || isset($arrGetBundleInfoRes['errcode']) && $arrGetBundleInfoRes['errcode']=='0'){	
			$varResult .= '<script type="text/javascript">
				$("#datatable_tabletools").DataTable();	
				</script>';
				
			$varResult .= '<table id="datatable_tabletools" class="table table-bordered table-striped nowrap dt-responsive" width="100%">
				<thead>
					<tr>
						<th data-class="expand">BundleId</th>
						<th data-hide="phone">Bundle Name</th>
						<th data-hide="phone">Price</th>
						<th data-hide="phone">Currency</th>
						<th data-hide="phone,tablet">Site code</th>
						<th data-hide="phone,tablet">Description</th>											
					</tr>
				</thead>
				<tbody>';
			foreach($arrGetBundleInfoRes as $arrResult){														  
				 $varResult .= "<tr><td>".$arrResult['Bundleid']."</td>
								<td>".$arrResult['name']."</td>
								<td>".number_format((float) $arrResult['price'], 2, '.', '')."</td>
								<td>".$arrResult['currency']."</td>
								<td>".$arrResult['sitecode']."</td>
								<td>".$arrResult['description']."</td></tr>";								
			}				
			$varResult .=	'</tbody>
								</table>';		
		}else{
			$varResult = '';			
		}
		echo $varResult;exit;
	}
	
	public function searchBundlePlan(){		
		//echo '<pre>';print_r($_REQUEST);exit;
		if($_POST){
			$varBundleId = trim($_REQUEST['bundleId']);	
			$_SESSION['BundleId'] = $varBundleId;
			unset($_SESSION['EditBundleStep1']);	
			unset($_SESSION['EditBundleStep2']);
			
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Bundleid'=>$varBundleId);
			$arrGetBundleInfoRes = ApiPostHeader($this->config->item('SearchBundlePlan'), $params);
			//echo '<pre>';print_r($params);print_r($arrGetBundleInfoRes);echo $this->config->item('SearchBundlePlan');exit;				
		}else{
			$varBundleId = $_SESSION['BundleId'];	
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Bundleid'=>$varBundleId);
			$arrGetBundleInfoRes = ApiPostHeader($this->config->item('SearchBundlePlan'), $params);
			//echo '<pre>';print_r($params);print_r($arrGetBundleInfoRes);echo $this->config->item('SearchBundlePlan');exit;	
		}
		
		//$arrGetBundleInfoRes[0]['errcode']='-1';
		//$arrGetBundleInfoRes[0]['errmsg']='Kindly check the given value';
		
		if((isset($arrGetBundleInfoRes[0]['errcode']) && $arrGetBundleInfoRes[0]['errcode']=='0') || isset($arrGetBundleInfoRes['errcode']) && $arrGetBundleInfoRes['errcode']=='0'){						
					
			$data['arrGetBundleInfoRes'] = $arrGetBundleInfoRes;
			$data['varBundleId'] = $varBundleId;
			
			//Paymode Default (Drop Down)
			$arrPaymentModeDefaultRes = array();
			$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'1');
			$arrPaymentModeDefaultRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
			//echo '<pre>';print_r($arrPaymentModeDefaultRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
			$data['arrPaymentModeDefault'] = $arrPaymentModeDefaultRes;				
				
			//date mode (Drop Down)		
			$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'4');
			$arrDateModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
			//echo '<pre>';print_r($arrDateModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
			$data['arrDateMode'] = $arrDateModeRes;
			
			//Tariff Class Group ID  (Drop Down)
			$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'16');
			$arrTariffClassGroupIdRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
			//echo '<pre>';print_r($params);print_r($arrTariffClassGroupIdRes);echo $this->config->item('CreateBundleFormValue');exit;		
			$data['arrTariffClassGroupId'] = $arrTariffClassGroupIdRes;
			
			/*$arrTariffClassGroupIdRes = array();
			$params = array('sitecode'=>$_SESSION['SiteCode'],'group_id'=>'0');
			$arrTariffClassGroupIdRes = ApiPostHeader($this->config->item('GetTariffClassGroupId'), $params);
			//echo '<pre>';print_r($arrTariffClassGroupIdRes);print_r($_SESSION);echo $this->config->item('GetTariffClassGroupId');exit;		
			$data['arrTariffClassGroupId'] = $arrTariffClassGroupIdRes;	*/
			
			//renew date mode (Drop Down)		
			$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'7');
			$arrRenewDateModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
			//echo '<pre>';print_r($arrRenewDateModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
			$data['arrRenewDateMode'] = $arrRenewDateModeRes;
			
			//renewal_mode (Drop Down)
			$arrRenewalModeRes = array();
			$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'14');
			$arrRenewalModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
			//echo '<pre>';print_r($arrRenewalModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
			$data['arrRenewalMode'] = $arrRenewalModeRes;
			
			$this->load->view('top_header_view');
			$this->load->view('left_menu_view');
			$this->load->view('bundlePlan/edit_bundle_view',$data);
			$this->load->view('footer_view');
		}else{
			$this->session->set_flashdata('errormsg',$arrGetBundleInfoRes[0]['errmsg']);
			redirect('bundleplan');	
		}
	}	
	
	public function editBundleStep2(){		
		//echo '<pre>';print_r($_REQUEST);exit;		
		if($_POST){
			$arrEditBundleStep1 = array('bundleId'=>trim($_REQUEST['bundleId']),'siteCode'=>trim($_REQUEST['siteCode']),'bundleName'=>trim($_REQUEST['bundleName']),'bundlePrice'=>trim($_REQUEST['bundlePrice']),'currency'=>trim($_REQUEST['currency']),'bundleStatus'=>trim($_REQUEST['bundleStatus']),'defaultFlag'=>trim($_REQUEST['defaultFlag']),'paymodeDefault'=>trim($_REQUEST['paymodeDefault']),'renewalDelay'=>trim($_REQUEST['renewalDelay']),'bundleDesc'=>trim($_REQUEST['bundleDesc']),'maxDueDelay'=>trim($_REQUEST['maxDueDelay']),'paymentDelay'=>trim($_REQUEST['paymentDelay']),'dateMode'=>trim($_REQUEST['dateMode']),'tariffClassGroupId'=>trim($_REQUEST['tariffClassGroupId']),'renewDateMode'=>trim($_REQUEST['renewDateMode']),'renewalMode'=>trim($_REQUEST['renewalMode']));
			
			$varBundleId = trim($_REQUEST['bundleId']);	
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Bundleid'=>$varBundleId);
			$arrGetBundleInfoRes = ApiPostHeader($this->config->item('SearchBundlePlan'), $params);
			//echo '<pre>';print_r($params);print_r($arrGetBundleInfoRes);echo $this->config->item('SearchBundlePlan');exit;			
			$_SESSION['EditBundleStep1'] = $arrEditBundleStep1;
		}else{			
			$varBundleId = $_SESSION['EditBundleStep1']['bundleId'];	
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Bundleid'=>$varBundleId);
			$arrGetBundleInfoRes = ApiPostHeader($this->config->item('SearchBundlePlan'), $params);
			//echo '<pre>';print_r($params);print_r($arrGetBundleInfoRes);echo $this->config->item('SearchBundlePlan');exit;			
		}			
		$data = array();
		$data['arrGetBundleInfoRes'] = $arrGetBundleInfoRes;
		$data['varBundleId'] = $varBundleId;
		
		//send message Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'2');
		$arrSendMessageModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrSendMessageModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrSendMessageMode'] = $arrSendMessageModeRes;	
		
		//payment flag (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'3');
		$arrPaymentFlagRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrPaymentFlagRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrPaymentFlag'] = $arrPaymentFlagRes;		
		
		//topup flag (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'5');
		$arrTopUpFlagRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrTopUpFlagRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrTopUpFlag'] = $arrTopUpFlagRes;
		
		//service flag (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'6');
		$arrServiceFlagRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrServiceFlagRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrServiceFlag'] = $arrServiceFlagRes;
		
		//Main Bundle Group ID & Family Group ID(Drop Down)
		/*$arrMainBundleGroupIdRes = array();
		$params = array('sitecode'=>$_SESSION['SiteCode'],'group_id'=>'0');
		$arrMainBundleGroupIdRes = ApiPostHeader($this->config->item('GetBundleGroupId'), $params);
		//echo '<pre>';print_r($arrMainBundleGroupIdRes);print_r($_SESSION);echo $this->config->item('GetBundleGroupId');exit;		
		$data['arrMainBundleGroupId'] = $arrMainBundleGroupIdRes;*/	
		
		//Main Bundle Group ID & Family Group ID(Drop Down)
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'15');
		$arrMainBundleGroupIdRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($params);print_r($arrMainBundleGroupIdRes);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrMainBundleGroupId'] = $arrMainBundleGroupIdRes;			
		
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlan/edit_bundle_step2_view',$data);
		$this->load->view('footer_view');
		
	}
	
	public function editBundleStep3(){
		//echo '<pre>';print_r($_REQUEST);exit;	
		//'smsSender'=>trim($_REQUEST['smsSender']),'smsSenderType'=>trim($_REQUEST['smsSenderType']),'smsAccountId'=>trim($_REQUEST['smsAccountId']),'smsURL'=>trim($_REQUEST['smsURL']),'ussdURL'=>trim($_REQUEST['ussdURL']),'tariffClassGroupId'=>trim($_REQUEST['tariffClassGroupId']),'renewDateMode'=>trim($_REQUEST['renewDateMode']),'bundleSource'=>trim($_REQUEST['bundleSource'])
		
		if($_POST){
			$arrEditBundleStep2 = array('sendingMode'=>trim($_REQUEST['sendingMode']),'paymentFlag'=>trim($_REQUEST['paymentFlag']),'topUpFlag'=>trim($_REQUEST['topUpFlag']),'serviceFlag'=>trim($_REQUEST['serviceFlag']),'mainBGroupId'=>trim($_REQUEST['mainBGroupId']),'familyGroupId'=>trim($_REQUEST['familyGroupId']),'autoStopMode'=>trim($_REQUEST['autoStopMode']),'autoStopDelay'=>trim($_REQUEST['autoStopDelay']),'resetMode'=>trim($_REQUEST['resetMode']),'incentiveFlag'=>trim($_REQUEST['incentiveFlag']),'sendMsgFlag'=>trim($_REQUEST['sendMsgFlag']),'prorataCharging'=>trim($_REQUEST['prorataCharging']),'airTimeFlag'=>trim($_REQUEST['airTimeFlag']),'bundleGroupId'=>trim($_REQUEST['bundleGroupId']),'familyGroupMax'=>trim($_REQUEST['familyGroupMax']),'bundleGroupIdBan'=>trim($_REQUEST['bundleGroupIdBan']));
		
			$_SESSION['EditBundleStep2'] = $arrEditBundleStep2;
		}		
		$data = array();
		$varBundleId = $_SESSION['EditBundleStep1']['bundleId'];	
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Bundleid'=>$varBundleId);
		$arrGetBundleInfoRes = ApiPostHeader($this->config->item('SearchBundlePlan'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundleInfoRes);echo $this->config->item('SearchBundlePlan');exit;
		$data['arrGetBundleInfoRes'] = $arrGetBundleInfoRes;
		$data['varBundleId'] = $varBundleId;
		
		//limit balance mode (Drop Down)
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'10');
		$arrLimitBalanceModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrLimitBalanceModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrLimitBalanceMode'] = $arrLimitBalanceModeRes;	
		
		//limit balance startdate mode (Drop Down)
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'11');
		$arrLimitBalanceStartDateModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrLimitBalanceStartDateModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrLimitBalanceStartDateMode'] = $arrLimitBalanceStartDateModeRes;	
		
		//disc startdate mode (Drop Down)
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'12');
		$arrDiscStartDateModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrDiscStartDateModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrDiscStartDateMode'] = $arrDiscStartDateModeRes;	
		
		//disc delay mode (Drop Down)
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'13');
		$arrDiscDelayModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrDiscDelayModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrDiscDelayMode'] = $arrDiscDelayModeRes;	
		
		//Package groupid disable (Drop Down)
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'17');
		$arrPackageGroupIdRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($params);print_r($arrPackageGroupIdRes);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrPackageGroupId'] = $arrPackageGroupIdRes;
		
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlan/edit_bundle_step3_view',$data);
		$this->load->view('footer_view');		
	}
	
	public function editBundleSteps(){
		//echo '<pre>';print_r($_SESSION['EditBundleStep1']);print_r($_SESSION['EditBundleStep2']);print_r($_REQUEST);exit;
		
		//Insert Bundle Plan
		$params = array(
			'Bundleid'=>$_SESSION['BundleId'],
			'sitecode'=>$_SESSION['SiteCode'],
			'name'=>$_SESSION['EditBundleStep1']['bundleName'],
			'price'=>$_SESSION['EditBundleStep1']['bundlePrice'],
			'currency'=>$_SESSION['EditBundleStep1']['currency'],
			'status'=>$_SESSION['EditBundleStep1']['bundleStatus'],
			'default_flag'=>$_SESSION['EditBundleStep1']['defaultFlag'],
			'paymode_default'=>$_SESSION['EditBundleStep1']['paymodeDefault'],
			'renewal_delay'=>$_SESSION['EditBundleStep1']['renewalDelay'],
			'description'=>$_SESSION['EditBundleStep1']['bundleDesc'],
			'maxduedelay'=>$_SESSION['EditBundleStep1']['maxDueDelay'],
			'payment_delay'=>$_SESSION['EditBundleStep1']['paymentDelay'],
			'datemode'=>$_SESSION['EditBundleStep1']['dateMode'],
			'tariffclass_groupid'=>$_SESSION['EditBundleStep1']['tariffClassGroupId'],
			'renew_datemode'=>$_SESSION['EditBundleStep1']['renewDateMode'],
			'renewal_mode'=>$_SESSION['EditBundleStep1']['renewalMode'],
			'lastupd'=>'',
			'updby'=>'BPPortal',
			'sendmsg_mode'=>$_SESSION['EditBundleStep2']['sendingMode'],			
			'payment_flag'=>$_SESSION['EditBundleStep2']['paymentFlag'],			
			'topup_flag'=>$_SESSION['EditBundleStep2']['topUpFlag'],
			'service_flag'=>$_SESSION['EditBundleStep2']['serviceFlag'],
			'mainbundle_groupid'=>$_SESSION['EditBundleStep2']['mainBGroupId'],
			'family_groupid'=>$_SESSION['EditBundleStep2']['familyGroupId'],
			'autostop_mode'=>$_SESSION['EditBundleStep2']['autoStopMode'],
			'autostop_delay'=>$_SESSION['EditBundleStep2']['autoStopDelay'],
			'reset_mode'=>$_SESSION['EditBundleStep2']['resetMode'],
			'incentive_flag'=>$_SESSION['EditBundleStep2']['incentiveFlag'],
			'sendmsg_flag'=>$_SESSION['EditBundleStep2']['sendMsgFlag'],
			'prorata_charging'=>$_SESSION['EditBundleStep2']['prorataCharging'],
			'airtime_flag'=>$_SESSION['EditBundleStep2']['airTimeFlag'],						
			'bundle_groupid'=>$_SESSION['EditBundleStep2']['bundleGroupId'],
			'family_group_maxctr'=>$_SESSION['EditBundleStep2']['familyGroupMax'],
			'bundle_groupid_ban'=>$_SESSION['EditBundleStep2']['bundleGroupIdBan'],			
			
			'limit_balance_mode'=>trim($_REQUEST['limitBalanceMode']),
			'limit_balance_amount'=>trim($_REQUEST['limitBalanceAmount']),
			'limit_balance_startdate_mode'=>trim($_REQUEST['limitBalanceStartDateMode']),
			'limit_balance_startdate'=>trim($_REQUEST['limitBalanceStartDate']),
			'disc_price'=>trim($_REQUEST['discPrice']),
			'disc_startdate_mode'=>trim($_REQUEST['discStartDateMode']),
			'disc_startdate'=>trim($_REQUEST['discStartDate']),
			'disc_delay_mode'=>trim($_REQUEST['discDelayMode']),
			'disc_delay'=>trim($_REQUEST['discDelay']),
			'promoid'=>trim($_REQUEST['promoId']),
			'saver_id'=>trim($_REQUEST['saverId']),
			'firstupdate_fr'=>trim($_REQUEST['firstUpdateFrom']),
			'firstupdate_to'=>trim($_REQUEST['firstUpdateTo']),
			'saver_ban'=>trim($_REQUEST['saverBan']),			
			'package_groupid_disable'=>trim($_REQUEST['packageGroupId']),
			'ip_address'=>getClientIp()
		);
		
		$arrEditBundlePlan = ApiPostHeader($this->config->item('UpdateBundlePlan'), $params);
		//echo '<pre>';print_r($params);print_r($arrEditBundlePlan);echo $this->config->item('UpdateBundlePlan');exit;
		
		if($arrEditBundlePlan[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrEditBundlePlan[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrEditBundlePlan[0]['errmsg']);
		}			
		redirect('bundleplan');				
	}
	
	public function createNewBundle($varCondition=''){		
		//Paymode Default (Drop Down)
		$arrPaymentModeDefaultRes = array();
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'1');
		$arrPaymentModeDefaultRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrPaymentModeDefaultRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrPaymentModeDefault'] = $arrPaymentModeDefaultRes;				
			
		//date mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'4');
		$arrDateModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrDateModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrDateMode'] = $arrDateModeRes;
		
		//Tariff Class Group ID  (Drop Down)
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'16');
		$arrTariffClassGroupIdRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($params);print_r($arrTariffClassGroupIdRes);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrTariffClassGroupId'] = $arrTariffClassGroupIdRes;
		
		/*$arrTariffClassGroupIdRes = array();
		$params = array('sitecode'=>$_SESSION['SiteCode'],'group_id'=>'0');
		$arrTariffClassGroupIdRes = ApiPostHeader($this->config->item('GetTariffClassGroupId'), $params);
		//echo '<pre>';print_r($arrTariffClassGroupIdRes);print_r($_SESSION);echo $this->config->item('GetTariffClassGroupId');exit;		
		$data['arrTariffClassGroupId'] = $arrTariffClassGroupIdRes;	*/
		
		//renew date mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'7');
		$arrRenewDateModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrRenewDateModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrRenewDateMode'] = $arrRenewDateModeRes;	
		
		//renewal_mode (Drop Down)
		$arrRenewalModeRes = array();
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'14');
		$arrRenewalModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrRenewalModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrRenewalMode'] = $arrRenewalModeRes;
		
		if($varCondition=="0"){
			unset($_SESSION['BundleStep1']);	
			unset($_SESSION['BundleStep2']);
		}
		if($varCondition=="1"){
			//unset($_SESSION['BundleStep1']);	
		}
		
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlan/create_bundle_view',$data);
		$this->load->view('footer_view');
	}
	
	public function createBundleStep2(){
		//echo '<pre>';print_r($_REQUEST);exit;		
		if($_POST){
			$arrBundleStep1 = array('siteCode'=>trim($_REQUEST['siteCode']),'bundleName'=>trim($_REQUEST['bundleName']),'bundlePrice'=>trim($_REQUEST['bundlePrice']),'currency'=>trim($_REQUEST['currency']),'bundleStatus'=>trim($_REQUEST['bundleStatus']),'defaultFlag'=>trim($_REQUEST['defaultFlag']),'paymodeDefault'=>trim($_REQUEST['paymodeDefault']),'renewalDelay'=>trim($_REQUEST['renewalDelay']),'bundleDesc'=>trim($_REQUEST['bundleDesc']),'maxDueDelay'=>trim($_REQUEST['maxDueDelay']),'paymentDelay'=>trim($_REQUEST['paymentDelay']),'dateMode'=>trim($_REQUEST['dateMode']),'tariffClassGroupId'=>trim($_REQUEST['tariffClassGroupId']),'renewDateMode'=>trim($_REQUEST['renewDateMode']),'renewalMode'=>trim($_REQUEST['renewalMode']));
			
			$_SESSION['BundleStep1'] = $arrBundleStep1;
		}		
		
		//send message Mode (Drop Down)
		$data = array();
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'2');
		$arrSendMessageModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrSendMessageModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrSendMessageMode'] = $arrSendMessageModeRes;	
		
		//payment flag (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'3');
		$arrPaymentFlagRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrPaymentFlagRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrPaymentFlag'] = $arrPaymentFlagRes;		
		
		//topup flag (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'5');
		$arrTopUpFlagRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrTopUpFlagRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrTopUpFlag'] = $arrTopUpFlagRes;
		
		//service flag (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'6');
		$arrServiceFlagRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrServiceFlagRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrServiceFlag'] = $arrServiceFlagRes;
		
		//Main Bundle Group ID & Family Group ID(Drop Down)
		/*$arrMainBundleGroupIdRes = array();
		$params = array('sitecode'=>$_SESSION['SiteCode'],'group_id'=>'0');
		$arrMainBundleGroupIdRes = ApiPostHeader($this->config->item('GetBundleGroupId'), $params);
		//echo '<pre>';print_r($arrMainBundleGroupIdRes);print_r($_SESSION);echo $this->config->item('GetBundleGroupId');exit;		
		$data['arrMainBundleGroupId'] = $arrMainBundleGroupIdRes;*/	
		
		//Main Bundle Group ID & Family Group ID(Drop Down)
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'15');
		$arrMainBundleGroupIdRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($params);print_r($arrMainBundleGroupIdRes);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrMainBundleGroupId'] = $arrMainBundleGroupIdRes;			
		
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlan/bundle_step2_view',$data);
		$this->load->view('footer_view');
		
	}
	
	public function createBundleStep3(){
		//echo '<pre>';print_r($_REQUEST);exit;	
		//'smsSender'=>trim($_REQUEST['smsSender']),'smsSenderType'=>trim($_REQUEST['smsSenderType']),'smsAccountId'=>trim($_REQUEST['smsAccountId']),'smsURL'=>trim($_REQUEST['smsURL']),'ussdURL'=>trim($_REQUEST['ussdURL']),'tariffClassGroupId'=>trim($_REQUEST['tariffClassGroupId']),'renewDateMode'=>trim($_REQUEST['renewDateMode']),'bundleSource'=>trim($_REQUEST['bundleSource'])
		
		if($_POST){
			$arrBundleStep2 = array('sendingMode'=>trim($_REQUEST['sendingMode']),'paymentFlag'=>trim($_REQUEST['paymentFlag']),'topUpFlag'=>trim($_REQUEST['topUpFlag']),'serviceFlag'=>trim($_REQUEST['serviceFlag']),'mainBGroupId'=>trim($_REQUEST['mainBGroupId']),'familyGroupId'=>trim($_REQUEST['familyGroupId']),'autoStopMode'=>trim($_REQUEST['autoStopMode']),'autoStopDelay'=>trim($_REQUEST['autoStopDelay']),'resetMode'=>trim($_REQUEST['resetMode']),'incentiveFlag'=>trim($_REQUEST['incentiveFlag']),'sendMsgFlag'=>trim($_REQUEST['sendMsgFlag']),'prorataCharging'=>trim($_REQUEST['prorataCharging']),'airTimeFlag'=>trim($_REQUEST['airTimeFlag']),'bundleGroupId'=>trim($_REQUEST['bundleGroupId']),'familyGroupMax'=>trim($_REQUEST['familyGroupMax']),'bundleGroupIdBan'=>trim($_REQUEST['bundleGroupIdBan']));
		
			$_SESSION['BundleStep2'] = $arrBundleStep2;
		}		
		
		$data = array();
		//limit balance mode (Drop Down)
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'10');
		$arrLimitBalanceModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrLimitBalanceModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrLimitBalanceMode'] = $arrLimitBalanceModeRes;	
		
		//limit balance startdate mode (Drop Down)
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'11');
		$arrLimitBalanceStartDateModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrLimitBalanceStartDateModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrLimitBalanceStartDateMode'] = $arrLimitBalanceStartDateModeRes;	
		
		//disc startdate mode (Drop Down)
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'12');
		$arrDiscStartDateModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrDiscStartDateModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrDiscStartDateMode'] = $arrDiscStartDateModeRes;	
		
		//disc delay mode (Drop Down)
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'13');
		$arrDiscDelayModeRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($arrDiscDelayModeRes);print_r($_SESSION);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrDiscDelayMode'] = $arrDiscDelayModeRes;	
		
		//Package groupid disable (Drop Down)
		$params = array('sitecode'=>$_SESSION['SiteCode'],'action'=>'17');
		$arrPackageGroupIdRes = ApiPostHeader($this->config->item('CreateBundleFormValue'), $params);
		//echo '<pre>';print_r($params);print_r($arrPackageGroupIdRes);echo $this->config->item('CreateBundleFormValue');exit;		
		$data['arrPackageGroupId'] = $arrPackageGroupIdRes;
		
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlan/bundle_step3_view',$data);
		$this->load->view('footer_view');		
	}
	
	public function addBundleSteps(){
		//echo '<pre>';print_r($_SESSION['BundleStep1']);print_r($_SESSION['BundleStep2']);print_r($_REQUEST);exit;
		
		//Insert Bundle Plan
		$params = array(			
			'sitecode'=>$_SESSION['SiteCode'],
			'name'=>$_SESSION['BundleStep1']['bundleName'],
			'price'=>$_SESSION['BundleStep1']['bundlePrice'],
			'currency'=>$_SESSION['BundleStep1']['currency'],
			'status'=>$_SESSION['BundleStep1']['bundleStatus'],
			'default_flag'=>$_SESSION['BundleStep1']['defaultFlag'],
			'paymode_default'=>$_SESSION['BundleStep1']['paymodeDefault'],
			'renewal_delay'=>$_SESSION['BundleStep1']['renewalDelay'],
			'description'=>$_SESSION['BundleStep1']['bundleDesc'],
			'maxduedelay'=>$_SESSION['BundleStep1']['maxDueDelay'],
			'payment_delay'=>$_SESSION['BundleStep1']['paymentDelay'],
			'datemode'=>$_SESSION['BundleStep1']['dateMode'],
			'tariffclass_groupid'=>$_SESSION['BundleStep1']['tariffClassGroupId'],
			'renew_datemode'=>$_SESSION['BundleStep1']['renewDateMode'],
			'renewal_mode'=>$_SESSION['BundleStep1']['renewalMode'],
			'lastupd'=>'',
			'updby'=>'BPPortal',
			'sendmsg_mode'=>$_SESSION['BundleStep2']['sendingMode'],			
			'payment_flag'=>$_SESSION['BundleStep2']['paymentFlag'],			
			'topup_flag'=>$_SESSION['BundleStep2']['topUpFlag'],
			'service_flag'=>$_SESSION['BundleStep2']['serviceFlag'],
			'mainbundle_groupid'=>$_SESSION['BundleStep2']['mainBGroupId'],
			'family_groupid'=>$_SESSION['BundleStep2']['familyGroupId'],
			'autostop_mode'=>$_SESSION['BundleStep2']['autoStopMode'],
			'autostop_delay'=>$_SESSION['BundleStep2']['autoStopDelay'],
			'reset_mode'=>$_SESSION['BundleStep2']['resetMode'],
			'incentive_flag'=>$_SESSION['BundleStep2']['incentiveFlag'],
			'sendmsg_flag'=>$_SESSION['BundleStep2']['sendMsgFlag'],
			'prorata_charging'=>$_SESSION['BundleStep2']['prorataCharging'],
			'airtime_flag'=>$_SESSION['BundleStep2']['airTimeFlag'],						
			'bundle_groupid'=>$_SESSION['BundleStep2']['bundleGroupId'],
			'family_group_maxctr'=>$_SESSION['BundleStep2']['familyGroupMax'],
			'bundle_groupid_ban'=>$_SESSION['BundleStep2']['bundleGroupIdBan'],			
			
			'limit_balance_mode'=>trim($_REQUEST['limitBalanceMode']),
			'limit_balance_amount'=>trim($_REQUEST['limitBalanceAmount']),
			'limit_balance_startdate_mode'=>trim($_REQUEST['limitBalanceStartDateMode']),
			'limit_balance_startdate'=>trim($_REQUEST['limitBalanceStartDate']),
			'disc_price'=>trim($_REQUEST['discPrice']),
			'disc_startdate_mode'=>trim($_REQUEST['discStartDateMode']),
			'disc_startdate'=>trim($_REQUEST['discStartDate']),
			'disc_delay_mode'=>trim($_REQUEST['discDelayMode']),
			'disc_delay'=>trim($_REQUEST['discDelay']),
			'promoid'=>trim($_REQUEST['promoId']),
			'saver_id'=>trim($_REQUEST['saverId']),
			'firstupdate_fr'=>trim($_REQUEST['firstUpdateFrom']),
			'firstupdate_to'=>trim($_REQUEST['firstUpdateTo']),
			'saver_ban'=>trim($_REQUEST['saverBan']),			
			'package_groupid_disable'=>trim($_REQUEST['packageGroupId']),
			'ip_address'=>getClientIp()	
		);
		
		$arrInsertBundlePlan = ApiPostHeader($this->config->item('InsertBundlePlan'), $params);
		//echo '<pre>';print_r($params);print_r($arrInsertBundlePlan);echo $this->config->item('InsertBundlePlan');exit;			
		if($arrInsertBundlePlan[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrInsertBundlePlan[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrInsertBundlePlan[0]['errmsg']);
		}			
		redirect('bundleplan');				
	}
	
	public function getTariffGroupInfo()
	{		
		$varTariffGroupId =  $this->input->post('tariffGroupId');		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'group_id'=>$varTariffGroupId);		
		$arrGetTariffGroupIdRes = ApiPostHeader($this->config->item('GetTariffClassGroup'), $params);		
		//echo '<pre>';print_r($params);print_r($arrGetTariffGroupIdRes);echo $this->config->item('GetTariffClassGroup');exit;	
		
		/*$params = array('sitecode'=>$_SESSION['SiteCode'],'group_id'=>$varTariffGroupId);		
		$arrGetTariffGroupIdRes = ApiPostHeader($this->config->item('GetBundleGroup'), $params);
		
		echo '<pre>';print_r($params);print_r($arrGetTariffGroupIdRes);echo $this->config->item('GetBundleGroup');exit;*/
		
		$varResult='';
		if((isset($arrGetTariffGroupIdRes[0]['errcode']) && $arrGetTariffGroupIdRes[0]['errcode']=='0') || isset($arrGetTariffGroupIdRes['errcode']) && $arrGetTariffGroupIdRes['errcode']=='0'){				
			
			$varResult .='<table class="table table-bordered">
					<thead>
						<tr>
							<th>Group Id</th>
							<th>Tariff Class</th>							
						</tr>
					</thead>
					<tbody>';
			foreach($arrGetTariffGroupIdRes as $arrResult){							
				$varResult .= "<tr><td>".$arrResult['groupid']."</td>
					<td>".$arrResult['tariffclass']."</td>
					</tr>";						
			}											
			$varResult .=	'</tbody>
								</table>';								
		}else{
			$varResult .= '<br><br><b><center><span style="color:red;">No records found</span></center></b></br></br>';			
		}
		echo $varResult;exit;
		
	}
	
	public function getBundleGroupInfo(){
		$varBundleGroupId =  $this->input->post('getBundleGroupId');				
		$params = array('sitecode'=>$_SESSION['SiteCode'],'group_id'=>$varBundleGroupId);		
		$arrGetBundleGroupIdRes = ApiPostHeader($this->config->item('GetBundleGroup'), $params);		
		//echo '<pre>';print_r($params);print_r($arrGetBundleGroupIdRes);echo $this->config->item('GetBundleGroup');exit;
		
		$varResult='';
		if((isset($arrGetBundleGroupIdRes[0]['errcode']) && $arrGetBundleGroupIdRes[0]['errcode']=='0') || isset($arrGetBundleGroupIdRes['errcode']) && $arrGetBundleGroupIdRes['errcode']=='0'){				
			
			$varResult .='<table class="table table-bordered">
					<thead>
						<tr>
							<th>Group Id</th>
							<th>Bundle Id</th>	
							<th>Comment</th>	
						</tr>
					</thead>
					<tbody>';
			foreach($arrGetBundleGroupIdRes as $arrResult){							
				$varResult .= "<tr><td>".$arrResult['groupid']."</td>
					<td>".$arrResult['Bundle_id']."</td>
					<td>".$arrResult['comment']."</td>
					</tr>";						
			}											
			$varResult .=	'</tbody>
								</table>';								
		}else{
			$varResult .= '<br><br><b><center><span style="color:red;">No records found</span></center></b></br></br>';			
		}
		echo $varResult;exit;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */