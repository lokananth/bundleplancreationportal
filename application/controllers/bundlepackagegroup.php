<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bundlepackagegroup extends CI_Controller {	
	function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {
				session_start();
		}		
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{		
		$data = array();
		//PackageId list (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'33');
		$arrGetBundlePackagesInfoRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrGetBundlePackagesInfoRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrGetBundlePackagesInfo'] = $arrGetBundlePackagesInfoRes;
		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlanPackage/packageGroup/package_group_view',$data);
		$this->load->view('footer_view');
	}
	
	public function getAllPackageGroupInfoList(){
		ini_set("memory_limit", "-1");
		ini_set('max_execution_time', 900);										
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'group_id'=>'-1');		
		$arrAllGetPackageGroupIdRes = ApiPostHeader($this->config->item('GetPackageByGroupId'), $params);		
		//echo '<pre>';print_r($params);print_r($arrAllGetPackageGroupIdRes);echo $this->config->item('GetPackageByGroupId');exit;				
		
		$varResult='';
		if((isset($arrAllGetPackageGroupIdRes[0]['errcode']) && $arrAllGetPackageGroupIdRes[0]['errcode']=='0') || isset($arrAllGetPackageGroupIdRes['errcode']) && $arrAllGetPackageGroupIdRes['errcode']=='0'){	
			$varResult .= '<script type="text/javascript">
				$("#datatable_tabletools").DataTable();	
				</script>';
				
			$varResult .= '<table id="datatable_tabletools" class="table table-bordered table-striped nowrap dt-responsive" width="100%">
				<thead>
					<tr>
						<th data-class="expand">GroupId</th>
						<th data-hide="phone">PackageId</th>						
						<th data-hide="phone">Comment</th>											
					</tr>
				</thead>
				<tbody>';
			foreach($arrAllGetPackageGroupIdRes as $arrResult){														  
				 $varResult .= "<tr><td>".$arrResult['Groupid']."</td>
								<td>".$arrResult['Package_id']."</td>								
								<td>".$arrResult['Comment']."</td>								
								</tr>";								
			}				
			$varResult .=	'</tbody>
								</table>';		
		}else{
			$varResult = '';			
		}
		echo $varResult;exit;
	}
	
	public function addPackageGroupId(){
		//echo '<pre>';print_r($_REQUEST);exit;		
		$varPackageId = trim($_REQUEST['selPackageId']);
		$varComment = trim($_REQUEST['comment']);
		$params = array('sitecode'=>$_SESSION['SiteCode'],'groupid'=>'0','packageid'=>$varPackageId,'comment'=>$varComment,'process_flag'=>'1');
		$arrAddPackageGroupId = ApiPostHeader($this->config->item('InsertUpdatePackageGroup'), $params);
		//echo '<pre>';print_r($params);print_r($arrAddPackageGroupId);echo $this->config->item('InsertUpdatePackageGroup');exit;
		
		if($arrAddPackageGroupId[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrAddPackageGroupId[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrAddPackageGroupId[0]['errmsg']);
		}			
		redirect('bundlepackagegroup');	
	}
	public function getByPackageGroupId()
	{	
		$data = array();		
		/*$params = array('sitecode'=>$_SESSION['SiteCode'],'package_id'=>'-1');
		$arrGetBundlePackagesInfoRes = ApiPostHeader($this->config->item('GetBundlePackages'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundlePackagesInfoRes);echo $this->config->item('GetBundlePackages');exit;			
		$data['arrGetBundlePackagesInfo']=$arrGetBundlePackagesInfoRes;*/		
		
		//PackageId list (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'33');
		$arrGetBundlePackagesInfoRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrGetBundlePackagesInfoRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrGetBundlePackagesInfo'] = $arrGetBundlePackagesInfoRes;
		
		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlanPackage/packageGroup/edit_package_group_view',$data);
		$this->load->view('footer_view');
	}
	public function getPackageIdByGroupId(){
		$varPackageGroupId =  $this->input->post('getPackageGroupId');				
		$params = array('sitecode'=>$_SESSION['SiteCode'],'group_id'=>$varPackageGroupId);		
		$arrGetPackageGroupIdRes = ApiPostHeader($this->config->item('GetPackageByGroupId'), $params);		
		//echo '<pre>';print_r($params);print_r($arrGetPackageGroupIdRes);echo $this->config->item('GetPackageByGroupId');exit;		
		$varResult='';
		if((isset($arrGetPackageGroupIdRes[0]['errcode']) && $arrGetPackageGroupIdRes[0]['errcode']=='0') || isset($arrGetPackageGroupIdRes['errcode']) && $arrGetPackageGroupIdRes['errcode']=='0'){			
			
			foreach($arrGetPackageGroupIdRes as $arrResult){							
				$varResult .= $arrResult['Package_id'].',';
			}					
			$arrResult['packageId'] = rtrim($varResult,',');								
			$arrResult['Comment'] = $arrGetPackageGroupIdRes[0]['Comment'];
		}else{			
			$arrResult = '';	
		}
		echo json_encode($arrResult);exit;
	}
	
	public function updatePackageGroupId(){
		//echo '<pre>';print_r($_REQUEST);exit;		
		$varPackageGroupId = trim($_REQUEST['getPackageGroupId']);
		$varPackageId = trim($_REQUEST['selPackageId']);
		$varComment = trim($_REQUEST['comment']);
		$params = array('sitecode'=>$_SESSION['SiteCode'],'groupid'=>$varPackageGroupId,'packageid'=>$varPackageId,'comment'=>$varComment,'process_flag'=>'2');
		$arrUpdatePackageGroupId = ApiPostHeader($this->config->item('InsertUpdatePackageGroup'), $params);
		//echo '<pre>';print_r($params);print_r($arrUpdatePackageGroupId);echo $this->config->item('InsertUpdatePackageGroup');exit;
		
		if($arrUpdatePackageGroupId[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrUpdatePackageGroupId[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrUpdatePackageGroupId[0]['errmsg']);
		}			
		redirect('bundlepackagegroup');	
	}
}
?>