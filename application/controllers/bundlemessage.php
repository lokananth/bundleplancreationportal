<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bundlemessage extends CI_Controller {	
	function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {
				session_start();
		}		
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	/*public function index()
	{				
		$data = array();		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Bundleid'=>'-1','process_flag'=>'1');
		$arrGetAllBundleMessageInfoRes = ApiPostHeader($this->config->item('GetBundleMessage'), $params);
		//echo '<pre>';print_r($arrGetAllBundleMessageInfoRes);print_r($_SESSION);echo $this->config->item('GetBundleMessage');exit;		
		$data['arrGetAllBundleMessageInfoRes'] = $arrGetAllBundleMessageInfoRes;
		
		//Bundle list (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'32');
		$arrGetBundleInfoRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrGetBundleInfoRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrGetBundleInfoRes'] = $arrGetBundleInfoRes;
		
		$params = array('sitecode'=>$_SESSION['SiteCode']);
		$arrGetBundleMessageTemplateInfo = ApiPostHeader($this->config->item('GetBundleMessageTemplate'), $params);
		//echo '<pre>';print_r($arrGetBundleMessageTemplateInfo);print_r($_SESSION);echo $this->config->item('GetBundleMessageTemplate');exit;		
		$data['arrGetBundleMessageTemplateInfo'] = $arrGetBundleMessageTemplateInfo;
		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundleMessage/bundle_message_view',$data);
		$this->load->view('footer_view');
	}*/
	
	public function addBundleMessage(){
		//echo '<pre>';print_r($_REQUEST);exit;
		$varBundleId = trim($_REQUEST['bundleId']);		
		$arrBundleMessage = array();		
		$arrMessageId = $_REQUEST['hidMessageId'];
		$arrComment = $_REQUEST['hidComment'];
		$arrMessageText = $_REQUEST['messageText'];
		$i=0;
		$varTempReplace = '';
		foreach($arrMessageId as $arrResult){
			$arrBundleMessage[$i]['MessageId'] = $arrMessageId[$i];
			$arrBundleMessage[$i]['Comment'] = $arrComment[$i];
			//$varTempReplace = str_replace('&',htmlspecialchars("amp;", ENT_QUOTES),trim($arrMessageText[$i]));
			$varTempReplace = str_replace('&',"\&\amp;",trim($arrMessageText[$i]));
			//$arrBundleMessage[$i]['MessageText'] = trim($arrMessageText[$i]); 
			$arrBundleMessage[$i]['MessageText'] = $varTempReplace; 
			$i++;
		}
		//echo '<pre>';print_r($arrBundleMessage);exit;		
		$varResult = '';
		foreach($arrBundleMessage as $arrResult){
				$varResult .='<MessageDetails>
								<messageid>'.$arrResult['MessageId'].'</messageid>
								<comment>'.$arrResult['Comment'].'</comment>
								<messagetext>'.stripslashes($arrResult['MessageText']).'</messagetext>
							</MessageDetails>';
		}
		//echo '<pre>';echo $varResult;exit;		
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>$varBundleId,'messageid'=>'0','comment'=>'testXML','message_text'=>'testXML','insertby'=>'BPPortal','process_flag'=>'1','xmlData'=>$varResult);
		$arrAddBundleMessage = ApiPostHeader($this->config->item('InsertBundleMessage'), $params);
		//echo '<pre>';print_r($params);print_r($arrAddBundleMessage);echo $this->config->item('InsertBundleMessage');exit;
		
		if($arrAddBundleMessage[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrAddBundleMessage[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrAddBundleMessage[0]['errmsg']);
		}			
		redirect('bundlemessage#resultDiv');
	}
	
	public function index()
	{				
		$data = array();		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Bundleid'=>'-1','process_flag'=>'1');
		$arrGetAllBundleMessageInfoRes = ApiPostHeader($this->config->item('GetBundleMessage'), $params);
		//echo '<pre>';print_r($arrGetAllBundleMessageInfoRes);print_r($_SESSION);echo $this->config->item('GetBundleMessage');exit;		
		$data['arrGetAllBundleMessageInfoRes'] = $arrGetAllBundleMessageInfoRes;
		
		//Bundle list (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Bundleid'=>'0','process_flag'=>'2');
		$arrGetBundleInfoRes = ApiPostHeader($this->config->item('GetBundleMessage'), $params);
		//echo '<pre>';print_r($arrGetBundleInfoRes);print_r($_SESSION);echo $this->config->item('GetBundleMessage');exit;		
		$data['arrGetBundleInfoRes'] = $arrGetBundleInfoRes;		
		
		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundleMessage/edit_bundle_message_view',$data);
		$this->load->view('footer_view');
	}
	
	public function getBundleMessageByBundleId(){		
		$varBundleId =  $this->input->post('bundleId');								
		//PackageId
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Bundleid'=>$varBundleId,'process_flag'=>'1');
		$arrGetBundleMessageInfoRes = ApiPostHeader($this->config->item('GetBundleMessage'), $params);
		//echo '<pre>';print_r($arrGetBundleMessageInfoRes);print_r($_SESSION);echo $this->config->item('GetBundleMessage');exit;
		
		$varResult='';
		if((isset($arrGetBundleMessageInfoRes[0]['errcode']) && $arrGetBundleMessageInfoRes[0]['errcode']=='0') || isset($arrGetBundleMessageInfoRes['errcode']) && $arrGetBundleMessageInfoRes['errcode']=='0'){
			
			$varResult .='<table id="datatable_tabletools" class="table table-bordered table-striped nowrap dt-responsive" width="100%">
				<thead>
					<tr>
						<th data-class="expand">BundleId</th>
						<th data-hide="phone">MessageId</th>
						<th data-hide="phone">Comment</th>
						<th data-hide="phone">MessageText</th>																
					</tr>
				</thead>
				<tbody>';
			$i=0;	
			foreach($arrGetBundleMessageInfoRes as $arrResult){ 
				$varResult .= '<input type="hidden" id="hidMessageId'.$i.'" name="hidMessageId[]"  value="'.$arrResult['messageid'].'" />
				<input type="hidden" id="hidComment'.$i.'" name="hidComment[]"  value="'.$arrResult['comment'].'" />';
				
				$varResult .= '<tr>
						<td>'.$arrResult['bundleid'].'</td>
						<td>'.$arrResult['messageid'].'</td>
						<td>'.$arrResult['comment'].'</td>
						<td><textarea disabled class="sms_content" id="messageText'.$i.'" name="messageText[]" maxlength="160">'.$arrResult['message_text'].'</textarea><br><span id="errMessageText'.$i.'" style="color:red;"></span></td>
						<td> <a id="'.$i.'" href="javascript:void(0);" class="btn btn-primary remove_button" style="text-decoration: none;">Edit</a><span id="resultRoller"></span>
						</tr>';
					$i++;	
			}
			$varResult .= '</tbody>
							</table>';
			echo $varResult;exit;	
		}else{
			echo $varResult;exit;	
		}
	}
	
	public function editBundleMessage(){
		//echo '<pre>';print_r($_REQUEST);exit;
		$varBundleId = trim($_REQUEST['bundleId']);		
		$arrBundleMessage = array();		
		$arrMessageId = $_REQUEST['hidMessageId'];
		$arrComment = $_REQUEST['hidComment'];
		$arrMessageText = $_REQUEST['messageText'];
		$i=0;
		$varTempReplace = '';
		foreach($arrMessageId as $arrResult){
			$arrBundleMessage[$i]['MessageId'] = $arrMessageId[$i];
			$arrBundleMessage[$i]['Comment'] = $arrComment[$i];
			//$varTempReplace = str_replace('&',htmlspecialchars("amp;", ENT_QUOTES),trim($arrMessageText[$i]));
			$varTempReplace = str_replace('&',"\&\amp;",trim($arrMessageText[$i]));
			//$arrBundleMessage[$i]['MessageText'] = trim($arrMessageText[$i]); 
			$arrBundleMessage[$i]['MessageText'] = $varTempReplace; 
			$i++;
		}
		//echo '<pre>';print_r($arrBundleMessage);exit;		
		$varResult = '';
		foreach($arrBundleMessage as $arrResult){
				$varResult .='<MessageDetails>
								<messageid>'.$arrResult['MessageId'].'</messageid>
								<comment>'.$arrResult['Comment'].'</comment>
								<messagetext>'.stripslashes($arrResult['MessageText']).'</messagetext>
							</MessageDetails>';
		}
		//echo '<pre>';echo $varResult;exit;		
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>$varBundleId,'messageid'=>'0','comment'=>'testXML','message_text'=>'testXML','insertby'=>'BPPortal','process_flag'=>'2','xmlData'=>$varResult);
		$arrEditBundleMessage = ApiPostHeader($this->config->item('InsertBundleMessage'), $params);
		//echo '<pre>';print_r($params);print_r($arrEditBundleMessage);echo $this->config->item('InsertBundleMessage');exit;
		
		if($arrEditBundleMessage[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrEditBundleMessage[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrEditBundleMessage[0]['errmsg']);
		}			
		redirect('bundlemessage#resultDiv');
	}
	
	public function editBundleMessageById(){
		//echo '<pre>';print_r($_REQUEST);exit;
		$varBundleId = trim($_REQUEST['bundleId']);					
		$varMessageId = trim($_REQUEST['messageId']);
		$varComment = trim($_REQUEST['comment']);		
		$varMessageText = trim($_REQUEST['messageText']);
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'bundleid'=>$varBundleId,'messageid'=>$varMessageId,'comment'=>$varComment,'message_text'=>$varMessageText,'insertby'=>'BPPortal','process_flag'=>'1','xmlData'=>'NULL');
		$arrUpdateBundleMessage = ApiPostHeader($this->config->item('InsertBundleMessage'), $params);
		//echo '<pre>';print_r($params);print_r($arrUpdateBundleMessage);echo $this->config->item('InsertBundleMessage');exit;
		
		$arrResult = array();
		$arrResult['errcode'] = $arrUpdateBundleMessage[0]['errcode'];	
		$arrResult['errmsg'] = $arrUpdateBundleMessage[0]['errmsg'];			
		echo json_encode($arrResult);exit;
	}
}
?>