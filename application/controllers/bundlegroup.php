<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bundlegroup extends CI_Controller {	
	function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {
				session_start();
		}		
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{				
		$data = array();							
		//Bundle list (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'32');
		$arrGetBundleInfoRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrGetBundleInfoRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrGetBundleInfoRes'] = $arrGetBundleInfoRes;
		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundleGroup/bundle_group_view',$data);
		$this->load->view('footer_view');
	}
	
	public function getAllBundleGroupListInfo(){
		ini_set("memory_limit", "-1");
		ini_set('max_execution_time', 900);										
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'group_id'=>'-1');		
		$arrGetAllBundleGroupIdRes = ApiPostHeader($this->config->item('GetBundleGroup'), $params);		
		//echo '<pre>';print_r($params);print_r($arrGetAllBundleGroupIdRes);echo $this->config->item('GetBundleGroup');exit;	
		
		$varResult='';
		if((isset($arrGetAllBundleGroupIdRes[0]['errcode']) && $arrGetAllBundleGroupIdRes[0]['errcode']=='0') || isset($arrGetAllBundleGroupIdRes['errcode']) && $arrGetAllBundleGroupIdRes['errcode']=='0'){	
			$varResult .= '<script type="text/javascript">
				$("#datatable_tabletools").DataTable();	
				</script>';
				
			$varResult .= '<table id="datatable_tabletools" class="table table-bordered table-striped nowrap dt-responsive" width="100%">
				<thead>
					<tr>
						<th data-class="expand">GroupId</th>
						<th data-hide="phone">BundleId</th>
						<th data-hide="phone">Comment</th>											
					</tr>
				</thead>
				<tbody>';
			foreach($arrGetAllBundleGroupIdRes as $arrResult){														  
				 $varResult .= "<tr><td>".$arrResult['groupid']."</td>
								<td>".$arrResult['Bundle_id']."</td>								
								<td>".$arrResult['comment']."</td>
								</tr>";								
			}				
			$varResult .=	'</tbody>
								</table>';		
		}else{
			$varResult = '';			
		}
		echo $varResult;exit;
	}
	
	public function addBundleGroupId(){
		//echo '<pre>';print_r($_REQUEST);exit;		
		$varBundleId = trim($_REQUEST['selBundleId']);
		$varComment = trim($_REQUEST['comment']);
		$params = array('sitecode'=>$_SESSION['SiteCode'],'groupid'=>'0','bundleid'=>$varBundleId,'comment'=>$varComment,'entryby'=>'BPPortal','process_flag'=>'1');
		$arrAddBundleGroupId = ApiPostHeader($this->config->item('AddBundleGroupId'), $params);
		//echo '<pre>';print_r($params);print_r($arrAddBundleGroupId);echo $this->config->item('AddBundleGroupId');exit;
		
		if($arrAddBundleGroupId[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrAddBundleGroupId[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrAddBundleGroupId[0]['errmsg']);
		}			
		redirect('bundlegroup');	
	}
	public function getByBundleGroupId()
	{	
		$data = array();		
		/*$params = array('sitecode'=>$_SESSION['SiteCode'],'Bundleid'=>'-1');
		$arrGetBundleInfoRes = ApiPostHeader($this->config->item('SearchBundlePlan'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundleInfoRes);echo $this->config->item('SearchBundlePlan');exit;			
		$data['arrGetBundleInfoRes']=$arrGetBundleInfoRes;*/	
		
		//Bundle list (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'32');
		$arrGetBundleInfoRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrGetBundleInfoRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrGetBundleInfoRes'] = $arrGetBundleInfoRes;
		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundleGroup/edit_bundle_group_view',$data);
		$this->load->view('footer_view');
	}
	public function getBundleIdByGroupId(){
		$varBundleGroupId =  $this->input->post('getBundleGroupId');				
		$params = array('sitecode'=>$_SESSION['SiteCode'],'group_id'=>$varBundleGroupId);		
		$arrGetBundleGroupIdRes = ApiPostHeader($this->config->item('GetBundleGroup'), $params);		
		//echo '<pre>';print_r($params);print_r($arrGetBundleGroupIdRes);echo $this->config->item('GetBundleGroup');exit;		
		$varResult='';
		if((isset($arrGetBundleGroupIdRes[0]['errcode']) && $arrGetBundleGroupIdRes[0]['errcode']=='0') || isset($arrGetBundleGroupIdRes['errcode']) && $arrGetBundleGroupIdRes['errcode']=='0'){			
			
			foreach($arrGetBundleGroupIdRes as $arrResult){							
				$varResult .= $arrResult['Bundle_id'].',';
			}					
			$arrResult['bundleId'] = rtrim($varResult,',');								
			$arrResult['Comment'] = $arrGetBundleGroupIdRes[0]['comment'];				
		}else{			
			$arrResult = '';			
		}
		echo json_encode($arrResult);exit;	
	}
	
	public function updateBundleGroupId(){
		//echo '<pre>';print_r($_REQUEST);exit;		
		$varBundleGroupId = trim($_REQUEST['getBundleGroupId']);
		$varBundleId = trim($_REQUEST['selBundleId']);
		$varComment = trim($_REQUEST['comment']);
		$params = array('sitecode'=>$_SESSION['SiteCode'],'groupid'=>$varBundleGroupId,'bundleid'=>$varBundleId,'comment'=>$varComment,'entryby'=>'BPPortal','process_flag'=>'2');
		$arrUpdateBundleGroupId = ApiPostHeader($this->config->item('AddBundleGroupId'), $params);
		//echo '<pre>';print_r($params);print_r($arrUpdateBundleGroupId);echo $this->config->item('AddBundleGroupId');exit;
		
		if($arrUpdateBundleGroupId[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrUpdateBundleGroupId[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrUpdateBundleGroupId[0]['errmsg']);
		}			
		redirect('bundlegroup');	
	}
}
?>