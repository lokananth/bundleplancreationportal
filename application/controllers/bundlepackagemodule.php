<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bundlepackagemodule extends CI_Controller {	
	function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {
				session_start();
		}		
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlanPackage/bundle_package_module_view');
		$this->load->view('footer_view');
	}	
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */