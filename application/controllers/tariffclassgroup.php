<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tariffclassgroup extends CI_Controller {	
	function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {
				session_start();
		}		
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundleTariffClassGroup/tariff_class_group_view');
		$this->load->view('footer_view');
	}
	
	public function getAllTariffGroupListInfo(){
		ini_set("memory_limit", "-1");
		ini_set('max_execution_time', 900);										
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'group_id'=>'-1');		
		$arrGetTariffGroupIdRes = ApiPostHeader($this->config->item('GetTariffClassGroup'), $params);		
		//echo '<pre>';print_r($params);print_r($arrGetTariffGroupIdRes);echo $this->config->item('GetTariffClassGroup');exit;		
		
		$varResult='';
		if((isset($arrGetTariffGroupIdRes[0]['errcode']) && $arrGetTariffGroupIdRes[0]['errcode']=='0') || isset($arrGetTariffGroupIdRes['errcode']) && $arrGetTariffGroupIdRes['errcode']=='0'){	
			$varResult .= '<script type="text/javascript">
				$("#datatable_tabletools").DataTable();	
				</script>';
				
			$varResult .= '<table id="datatable_tabletools" class="table table-bordered table-striped nowrap dt-responsive" width="100%">
				<thead>
					<tr>
						<th data-class="expand">GroupId</th>
						<th data-hide="phone">TariffClass</th>
						<th data-hide="phone">Comment</th>											
					</tr>
				</thead>
				<tbody>';
			foreach($arrGetTariffGroupIdRes as $arrResult){														  
				 $varResult .= "<tr><td>".$arrResult['groupid']."</td>
								<td>".$arrResult['tariffclass']."</td>								
								<td>".$arrResult['comment']."</td>
								</tr>";								
			}				
			$varResult .=	'</tbody>
								</table>';		
		}else{
			$varResult = '';			
		}
		echo $varResult;exit;
	}
	
	public function addTariffClassGroup(){
		//echo '<pre>';print_r($_REQUEST);
		//exit;
		$varTariffClassRequest = trim($_REQUEST['tariffClass']);		
		$arrTariffClass = $_REQUEST['field_name'];
		array_push($arrTariffClass,$varTariffClassRequest);		
		$arrTariffClass = array_unique(array_filter($arrTariffClass));
		//echo '<pre>';print_r($arrTariffClass);exit;
		$varTariffClass = implode(",",$arrTariffClass);				
		//$varTariffClass = trim($_REQUEST['tariffClass']);
		$varComment = trim($_REQUEST['comment']);
		$params = array('sitecode'=>$_SESSION['SiteCode'],'groupid'=>'0','tariffclass'=>$varTariffClass,'comment'=>$varComment,'process_flag'=>'1');
		$arrAddTariffClassGroup = ApiPostHeader($this->config->item('AddTariffClassGroup'), $params);
		//echo '<pre>';print_r($params);print_r($arrAddTariffClassGroup);echo $this->config->item('AddTariffClassGroup');exit;
		
		if($arrAddTariffClassGroup[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrAddTariffClassGroup[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrAddTariffClassGroup[0]['errmsg']);
		}			
		redirect('tariffclassgroup');			
	}
	
	/*public function checkTariffClassGroup(){
		//echo '<pre>';print_r($_REQUEST);exit;
		$varTariffClass = trim($_REQUEST['tariffValue']);	
		$varResult = '';
		$arrTariffClass = explode(",",$varTariffClass);
		//echo '<pre>';print_r($arrTariffClass);exit;
		$arrUniqueTariffClass = array_unique($arrTariffClass);
		$varCountTariffClass = count($arrTariffClass);
		$varCountUniqueTariffClass = count($arrUniqueTariffClass);
		if($varCountTariffClass==$varCountUniqueTariffClass){
			$varResult = '0';
		}else{
			$varResult = '-1';
		}				
		echo $varResult;exit;
	}*/
	
	public function editTariffClassByGroupId($varCondition='')
	{		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		if($varCondition=='1'){
			$this->load->view('bundleTariffClassGroup/edit_tariff_class_group_view');
		}else if($varCondition=='2'){
			$this->load->view('bundleTariffClassGroup/delete_tariff_class_group_view');
		}
		$this->load->view('footer_view');
	}
	
	public function getTariffClassByGroupId(){
		$varTariffClassGroupId =  $this->input->post('getTariffClassGroupId');				
		$params = array('sitecode'=>$_SESSION['SiteCode'],'group_id'=>$varTariffClassGroupId);	
		$arrGetTariffGroupIdRes = ApiPostHeader($this->config->item('GetTariffClassGroup'), $params);		
		//echo '<pre>';print_r($params);print_r($arrGetTariffGroupIdRes);echo $this->config->item('GetTariffClassGroup');exit;				
			
		$varResult='';
		if((isset($arrGetTariffGroupIdRes[0]['errcode']) && $arrGetTariffGroupIdRes[0]['errcode']=='0') || isset($arrGetTariffGroupIdRes['errcode']) && $arrGetTariffGroupIdRes['errcode']=='0'){			
			
			/*foreach($arrGetTariffGroupIdRes as $arrResult){							
				$varResult .= '<input type="text" id="tariffClass" name="field_name[]" class="tariffCSS tariffSpace" value="'.$arrResult['tariffclass'].'" placeholder="Enter the Tariff Class" maxlength="4" autocomplete="off" /><br><br>';
			}					
			$arrResult['TariffClass'] = $varResult;*/								
			$arrResult['Comment'] = $arrGetTariffGroupIdRes[0]['comment'];
		}else{			
			$arrResult = '';	
		}
		echo json_encode($arrResult);exit;
	}
	
	public function updateTariffClassGroup(){
		//echo '<pre>';print_r($_REQUEST);exit;	
		$varTariffClassRequest = trim($_REQUEST['tariffClass']);		
		$arrTariffClass = $_REQUEST['field_name'];
		array_push($arrTariffClass,$varTariffClassRequest);				
		$varTariffGroupId = trim($_REQUEST['tariffGroupId']);		
		$arrTariffClass = array_unique(array_filter($arrTariffClass));
		//echo '<pre>';print_r($arrTariffClass);exit;
		$varTariffClass = implode(",",$arrTariffClass);		
		//$varTariffClass = trim($_REQUEST['tariffClass']);
		$varComment = trim($_REQUEST['comment']);
		$params = array('sitecode'=>$_SESSION['SiteCode'],'groupid'=>$varTariffGroupId,'tariffclass'=>$varTariffClass,'comment'=>$varComment,'process_flag'=>'2');
		$arrUpdateTariffClassGroup = ApiPostHeader($this->config->item('AddTariffClassGroup'), $params);
		//echo '<pre>';print_r($params);print_r($arrUpdateTariffClassGroup);echo $this->config->item('AddTariffClassGroup');exit;
		
		if($arrUpdateTariffClassGroup[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrUpdateTariffClassGroup[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrUpdateTariffClassGroup[0]['errmsg']);
		}			
		redirect('tariffclassgroup');			
	}
	
	public function getTariffClassByTariffGroupId(){
		$varTariffClassGroupId =  $this->input->post('getTariffClassGroupId');								
		$params = array('sitecode'=>$_SESSION['SiteCode'],'group_id'=>$varTariffClassGroupId);	
		$arrGetTariffGroupIdRes = ApiPostHeader($this->config->item('GetTariffClassGroup'), $params);		
		//echo '<pre>';print_r($params);print_r($arrGetTariffGroupIdRes);echo $this->config->item('GetTariffClassGroup');exit;	
		
		$varResult='';
		if((isset($arrGetTariffGroupIdRes[0]['errcode']) && $arrGetTariffGroupIdRes[0]['errcode']=='0') || isset($arrGetTariffGroupIdRes['errcode']) && $arrGetTariffGroupIdRes['errcode']=='0'){
			$varResult .='<option value="">Select</option>';
			foreach($arrGetTariffGroupIdRes as $arrResult){ 
				$varResult .= "<option value=".$arrResult['tariffclass'].">".$arrResult['tariffclass']."</option>";
			}
			echo $varResult;exit;	
		}else{
			echo $varResult;exit;	
		}		
	}
	
	public function deleteTariffClassGroup(){
		//echo '<pre>';print_r($_REQUEST);exit;	
		$varTariffGroupId = trim($_REQUEST['tariffGroupId']);	
		$varTariffClass = trim($_REQUEST['selTariffClass']);		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'groupid'=>$varTariffGroupId,'tariffclass'=>$varTariffClass);
		$arrDeleteTariffClassGroup = ApiPostHeader($this->config->item('DeleteTariffClassGroup'), $params);
		//echo '<pre>';print_r($params);print_r($arrDeleteTariffClassGroup);echo $this->config->item('DeleteTariffClassGroup');exit;
		
		if($arrDeleteTariffClassGroup[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrDeleteTariffClassGroup[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrDeleteTariffClassGroup[0]['errmsg']);
		}			
		redirect('tariffclassgroup');	
	}
}
?>