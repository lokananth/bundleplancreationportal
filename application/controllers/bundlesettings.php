<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bundlesettings extends CI_Controller {	
	function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {
				session_start();
		}		
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	public function index()
	{		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlesettings/upload_download_settings_view');
		$this->load->view('footer_view');
	}
	public function uploadFileSettings(){
		//echo '<pre>';print_r($_FILES);exit;
		if(isset($_FILES['fileUpload'])){	
			$config['upload_path'] = './uploadExcel/';
			$config['overwrite'] = TRUE;
			$config['allowed_types'] = 'xls|xlsx';	
			$new_name = 'SettingsFiles.xls';
			$config['file_name'] = $new_name;

			$field='fileUpload';
			$this->load->library('upload', $config);			
			
			if ($this->upload->do_upload($field))
			{	
				//echo '<pre>';print_r($this->upload->data());exit;
				$arrUploadedFile = $this->upload->data();
				$varFileName = $arrUploadedFile['file_name'];
				$this->session->set_flashdata('successmsg',"File Uploaded Successfully");
				redirect('bundlesettings');	
			}
			else
			{			
				$varFileName = '';
				$this->session->set_flashdata('errormsg',"Error in file upload");
				redirect('bundlesettings');
			}		
		}else{
			$this->session->set_flashdata('errormsg',"Error in file upload");
			redirect('bundlesettings');
		}
	}
	
	public function downloadSettingsFile(){
		$varFileName = 'SettingsFiles.xls';
		$varFilePath = dirname(dirname(dirname(realpath(__FILE__)))).DIRECTORY_SEPARATOR.'uploadExcel'.DIRECTORY_SEPARATOR;
		$varFile = $varFilePath.$varFileName;		
		if($varFileName==''){
			$this->session->set_flashdata('successmsg','Sorry file entry missing!');
			redirect('bundlesettings');exit;
		}		
		if (file_exists($varFile)) {			
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header("Cache-Control: public");
			header("Content-Description: File Transfer");
			header("Content-Disposition: attachment; filename=".$varFileName."");
			header("Content-Transfer-Encoding: binary");
			header("Content-Type: binary/octet-stream");
			ob_clean();
			flush();
			readfile($varFile);
			exit;			
		}else{			
			$this->session->set_flashdata('successmsg','File doesn\'t exit in path!');
			redirect('bundlesettings');exit;			
		}
	}
}
?>