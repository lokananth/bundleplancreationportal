<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bundlepackagecreation extends CI_Controller {	
	function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {
				session_start();
		}		
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlanPackage/packageCreation/bundle_package_search_view');
		$this->load->view('footer_view');
	}
	
	public function getAllPackageCreationListInfo(){
		ini_set("memory_limit", "-1");
		ini_set('max_execution_time', 900);										
		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'package_id'=>'-1');
		$arrGetBundlePackagesInfoRes = ApiPostHeader($this->config->item('GetBundlePackages'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundlePackagesInfoRes);echo $this->config->item('GetBundlePackages');exit;	
		
		$varResult='';
		if((isset($arrGetBundlePackagesInfoRes[0]['errcode']) && $arrGetBundlePackagesInfoRes[0]['errcode']=='0') || isset($arrGetBundlePackagesInfoRes['errcode']) && $arrGetBundlePackagesInfoRes['errcode']=='0'){	
			$varResult .= '<script type="text/javascript">
				$("#datatable_tabletools").DataTable();	
				</script>';
				
			$varResult .= '<table id="datatable_tabletools" class="table table-bordered table-striped nowrap dt-responsive" width="100%">
				<thead>
					<tr>
						<th data-class="expand">PackageId</th>
						<th data-hide="phone">Package Name</th>
						<th data-hide="phone">Price</th>
						<th data-hide="phone">Currency</th>
						<th data-hide="phone,tablet">Tariff Class</th>
						<th data-hide="phone,tablet">Description</th>												
					</tr>
				</thead>
				<tbody>';
			foreach($arrGetBundlePackagesInfoRes as $arrResult){														  
				 $varResult .= "<tr><td>".$arrResult['package_id']."</td>
								<td>".$arrResult['name']."</td>
								<td>".number_format((float) $arrResult['price'], 2, '.', '')."</td>
								<td>".$arrResult['currcode']."</td>
								<td>".$arrResult['tariffclass']."</td>
								<td>".$arrResult['deskription']."</td></tr>";								
			}				
			$varResult .=	'</tbody>
								</table>';		
		}else{
			$varResult = '';			
		}
		echo $varResult;exit;
	}
	
	public function searchPackagePlan(){		
		//echo '<pre>';print_r($_REQUEST);exit;
		if($_POST){
			$varPackageId = trim($_REQUEST['packageId']);	
			$_SESSION['PackageId'] = $varPackageId;
			unset($_SESSION['EditPackageStep1']);	
			unset($_SESSION['EditPackageStep2']);
			
			$params = array('sitecode'=>$_SESSION['SiteCode'],'package_id'=>$varPackageId);
			$arrGetPackageInfoRes = ApiPostHeader($this->config->item('GetBundlePackages'), $params);
			//echo '<pre>';print_r($params);print_r($arrGetPackageInfoRes);echo $this->config->item('GetBundlePackages');exit;				
		}else{
			$varPackageId = $_SESSION['PackageId'];	
			$params = array('sitecode'=>$_SESSION['SiteCode'],'package_id'=>$varPackageId);
			$arrGetPackageInfoRes = ApiPostHeader($this->config->item('GetBundlePackages'), $params);
			//echo '<pre>';print_r($params);print_r($arrGetPackageInfoRes);echo $this->config->item('GetBundlePackages');exit;	
		}
		
		//$arrGetBundleInfoRes[0]['errcode']='-1';
		//$arrGetBundleInfoRes[0]['errmsg']='Kindly check the given value';
		
		if((isset($arrGetPackageInfoRes[0]['errcode']) && $arrGetPackageInfoRes[0]['errcode']=='0') || isset($arrGetPackageInfoRes['errcode']) && $arrGetPackageInfoRes['errcode']=='0'){						
					
			$data['arrGetPackageInfoRes'] = $arrGetPackageInfoRes;
			$data['varPackageId'] = $varPackageId;
			
			//Package Type Id (Drop Down)
			$arrPackageTypeIdRes = array();
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'1');
			$arrPackageTypeIdRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
			//echo '<pre>';print_r($arrPackageTypeIdRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
			$data['arrPackageTypeId'] = $arrPackageTypeIdRes;				
				
			//USSD Info Type (Drop Down)		
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'2');
			$arrUSSDInfoTypeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
			//echo '<pre>';print_r($arrUSSDInfoTypeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
			$data['arrUSSDInfoType'] = $arrUSSDInfoTypeRes;
			
			//Play Prompt (Drop Down)		
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'3');
			$arrPlayPromptRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
			//echo '<pre>';print_r($arrPlayPromptRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
			$data['arrPlayPrompt'] = $arrPlayPromptRes;
			
			//Time ANC Mode (Drop Down)		
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'4');
			$arrTimeANCModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
			//echo '<pre>';print_r($arrTimeANCModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
			$data['arrTimeANCMode'] = $arrTimeANCModeRes;
			
			//Used Mode (Drop Down)		
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'5');
			$arrUsedModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
			//echo '<pre>';print_r($arrUsedModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
			$data['arrUsedMode'] = $arrUsedModeRes;		
			
			//Used Limit Mode (Drop Down)		
			$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'6');
			$arrUsedLimitModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
			//echo '<pre>';print_r($arrUsedLimitModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
			$data['arrUsedLimitMode'] = $arrUsedLimitModeRes;
			
			$this->load->view('top_header_view');
			$this->load->view('left_menu_view');
			$this->load->view('bundlePlanPackage/packageCreation/edit_package_view',$data);
			$this->load->view('footer_view');
		}else{
			$this->session->set_flashdata('errormsg',$arrGetPackageInfoRes[0]['errmsg']);
			redirect('bundlepackagecreation');	
		}
	}	
	
	public function editPackageStep2(){		
		//echo '<pre>';print_r($_REQUEST);exit;		
		if($_POST){
			
			$arrEditPackageStep1 = array('packageId'=>trim($_REQUEST['packageId']),'siteCode'=>trim($_REQUEST['siteCode']),'packageName'=>trim($_REQUEST['packageName']),'packageTypeId'=>trim($_REQUEST['packageTypeId']),'packageDescription'=>trim($_REQUEST['packageDescription']),'packagePrice'=>trim($_REQUEST['packagePrice']),'currency'=>trim($_REQUEST['currency']),'duration'=>trim($_REQUEST['duration']),'tariffClass'=>trim($_REQUEST['tariffClass']),'pricePerUnit'=>trim($_REQUEST['pricePerUnit']),'ussdInfoType'=>trim($_REQUEST['ussdInfoType']),'ussdInfoText'=>trim($_REQUEST['ussdInfoText']),'prompt'=>trim($_REQUEST['prompt']),'promptBalance'=>trim($_REQUEST['promptBalance']),'promptUnit'=>trim($_REQUEST['promptUnit']),'priority'=>trim($_REQUEST['priority']),'cardId'=>trim($_REQUEST['cardId']),'infoType'=>trim($_REQUEST['infoType']),'infoText'=>trim($_REQUEST['infoText']),'playPrompt'=>trim($_REQUEST['playPrompt']),'promptTimeANC'=>trim($_REQUEST['promptTimeANC']),'promptInfo'=>trim($_REQUEST['promptInfo']),'usedMode'=>trim($_REQUEST['usedMode']),'limitMode'=>trim($_REQUEST['limitMode']),'limitDelay'=>trim($_REQUEST['limitDelay']),'limitAmount'=>trim($_REQUEST['limitAmount']),'limitMinutes'=>trim($_REQUEST['limitMinutes']),'limitCall'=>trim($_REQUEST['limitCall']),'infoPrice'=>trim($_REQUEST['infoPrice']));			
			
			$varPackageId = trim($_REQUEST['packageId']);				
			$params = array('sitecode'=>$_SESSION['SiteCode'],'package_id'=>$varPackageId);
			$arrGetPackageInfoRes = ApiPostHeader($this->config->item('GetBundlePackages'), $params);
			//echo '<pre>';print_r($params);print_r($arrGetPackageInfoRes);echo $this->config->item('GetBundlePackages');exit;
			$_SESSION['EditPackageStep1'] = $arrEditPackageStep1;
		}else{		
			$varPackageId = $_SESSION['EditPackageStep1']['packageId'];	
			$params = array('sitecode'=>$_SESSION['SiteCode'],'package_id'=>$varPackageId);
			$arrGetPackageInfoRes = ApiPostHeader($this->config->item('GetBundlePackages'), $params);
			//echo '<pre>';print_r($params);print_r($arrGetPackageInfoRes);echo $this->config->item('GetBundlePackages');exit;			
		}			
		$data = array();
		$data['arrGetPackageInfoRes'] = $arrGetPackageInfoRes;
		$data['varPackageId'] = $varPackageId;
		
		//Pack Connection Charge (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'7');
		$arrPackConnectionChargeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPackConnectionChargeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPackConnectionCharge'] = $arrPackConnectionChargeRes;
		
		//Pack Free Flag (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'8');
		$arrPackFreeFlagRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPackFreeFlagRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPackFreeFlag'] = $arrPackFreeFlagRes;
		
		//Check Destination (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'9');
		$arrCheckDestinationRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrCheckDestinationRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrCheckDestination'] = $arrCheckDestinationRes;
		
		//Maximum Destination (Drop Down)		
		
		//Call Max Talk Time Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'10');
		$arrCallMaxTalkTimeModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrCallMaxTalkTimeModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrCallMaxTalkTimeMode'] = $arrCallMaxTalkTimeModeRes;
		
		//Sub Package (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'11');
		$arrSubPackageRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrSubPackageRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrSubPackage'] = $arrSubPackageRes;
		
		//Extension Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'12');
		$arrExtensionModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrExtensionModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrExtensionMode'] = $arrExtensionModeRes;
		
		//Info Day Code (Drop Down)	
		
		//Prefix Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'13');
		$arrPrefixModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPrefixModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPrefixMode'] = $arrPrefixModeRes;
		
		//Usage Notification Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'14');
		$arrUsageNotificationModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrUsageNotificationModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrUsageNotificationMode'] = $arrUsageNotificationModeRes;
		
		//Usage Notification Type (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'15');
		$arrUsageNotificationTypeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrUsageNotificationTypeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrUsageNotificationType'] = $arrUsageNotificationTypeRes;
		
		//Prefix CLI Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'16');
		$arrPrefixCLIModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPrefixCLIModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPrefixCLIMode'] = $arrPrefixCLIModeRes;			
		
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlanPackage/packageCreation/edit_package_step2_view',$data);
		$this->load->view('footer_view');
		
	}
	
	public function editPackageStep3(){
		//echo '<pre>';print_r($_REQUEST);exit;		
		
		if($_POST){
			$arrEditPackageStep2 = array('packConnection'=>trim($_REQUEST['packConnection']),'packFreeFlag'=>trim($_REQUEST['packFreeFlag']),'maxTalkTime'=>trim($_REQUEST['maxTalkTime']),'checkDestination'=>trim($_REQUEST['checkDestination']),'maxDestination'=>trim($_REQUEST['maxDestination']),'destinationDelay'=>trim($_REQUEST['destinationDelay']),'maxTalkTimeMode'=>trim($_REQUEST['maxTalkTimeMode']),'packStartDate'=>trim($_REQUEST['packStartDate']),'packEndDate'=>trim($_REQUEST['packEndDate']),'subPackage'=>trim($_REQUEST['subPackage']),'promoBalance'=>trim($_REQUEST['promoBalance']),'extensionMode'=>trim($_REQUEST['extensionMode']),'infoDayCode'=>trim($_REQUEST['infoDayCode']),'noPriceUnit'=>trim($_REQUEST['noPriceUnit']),'pricePerUnitList'=>trim($_REQUEST['pricePerUnitList']),'masterNotif'=>trim($_REQUEST['masterNotif']),'prefixMode'=>trim($_REQUEST['prefixMode']),'usageNotifMode'=>trim($_REQUEST['usageNotifMode']),'usageNotifType'=>trim($_REQUEST['usageNotifType']),'usageNotifText'=>trim($_REQUEST['usageNotifText']),'usageNotifPrompt'=>trim($_REQUEST['usageNotifPrompt']),'prefixDest'=>trim($_REQUEST['prefixDest']),'prefixDestMode'=>trim($_REQUEST['prefixDestMode']),'prefixCLIMode'=>trim($_REQUEST['prefixCLIMode']),'checkCLI'=>trim($_REQUEST['checkCLI']),'maximumCLI'=>trim($_REQUEST['maximumCLI']),'maxAccount'=>trim($_REQUEST['maxAccount']),'checkAccount'=>trim($_REQUEST['checkAccount']));
		
			$_SESSION['EditPackageStep2'] = $arrEditPackageStep2;
		}		
		$data = array();
		
		$varPackageId = $_SESSION['EditPackageStep1']['packageId'];	
		$params = array('sitecode'=>$_SESSION['SiteCode'],'package_id'=>$varPackageId);
		$arrGetPackageInfoRes = ApiPostHeader($this->config->item('GetBundlePackages'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetPackageInfoRes);echo $this->config->item('GetBundlePackages');exit;	
		
		$data['arrGetPackageInfoRes'] = $arrGetPackageInfoRes;
		$data['varPackageId'] = $varPackageId;
		
		//Prompt Not Ok Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'17');
		$arrPromptNotOkModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPromptNotOkModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPromptNotOkMode'] = $arrPromptNotOkModeRes;
		
		//Flag Buy Bundle (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'18');
		$arrFlagBuyBundleRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrFlagBuyBundleRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrFlagBuyBundle'] = $arrFlagBuyBundleRes;
		
		//Flag Buy AirTime (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'19');
		$arrFlagBuyAirTimeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrFlagBuyAirTimeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrFlagBuyAirTime'] = $arrFlagBuyAirTimeRes;
		
		//Breakage Charge Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'20');
		$arrBreakageChargeModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrBreakageChargeModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrBreakageChargeMode'] = $arrBreakageChargeModeRes;
		
		//Flag To Pay Connection Charge (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'21');
		$arrFlagPayConnRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrFlagPayConnRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrFlagPayConn'] = $arrFlagPayConnRes;
		
		//Auth Destination Flag (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'22');
		$arrAuthDestinationFlagRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrAuthDestinationFlagRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrAuthDestinationFlag'] = $arrAuthDestinationFlagRes;
		
		//ACC Minimum Balance (Drop Down)
		
		//Auth Destination Action (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'23');
		$arrAuthDestinationActionRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrAuthDestinationActionRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrAuthDestinationAction'] = $arrAuthDestinationActionRes;
		
		//Use Limit Action (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'24');
		$arrUseLimitActionRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrUseLimitActionRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrUseLimitAction'] = $arrUseLimitActionRes;		
		
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlanPackage/packageCreation/edit_package_step3_view',$data);
		$this->load->view('footer_view');		
	}
	
	public function editPackageSteps(){
		//echo '<pre>';print_r($_SESSION['EditPackageStep1']);print_r($_SESSION['EditPackageStep2']);print_r($_REQUEST);exit;		
		
		//Update Bundle Plan
		$params = array(			
			'sitecode'=>$_SESSION['SiteCode'],
			'package_id'=>$_SESSION['EditPackageStep1']['packageId'],
			'name'=>$_SESSION['EditPackageStep1']['packageName'],
			'packageTypeID'=>$_SESSION['EditPackageStep1']['packageTypeId'],
			'price'=>$_SESSION['EditPackageStep1']['packagePrice'],
			'currcode'=>$_SESSION['EditPackageStep1']['currency'],
			'duration'=>$_SESSION['EditPackageStep1']['duration'],
			'deskription'=>$_SESSION['EditPackageStep1']['packageDescription'],
			'prompt'=>$_SESSION['EditPackageStep1']['prompt'],
			'promptBal'=>$_SESSION['EditPackageStep1']['promptBalance'],
			'promptUnit'=>$_SESSION['EditPackageStep1']['promptUnit'],
			'priority'=>$_SESSION['EditPackageStep1']['priority'],
			'tariffclass'=>$_SESSION['EditPackageStep1']['tariffClass'],
			'card_id'=>$_SESSION['EditPackageStep1']['cardId'],
			'priceperunit'=>$_SESSION['EditPackageStep1']['pricePerUnit'],
			'info_type'=>$_SESSION['EditPackageStep1']['infoType'],
			'info_text'=>$_SESSION['EditPackageStep1']['infoText'],
			'playprompt'=>$_SESSION['EditPackageStep1']['playPrompt'],
			'promptTimeAnc'=>$_SESSION['EditPackageStep1']['promptTimeANC'],
			'promptInfo'=>$_SESSION['EditPackageStep1']['promptInfo'],
			'usedmode'=>$_SESSION['EditPackageStep1']['usedMode'],			
			'uselimit_delay'=>$_SESSION['EditPackageStep1']['limitDelay'],
			'uselimit_amount'=>$_SESSION['EditPackageStep1']['limitAmount'],
			'uselimit_mode'=>$_SESSION['EditPackageStep1']['limitMode'],
			'uselimit_minutes'=>$_SESSION['EditPackageStep1']['limitMinutes'],
			'uselimit_call'=>$_SESSION['EditPackageStep1']['limitCall'],
			'ussd_info_type'=>$_SESSION['EditPackageStep1']['ussdInfoType'],
			'ussd_info_text'=>$_SESSION['EditPackageStep1']['ussdInfoText'],
			'info_price'=>$_SESSION['EditPackageStep1']['infoPrice'],
			'call_maxtalktime_mode'=>$_SESSION['EditPackageStep2']['maxTalkTimeMode'],
			'ispackcnxcharge'=>$_SESSION['EditPackageStep2']['packConnection'],
			'packfree_flag'=>$_SESSION['EditPackageStep2']['packFreeFlag'],
			'call_maxtalktime'=>$_SESSION['EditPackageStep2']['maxTalkTime'],						
			'check_dest'=>$_SESSION['EditPackageStep2']['checkDestination'],
			'pack_startdate'=>$_SESSION['EditPackageStep2']['packStartDate'],
			'pack_enddate'=>$_SESSION['EditPackageStep2']['packEndDate'],			
			'isSubPackage'=>$_SESSION['EditPackageStep2']['subPackage'],
			'promobal_ratio'=>$_SESSION['EditPackageStep2']['promoBalance'],
			'extension_mode'=>$_SESSION['EditPackageStep2']['extensionMode'],
			'info_daycode'=>$_SESSION['EditPackageStep2']['infoDayCode'],
			'master_notif'=>$_SESSION['EditPackageStep2']['masterNotif'],
			'nof_priceperunit'=>$_SESSION['EditPackageStep2']['noPriceUnit'], 
			'priceperunit_list'=>$_SESSION['EditPackageStep2']['pricePerUnitList'],
			'max_dest'=>$_SESSION['EditPackageStep2']['maxDestination'],
			'Prefix_mode'=>$_SESSION['EditPackageStep2']['prefixMode'],
			'usagenotif_mode'=>$_SESSION['EditPackageStep2']['usageNotifMode'],
			'usagenotif_type'=>$_SESSION['EditPackageStep2']['usageNotifType'],
			'usagenotif_text'=>$_SESSION['EditPackageStep2']['usageNotifText'],
			'usagenotif_prompt'=>$_SESSION['EditPackageStep2']['usageNotifPrompt'],
			'prefixdest'=>$_SESSION['EditPackageStep2']['prefixDest'],			
			'check_cli'=>$_SESSION['EditPackageStep2']['checkCLI'],
			'max_cli'=>$_SESSION['EditPackageStep2']['maximumCLI'],
			'max_account'=>$_SESSION['EditPackageStep2']['maxAccount'],
			'check_account'=>$_SESSION['EditPackageStep2']['checkAccount'],			
			'prefixcli_mode'=>$_SESSION['EditPackageStep2']['prefixCLIMode'],
			'prefixdest_mode'=>$_SESSION['EditPackageStep2']['prefixDestMode'],			
			'dest_delay'=>$_SESSION['EditPackageStep2']['destinationDelay'],
			'usagenotif_threshold'=>trim($_REQUEST['usageNotIfThreshold']),			
			'flag_tobuy_bundle'=>trim($_REQUEST['flagBuyBundle']),
			'promptBalZero'=>trim($_REQUEST['pBalanceZero']),
			'promptExp'=>trim($_REQUEST['promptExpiry']),
			'promptInactive'=>trim($_REQUEST['promptInactive']),
			'prompt_notOK_mode'=>trim($_REQUEST['promptNotOkMode']),
			'breakage_charge_mode'=>trim($_REQUEST['breakageChargeMode']),
			'flag_tobuy_airtime'=>trim($_REQUEST['flagBuyAirTime']),
			'info_order'=>trim($_REQUEST['infoOrder']),
			'flag_topay_cnxcharge'=>trim($_REQUEST['flagPayConn']),
			'acc_minbal'=>trim($_REQUEST['accMinBalance']),
			'dest_pool_id'=>trim($_REQUEST['destPoolId']),
			'activate_minbal_master'=>trim($_REQUEST['activateMinBalMaster']),
			'auth_dest_flag'=>trim($_REQUEST['authDestinationFlag']),
			'auth_dest_ctr'=>trim($_REQUEST['authDestinationCTR']),
			'auth_dest_delay'=>trim($_REQUEST['authDestinationDelay']),
			'auth_dest_action'=>trim($_REQUEST['authDestinationAction']),
			'auth_dest_prompt'=>trim($_REQUEST['authDestinationPrompt']),
			'activate_package_groupID'=>trim($_REQUEST['actPackageGroupId']),
			'uselimit_action'=>trim($_REQUEST['useLimitAction']),
			'ODB_balance_threshold'=>trim($_REQUEST['balanceThreshold']),
			'promo_pack_groupid'=>trim($_REQUEST['promoPackageGroupId']),
			'ip_address'=>getClientIp()	
		);
		
		$arrEditPackageCreation = ApiPostHeader($this->config->item('UpdatePackageCreation'), $params);
		//echo '<pre>';print_r($params);print_r($arrEditPackageCreation);echo $this->config->item('UpdatePackageCreation');exit;
		
		if($arrEditPackageCreation[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrEditPackageCreation[0]['errmsg']);
				$this->session->set_flashdata('InsertPackageId',$_SESSION['EditPackageStep1']['packageId']);	
		}else{
			$this->session->set_flashdata('errormsg',$arrEditPackageCreation[0]['errmsg']);
		}			
		redirect('bundlepackagecreation');				
	}
	
	public function createNewPackage($varCondition=''){		
		//Package Type Id (Drop Down)
		$arrPackageTypeIdRes = array();
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'1');
		$arrPackageTypeIdRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPackageTypeIdRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPackageTypeId'] = $arrPackageTypeIdRes;				
			
		//USSD Info Type (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'2');
		$arrUSSDInfoTypeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrUSSDInfoTypeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrUSSDInfoType'] = $arrUSSDInfoTypeRes;
		
		//Play Prompt (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'3');
		$arrPlayPromptRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPlayPromptRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPlayPrompt'] = $arrPlayPromptRes;
		
		//Time ANC Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'4');
		$arrTimeANCModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrTimeANCModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrTimeANCMode'] = $arrTimeANCModeRes;
		
		//Used Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'5');
		$arrUsedModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrUsedModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrUsedMode'] = $arrUsedModeRes;		
		
		//Used Limit Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'6');
		$arrUsedLimitModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrUsedLimitModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrUsedLimitMode'] = $arrUsedLimitModeRes;
		
		if($varCondition=="0"){
			unset($_SESSION['PackageStep1']);	
			unset($_SESSION['PackageStep2']);
		}
		if($varCondition=="1"){
			//unset($_SESSION['BundleStep1']);	
		}
		
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlanPackage/packageCreation/create_package_view',$data);
		$this->load->view('footer_view');
	}
	
	public function createPackageStep2(){
		//echo '<pre>';print_r($_REQUEST);exit;		
		if($_POST){
			$arrPackageStep1 = array('siteCode'=>trim($_REQUEST['siteCode']),'packageName'=>trim($_REQUEST['packageName']),'packageTypeId'=>trim($_REQUEST['packageTypeId']),'packageDescription'=>trim($_REQUEST['packageDescription']),'packagePrice'=>trim($_REQUEST['packagePrice']),'currency'=>trim($_REQUEST['currency']),'duration'=>trim($_REQUEST['duration']),'tariffClass'=>trim($_REQUEST['tariffClass']),'pricePerUnit'=>trim($_REQUEST['pricePerUnit']),'ussdInfoType'=>trim($_REQUEST['ussdInfoType']),'ussdInfoText'=>trim($_REQUEST['ussdInfoText']),'prompt'=>trim($_REQUEST['prompt']),'promptBalance'=>trim($_REQUEST['promptBalance']),'promptUnit'=>trim($_REQUEST['promptUnit']),'priority'=>trim($_REQUEST['priority']),'cardId'=>trim($_REQUEST['cardId']),'infoType'=>trim($_REQUEST['infoType']),'infoText'=>trim($_REQUEST['infoText']),'playPrompt'=>trim($_REQUEST['playPrompt']),'promptTimeANC'=>trim($_REQUEST['promptTimeANC']),'promptInfo'=>trim($_REQUEST['promptInfo']),'usedMode'=>trim($_REQUEST['usedMode']),'limitMode'=>trim($_REQUEST['limitMode']),'limitDelay'=>trim($_REQUEST['limitDelay']),'limitAmount'=>trim($_REQUEST['limitAmount']),'limitMinutes'=>trim($_REQUEST['limitMinutes']),'limitCall'=>trim($_REQUEST['limitCall']),'infoPrice'=>trim($_REQUEST['infoPrice']));
			
			$_SESSION['PackageStep1'] = $arrPackageStep1;
		}			
		
		//Pack Connection Charge (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'7');
		$arrPackConnectionChargeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPackConnectionChargeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPackConnectionCharge'] = $arrPackConnectionChargeRes;
		
		//Pack Free Flag (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'8');
		$arrPackFreeFlagRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPackFreeFlagRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPackFreeFlag'] = $arrPackFreeFlagRes;
		
		//Check Destination (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'9');
		$arrCheckDestinationRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrCheckDestinationRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrCheckDestination'] = $arrCheckDestinationRes;
		
		//Maximum Destination (Drop Down)		
		
		//Call Max Talk Time Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'10');
		$arrCallMaxTalkTimeModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrCallMaxTalkTimeModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrCallMaxTalkTimeMode'] = $arrCallMaxTalkTimeModeRes;
		
		//Sub Package (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'11');
		$arrSubPackageRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrSubPackageRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrSubPackage'] = $arrSubPackageRes;
		
		//Extension Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'12');
		$arrExtensionModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrExtensionModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrExtensionMode'] = $arrExtensionModeRes;
		
		//Info Day Code (Drop Down)	
		
		//Prefix Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'13');
		$arrPrefixModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPrefixModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPrefixMode'] = $arrPrefixModeRes;
		
		//Usage Notification Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'14');
		$arrUsageNotificationModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrUsageNotificationModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrUsageNotificationMode'] = $arrUsageNotificationModeRes;
		
		//Usage Notification Type (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'15');
		$arrUsageNotificationTypeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrUsageNotificationTypeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrUsageNotificationType'] = $arrUsageNotificationTypeRes;
		
		//Prefix CLI Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'16');
		$arrPrefixCLIModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPrefixCLIModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPrefixCLIMode'] = $arrPrefixCLIModeRes;			
		
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlanPackage/packageCreation/package_step2_view',$data);
		$this->load->view('footer_view');
		
	}
	
	public function createPackageStep3(){
		//echo '<pre>';print_r($_REQUEST);exit;			
		
		if($_POST){
			$arrPackageStep2 = array('packConnection'=>trim($_REQUEST['packConnection']),'packFreeFlag'=>trim($_REQUEST['packFreeFlag']),'maxTalkTime'=>trim($_REQUEST['maxTalkTime']),'checkDestination'=>trim($_REQUEST['checkDestination']),'maxDestination'=>trim($_REQUEST['maxDestination']),'destinationDelay'=>trim($_REQUEST['destinationDelay']),'maxTalkTimeMode'=>trim($_REQUEST['maxTalkTimeMode']),'packStartDate'=>trim($_REQUEST['packStartDate']),'packEndDate'=>trim($_REQUEST['packEndDate']),'subPackage'=>trim($_REQUEST['subPackage']),'promoBalance'=>trim($_REQUEST['promoBalance']),'extensionMode'=>trim($_REQUEST['extensionMode']),'infoDayCode'=>trim($_REQUEST['infoDayCode']),'noPriceUnit'=>trim($_REQUEST['noPriceUnit']),'pricePerUnitList'=>trim($_REQUEST['pricePerUnitList']),'masterNotif'=>trim($_REQUEST['masterNotif']),'prefixMode'=>trim($_REQUEST['prefixMode']),'usageNotifMode'=>trim($_REQUEST['usageNotifMode']),'usageNotifType'=>trim($_REQUEST['usageNotifType']),'usageNotifText'=>trim($_REQUEST['usageNotifText']),'usageNotifPrompt'=>trim($_REQUEST['usageNotifPrompt']),'prefixDest'=>trim($_REQUEST['prefixDest']),'prefixDestMode'=>trim($_REQUEST['prefixDestMode']),'prefixCLIMode'=>trim($_REQUEST['prefixCLIMode']),'checkCLI'=>trim($_REQUEST['checkCLI']),'maximumCLI'=>trim($_REQUEST['maximumCLI']),'maxAccount'=>trim($_REQUEST['maxAccount']),'checkAccount'=>trim($_REQUEST['checkAccount']));
		
			$_SESSION['PackageStep2'] = $arrPackageStep2;
		}		
		
		$data = array();
		
		//Prompt Not Ok Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'17');
		$arrPromptNotOkModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrPromptNotOkModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrPromptNotOkMode'] = $arrPromptNotOkModeRes;
		
		//Flag Buy Bundle (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'18');
		$arrFlagBuyBundleRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrFlagBuyBundleRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrFlagBuyBundle'] = $arrFlagBuyBundleRes;
		
		//Flag Buy AirTime (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'19');
		$arrFlagBuyAirTimeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrFlagBuyAirTimeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrFlagBuyAirTime'] = $arrFlagBuyAirTimeRes;
		
		//Breakage Charge Mode (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'20');
		$arrBreakageChargeModeRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrBreakageChargeModeRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrBreakageChargeMode'] = $arrBreakageChargeModeRes;
		
		//Flag To Pay Connection Charge (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'21');
		$arrFlagPayConnRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrFlagPayConnRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrFlagPayConn'] = $arrFlagPayConnRes;
		
		//Auth Destination Flag (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'22');
		$arrAuthDestinationFlagRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrAuthDestinationFlagRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrAuthDestinationFlag'] = $arrAuthDestinationFlagRes;
		
		//ACC Minimum Balance (Drop Down)
		
		//Auth Destination Action (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'23');
		$arrAuthDestinationActionRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrAuthDestinationActionRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrAuthDestinationAction'] = $arrAuthDestinationActionRes;
		
		//Use Limit Action (Drop Down)		
		$params = array('sitecode'=>$_SESSION['SiteCode'],'Action'=>'24');
		$arrUseLimitActionRes = ApiPostHeader($this->config->item('GetPackagesDDInfo'), $params);
		//echo '<pre>';print_r($arrUseLimitActionRes);print_r($_SESSION);echo $this->config->item('GetPackagesDDInfo');exit;		
		$data['arrUseLimitAction'] = $arrUseLimitActionRes;		
		
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundlePlanPackage/packageCreation/package_step3_view',$data);
		$this->load->view('footer_view');		
	}
	
	public function addPackageSteps(){
		//echo '<pre>';print_r($_SESSION['PackageStep1']);print_r($_SESSION['PackageStep2']);print_r($_REQUEST);exit;
		
		//Insert Bundle Plan
		$params = array(			
			'sitecode'=>$_SESSION['SiteCode'],
			'name'=>$_SESSION['PackageStep1']['packageName'],
			'packageTypeID'=>$_SESSION['PackageStep1']['packageTypeId'],
			'price'=>$_SESSION['PackageStep1']['packagePrice'],
			'currcode'=>$_SESSION['PackageStep1']['currency'],
			'duration'=>$_SESSION['PackageStep1']['duration'],
			'deskription'=>$_SESSION['PackageStep1']['packageDescription'],
			'prompt'=>$_SESSION['PackageStep1']['prompt'],
			'promptBal'=>$_SESSION['PackageStep1']['promptBalance'],
			'promptUnit'=>$_SESSION['PackageStep1']['promptUnit'],
			'priority'=>$_SESSION['PackageStep1']['priority'],
			'tariffclass'=>$_SESSION['PackageStep1']['tariffClass'],
			'card_id'=>$_SESSION['PackageStep1']['cardId'],
			'priceperunit'=>$_SESSION['PackageStep1']['pricePerUnit'],
			'info_type'=>$_SESSION['PackageStep1']['infoType'],
			'info_text'=>$_SESSION['PackageStep1']['infoText'],
			'playprompt'=>$_SESSION['PackageStep1']['playPrompt'],
			'promptTimeAnc'=>$_SESSION['PackageStep1']['promptTimeANC'],
			'promptInfo'=>$_SESSION['PackageStep1']['promptInfo'],
			'usedmode'=>$_SESSION['PackageStep1']['usedMode'],			
			'uselimit_delay'=>$_SESSION['PackageStep1']['limitDelay'],
			'uselimit_amount'=>$_SESSION['PackageStep1']['limitAmount'],
			'uselimit_mode'=>$_SESSION['PackageStep1']['limitMode'],
			'uselimit_minutes'=>$_SESSION['PackageStep1']['limitMinutes'],
			'uselimit_call'=>$_SESSION['PackageStep1']['limitCall'],
			'ussd_info_type'=>$_SESSION['PackageStep1']['ussdInfoType'],
			'ussd_info_text'=>$_SESSION['PackageStep1']['ussdInfoText'],
			'info_price'=>$_SESSION['PackageStep1']['infoPrice'],
			'call_maxtalktime_mode'=>$_SESSION['PackageStep2']['maxTalkTimeMode'],
			'ispackcnxcharge'=>$_SESSION['PackageStep2']['packConnection'],
			'packfree_flag'=>$_SESSION['PackageStep2']['packFreeFlag'],
			'call_maxtalktime'=>$_SESSION['PackageStep2']['maxTalkTime'],						
			'check_dest'=>$_SESSION['PackageStep2']['checkDestination'],
			'pack_startdate'=>$_SESSION['PackageStep2']['packStartDate'],
			'pack_enddate'=>$_SESSION['PackageStep2']['packEndDate'],			
			'isSubPackage'=>$_SESSION['PackageStep2']['subPackage'],
			'promobal_ratio'=>$_SESSION['PackageStep2']['promoBalance'],
			'extension_mode'=>$_SESSION['PackageStep2']['extensionMode'],
			'info_daycode'=>$_SESSION['PackageStep2']['infoDayCode'],
			'master_notif'=>$_SESSION['PackageStep2']['masterNotif'],
			'nof_priceperunit'=>$_SESSION['PackageStep2']['noPriceUnit'], 
			'priceperunit_list'=>$_SESSION['PackageStep2']['pricePerUnitList'],
			'max_dest'=>$_SESSION['PackageStep2']['maxDestination'],
			'Prefix_mode'=>$_SESSION['PackageStep2']['prefixMode'],
			'usagenotif_mode'=>$_SESSION['PackageStep2']['usageNotifMode'],
			'usagenotif_type'=>$_SESSION['PackageStep2']['usageNotifType'],
			'usagenotif_text'=>$_SESSION['PackageStep2']['usageNotifText'],
			'usagenotif_prompt'=>$_SESSION['PackageStep2']['usageNotifPrompt'],
			'prefixdest'=>$_SESSION['PackageStep2']['prefixDest'],			
			'check_cli'=>$_SESSION['PackageStep2']['checkCLI'],
			'max_cli'=>$_SESSION['PackageStep2']['maximumCLI'],
			'max_account'=>$_SESSION['PackageStep2']['maxAccount'],
			'check_account'=>$_SESSION['PackageStep2']['checkAccount'],			
			'prefixcli_mode'=>$_SESSION['PackageStep2']['prefixCLIMode'],
			'prefixdest_mode'=>$_SESSION['PackageStep2']['prefixDestMode'],			
			'dest_delay'=>$_SESSION['PackageStep2']['destinationDelay'],
			'usagenotif_threshold'=>trim($_REQUEST['usageNotIfThreshold']),			
			'flag_tobuy_bundle'=>trim($_REQUEST['flagBuyBundle']),
			'promptBalZero'=>trim($_REQUEST['pBalanceZero']),
			'promptExp'=>trim($_REQUEST['promptExpiry']),
			'promptInactive'=>trim($_REQUEST['promptInactive']),
			'prompt_notOK_mode'=>trim($_REQUEST['promptNotOkMode']),
			'breakage_charge_mode'=>trim($_REQUEST['breakageChargeMode']),
			'flag_tobuy_airtime'=>trim($_REQUEST['flagBuyAirTime']),
			'info_order'=>trim($_REQUEST['infoOrder']),
			'flag_topay_cnxcharge'=>trim($_REQUEST['flagPayConn']),
			'acc_minbal'=>trim($_REQUEST['accMinBalance']),
			'dest_pool_id'=>trim($_REQUEST['destPoolId']),
			'activate_minbal_master'=>trim($_REQUEST['activateMinBalMaster']),
			'auth_dest_flag'=>trim($_REQUEST['authDestinationFlag']),
			'auth_dest_ctr'=>trim($_REQUEST['authDestinationCTR']),
			'auth_dest_delay'=>trim($_REQUEST['authDestinationDelay']),
			'auth_dest_action'=>trim($_REQUEST['authDestinationAction']),
			'auth_dest_prompt'=>trim($_REQUEST['authDestinationPrompt']),
			'activate_package_groupID'=>trim($_REQUEST['actPackageGroupId']),
			'uselimit_action'=>trim($_REQUEST['useLimitAction']),
			'ODB_balance_threshold'=>trim($_REQUEST['balanceThreshold']),
			'promo_pack_groupid'=>trim($_REQUEST['promoPackageGroupId']),
			'ip_address'=>getClientIp()	
		);
		
		$arrInsertPackageCreate = ApiPostHeader($this->config->item('InsertPackageCreate'), $params);
		//echo '<pre>';print_r($params);print_r($arrInsertPackageCreate);echo $this->config->item('InsertPackageCreate');exit;			
		if($arrInsertPackageCreate[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrInsertPackageCreate[0]['errmsg']);
				$this->session->set_flashdata('InsertPackageId',$arrInsertPackageCreate[0]['package_id']);
		}else{
			$this->session->set_flashdata('errormsg',$arrInsertPackageCreate[0]['errmsg']);
		}			
		redirect('bundlepackagecreation');				
	}	
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */