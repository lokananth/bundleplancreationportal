<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {	
	function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {
				session_start();
		}		
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{			
		$arrGetSiteProduceCodeRes = ApiGetHeader($this->config->item('GetSiteProductCode'), '');
		//echo '<pre>';print_r($arrGetSiteProduceCodeRes);print_r($_SESSION);echo $this->config->item('GetSiteProductCode');exit;
		
		$data['arrGetProductInfo'] = $arrGetSiteProduceCodeRes;	
		
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('dashboard_view',$data);
		$this->load->view('footer_view');		
	}	
	
	public function setProductSiteCode($varProductCode='',$varSiteCode='',$varCurrency=''){		
		$_SESSION['ProductCode'] = $varProductCode;
		$_SESSION['SiteCode'] = $varSiteCode;
		$_SESSION['Currency'] = $varCurrency;
		redirect('bundlemodule');		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */