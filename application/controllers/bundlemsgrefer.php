<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bundlemsgrefer extends CI_Controller {	
	function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {
				session_start();
		}		
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}	
	public function index()
	{	
		$data = array();
		$params = array('sitecode'=>$_SESSION['SiteCode'],'messageid'=>'-1');
		$arrGetBundleMessageReference = ApiPostHeader($this->config->item('GetBundleMessageReference'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundleMessageReference);echo $this->config->item('GetBundleMessageReference');exit;			
		$data['arrGetBundleMessageReference']=$arrGetBundleMessageReference;
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundleMessageReference/bundle_message_refer_view',$data);
		$this->load->view('footer_view');
	}
	public function addBundleMsgReference(){
		//echo '<pre>';print_r($_REQUEST);exit;		
		$varMessageId = trim($_REQUEST['messageId']);
		$varComment = trim($_REQUEST['comment']);
		$params = array('sitecode'=>$_SESSION['SiteCode'],'messageid'=>$varMessageId,'comment'=>$varComment,'insertby'=>'BPPortal','process_flag'=>'1');
		$arrAddBundleMessageReference = ApiPostHeader($this->config->item('AddBundleMessageReference'), $params);
		//echo '<pre>';print_r($params);print_r($arrAddBundleMessageReference);echo $this->config->item('AddBundleMessageReference');exit;
		
		if($arrAddBundleMessageReference[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrAddBundleMessageReference[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrAddBundleMessageReference[0]['errmsg']);
		}			
		redirect('bundlemsgrefer');			
	}
	public function getByBundleMsgReferenceId()
	{	$data = array();
		$params = array('sitecode'=>$_SESSION['SiteCode'],'messageid'=>'-1');
		$arrGetBundleMessageReference = ApiPostHeader($this->config->item('GetBundleMessageReference'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundleMessageReference);echo $this->config->item('GetBundleMessageReference');exit;			
		$data['arrGetBundleMessageReference']=$arrGetBundleMessageReference;
		$this->load->library('session');
		$this->load->view('top_header_view');
		$this->load->view('left_menu_view');
		$this->load->view('bundleMessageReference/edit_bundle_message_refer_view',$data);
		$this->load->view('footer_view');
	}
	public function getCommentByMessageId(){
		$varMessageId =  $this->input->post('messageId');				
		$params = array('sitecode'=>$_SESSION['SiteCode'],'messageid'=>$varMessageId);		
		$arrGetBundleMessageReference = ApiPostHeader($this->config->item('GetBundleMessageReference'), $params);		
		//echo '<pre>';print_r($params);print_r($arrGetBundleMessageReference);echo $this->config->item('GetBundleGroup');exit;		
		$varResult='';
		if((isset($arrGetBundleMessageReference[0]['errcode']) && $arrGetBundleMessageReference[0]['errcode']=='0') || isset($arrGetBundleMessageReference['errcode']) && $arrGetBundleMessageReference['errcode']=='0'){						
			$varResult = $arrGetBundleMessageReference[0]['comment'];														
		}else{			
			$varResult = '';	
		}
		echo $varResult;exit;
	}
	public function updateBundleMsgReference(){
		//echo '<pre>';print_r($_REQUEST);exit;		
		$varMessageId = trim($_REQUEST['messageId']);
		$varComment = trim($_REQUEST['comment']);
		$params = array('sitecode'=>$_SESSION['SiteCode'],'messageid'=>$varMessageId,'comment'=>$varComment,'insertby'=>'BPPortal','process_flag'=>'2');
		$arrUpdateBundleMessageReference = ApiPostHeader($this->config->item('AddBundleMessageReference'), $params);
		//echo '<pre>';print_r($params);print_r($arrUpdateBundleMessageReference);echo $this->config->item('AddBundleMessageReference');exit;
		
		if($arrUpdateBundleMessageReference[0]['errcode']=='0'){
				$this->session->set_flashdata('successmsg',$arrUpdateBundleMessageReference[0]['errmsg']);
		}else{
			$this->session->set_flashdata('errormsg',$arrUpdateBundleMessageReference[0]['errmsg']);
		}			
		redirect('bundlemsgrefer');			
	}
}
?>